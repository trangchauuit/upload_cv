import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import {Helmet} from 'react-helmet'
import * as serviceWorker from './serviceWorker';
import { I18nextProvider } from 'react-i18next';
import i18next from 'i18next';
import common_ja from "./translations/ja/common.json";
import common_en from "./translations/en/common.json";
import common_vi from "./translations/vi/common.json";
import LanguageDetector from 'i18next-browser-languagedetector';
import { Font } from '@react-pdf/renderer';
// import fontMincho from './assets/fonts/MS Mincho.ttf';
import fontAUMS from './assets/fonts/Arial Unicode MS Font.ttf';

Font.register({
    family: 'Arial Unicode MS',
    format: "truetype",
    fonts: [
        {
        src: fontAUMS
        },
        {
            src: fontAUMS,
            fontWeight: 500
        },
    ]
})
i18next.use(LanguageDetector)
.init({
    fallbackLng: 'ja',
    detection: {
        lookupLocalStorage: 'lang',
    },
    interpolation: { escapeValue: false },  // React already does escaping
    resources: {
        en: {
            common: common_en               // 'common' is our custom namespace
        },
        vi: {
            common: common_vi
        },
        ja: {
            common: common_ja
        },
    },
})


ReactDOM.render(
  <I18nextProvider i18n={i18next}>
    <Helmet title="Da Nang Fujikin" />
    <App />
  </I18nextProvider>,
  document.getElementById('root')
);

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
