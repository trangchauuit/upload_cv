import i18nextConfig from 'i18next';
import LanguageDetector from 'i18next-browser-languagedetector';
import common_vi from "../translations/vi/common.json";
import common_en from "../translations/en/common.json";
import common_ja from "../translations/ja/common.json";
i18nextConfig.use(LanguageDetector)
.init({
    detection: {
        lookupLocalStorage: 'lang',
    },
    interpolation: { escapeValue: false },  // React already does escaping
    resources: {
        en: {
            common: common_en               // 'common' is our custom namespace
        },
        vi: {
            common: common_vi
        },
        ja: {
            common: common_ja
        },
    },
})
export default i18nextConfig;