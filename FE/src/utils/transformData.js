import moment from "moment";

export const transformCandidateBasicInfor = (dataCommonInfo, dataBasicInfo, dataLanguage, dataWorkExperience, dataComment, dataAdvantageSkill) => {
  let infos = {
    status: dataCommonInfo.status,
    introduce_company_type_id: dataCommonInfo.introduceCompanyTypeId,
    company_referral: dataCommonInfo.introduceCompanyName,
    created_at: dataCommonInfo.applicationDate != null ?
        `${moment(new Date(dataCommonInfo.applicationDate)).format('YYYY-MM-DD')}` : null,
    remark: dataCommonInfo.remark,

    name_kana: dataBasicInfo.nameKana.value,
    name: dataBasicInfo.name.value,
    country: dataBasicInfo.country.value,
    phone: dataBasicInfo.phone.value,
    birthday: dataBasicInfo.birthday.value,
    age: dataBasicInfo.age.value,
    gender: dataBasicInfo.gender.value,
    email: dataBasicInfo.email.value,
    married_status: dataBasicInfo.marriedStatus.value,
    zip_code: dataBasicInfo.zipCode.value,
    near_train_station: dataBasicInfo.nearTrainStation.value,
    japan_visa: dataBasicInfo.japanVisa.value,
    japan_visa_date_expired: dataBasicInfo.japanVisaDateExpired.value,
    japan_visa_type: dataBasicInfo.japanVisaType.value,
    address: dataBasicInfo.currentAddress.value,
    address_contact: dataBasicInfo.contactAddress.value,
    zip_code_contact: dataBasicInfo.zipCodeContact.value,
    year_birthday: dataBasicInfo.dobYear.value,
    month_birthday: dataBasicInfo.dobMonth.value,
    day_birthday: dataBasicInfo.dobDay.value,

    japan_language_level: dataLanguage.japanLanguageLevel,
    english_language_level: dataLanguage.englishLanguageLevel,
    // other_language: dataLanguage.otherLanguage,
    // other_language_level: dataLanguage.otherLanguageLevel,
    korean_language_level: dataLanguage.koreanLanguageLevel,

    self_pr: dataWorkExperience.selfPr,
    applicant_comment: dataComment.applicantComment,
    applicant_advantage_skill_explanation: dataAdvantageSkill.applicantAdvantageSkillExplanation
  }
  return infos;
};

export const transformCandidateArrSoftware = (list, id) => {
  if (!list || list == null || list.length === 0) {
    return [
      {
        'id': { value: 'fake_1', isValid: true, message: '' },
        'candidateId': { value: id, isValid: true, message: '' },
        'software': { value: 'AutoCAD', isValid: true, message: '' },
        'year': { value: "", isValid: true, message: '' }
      },
      {
        'id': { value: 'fake_2', isValid: true, message: '' },
        'candidateId': { value: id, isValid: true, message: '' },
        'software': { value: 'Tfas', isValid: true, message: '' },
        'year': { value: "", isValid: true, message: '' }
      },
      {
        'id': { value: 'fake_3', isValid: true, message: '' },
        'candidateId': { value: id, isValid: true, message: '' },
        'software': { value: 'JW CAD', isValid: true, message: '' },
        'year': { value: "", isValid: true, message: '' }
      },
      {
        'id': { value: 'fake_4', isValid: true, message: '' },
        'candidateId': { value: id, isValid: true, message: '' },
        'software': { value: 'SOLIDWORKS', isValid: true, message: '' },
        'year': { value: "", isValid: true, message: '' }
      },
      {
      'id': { value: 'fake_5', isValid: true, message: '' },
      'candidateId': { value: id, isValid: true, message: '' },
      'software': { value: '', isValid: true, message: '' },
      'year': { value: '', isValid: true, message: '' }
      },
      {
      'id': { value: 'fake_6', isValid: true, message: '' },
      'candidateId': { value: id, isValid: true, message: '' },
      'software': { value: '', isValid: true, message: '' },
      'year': { value: '', isValid: true, message: '' }
      }
    ];
  }
  let isOther = 2;
  let fake_id = 1;
  let arr = [];
  let isAutoCAD = false, isTfas = false, isCAD = false, isSOLIDWORKS = false;
  list.map(el => {
    if (el.software.includes('AutoCAD')) isAutoCAD = true;
    if (el.software.includes('Tfas')) isTfas = true;
    if (el.software.includes('JW CAD')) isCAD = true;
    if (el.software.includes('SOLIDWORKS')) isSOLIDWORKS = true;
    if (!["AutoCAD", "Tfas", "JW CAD", "SOLIDWORKS"].includes(el.software) ){
      isOther = isOther - 1;
    }
    return arr.push({
      'id': { value: el.id, isValid: true, message: '' },
      'candidateId': { value: el.candidate_id, isValid: true, message: '' },
      'software': { value: el.software === null? "": el.software, isValid: true, message: '' },
      'year': { value: el.year === null? "": el.year, isValid: true, message: '' }
    });
  });
  if(!isAutoCAD){
    arr.push({
      'id': { value: 'fake_' + fake_id, isValid: true, message: '' },
      'candidateId': { value: id, isValid: true, message: '' },
      'software': { value: 'AutoCAD', isValid: true, message: '' },
      'year': { value: "", isValid: true, message: '' }
    });
    fake_id = fake_id + 1;
  }
  if(!isTfas){
    arr.push({
      'id': { value: 'fake_' + fake_id, isValid: true, message: '' },
      'candidateId': { value: id, isValid: true, message: '' },
      'software': { value: 'Tfas', isValid: true, message: '' },
      'year': { value: "", isValid: true, message: '' }
    });
    fake_id = fake_id + 1;
  }
  if(!isCAD){
    arr.push({
      'id': { value: 'fake_' + fake_id, isValid: true, message: '' },
      'candidateId': { value: id, isValid: true, message: '' },
      'software': { value: 'JW CAD', isValid: true, message: '' },
      'year': { value: "", isValid: true, message: '' }
    });
    fake_id = fake_id + 1;
  }
  if(!isSOLIDWORKS){
    arr.push({
      'id': { value: 'fake_' + fake_id, isValid: true, message: '' },
      'candidateId': { value: id, isValid: true, message: '' },
      'software': { value: 'SOLIDWORKS', isValid: true, message: '' },
      'year': { value: "", isValid: true, message: '' }
    });
    fake_id = fake_id + 1;
  }
  for(let i = isOther; i > 0; i--){
    arr.push({
      'id': { value: 'fake_' + fake_id, isValid: true, message: '' },
      'candidateId': { value: id, isValid: true, message: '' },
      'software': { value: '', isValid: true, message: '' },
      'year': { value: "", isValid: true, message: '' }
    });
    fake_id = fake_id + 1;
  }
  return arr;
};

export const transformCandidateArrLicense = (list, id) => {
  if (!list || list == null || list.length === 0) {
    return [{
      id: { value: 'fake_1', isValid: true, message: "" },
      candidateId: { value: id, isValid: true, message: "" },
      name: { value: '日本語能力', isValid: true, message: "" },
      level: { value: '', isValid: true, message: "" },
      year: { value: "", isValid: true, message: "" },
      month: { value: "", isValid: true, message: "" },
      },
      {
        id: { value: 'fake_2', isValid: true, message: "" },
        candidateId: { value: id, isValid: true, message: "" },
        name: { value: '英語（TOEIC）', isValid: true, message: "" },
        level: { value: '', isValid: true, message: "" },
        year: { value: "", isValid: true, message: "" },
        month: { value: "", isValid: true, message: "" },
      },
      {
        id: { value: 'fake_3', isValid: true, message: "" },
        candidateId: { value: id, isValid: true, message: "" },
        name: { value: '日本の自動車免許', isValid: true, message: "" },
        level: { value: '', isValid: true, message: "" },
        year: { value: "", isValid: true, message: "" },
        month: { value: "", isValid: true, message: "" },
      },
      {
        id: { value: 'fake_4', isValid: true, message: "" },
        candidateId: { value: id, isValid: true, message: "" },
        name: { value: 'その他', isValid: true, message: "" },
        level: { value: '', isValid: true, message: "" },
        year: { value: "", isValid: true, message: "" },
        month: { value: "", isValid: true, message: "" },
      }
    ];
  }
  let isJapanese = false, isEnglish = false, isCar = false, isOther = false;
  let arr = [];
  let fake_id = 1;
  list.map((el) => {
    if (el.name.includes('日本語能力')) isJapanese = true;
    if (el.name.includes('英語')) isEnglish = true;
    if (el.name.includes('日本の自動車免許')) isCar = true;
    if (!["日本語能力", "英語", "日本の自動車免許"].includes(el.name) ){
      isOther = true;
    }
    return arr.push({
      id: { value: el.id, isValid: true, message: "" },
      candidateId: { value: el.candidate_id, isValid: true, message: "" },
      name: { value: el.name === null? "": el.name, isValid: true, message: "" },
      level: { value: el.level === null? "": el.level, isValid: true, message: "" },
      year: { value: el.year === null? "": el.year, isValid: true, message: "" },
      month: { value: el.month === null? "": el.month, isValid: true, message: "" },
    });
  });
  if(!isJapanese){
    arr.push({
      id: { value: 'fake_' + fake_id, isValid: true, message: "" },
      candidateId: { value: id, isValid: true, message: "" },
      name: { value: '日本語能力', isValid: true, message: "" },
      level: { value: '', isValid: true, message: "" },
      year: { value: "", isValid: true, message: "" },
      month: { value: "", isValid: true, message: "" },
      });
    fake_id = fake_id + 1;
  }
  if(!isEnglish){
    arr.push({
      id: { value: 'fake_' + fake_id, isValid: true, message: "" },
      candidateId: { value: id, isValid: true, message: "" },
      name: { value: '英語（TOEIC）', isValid: true, message: "" },
      level: { value: '', isValid: true, message: "" },
      year: { value: "", isValid: true, message: "" },
      month: { value: "", isValid: true, message: "" },
      });
    fake_id = fake_id + 1;
  }
  if(!isCar){
    arr.push({
      id: { value: 'fake_' + fake_id, isValid: true, message: "" },
      candidateId: { value: id, isValid: true, message: "" },
      name: { value: '日本の自動車免許', isValid: true, message: "" },
      level: { value: '', isValid: true, message: "" },
      year: { value: "", isValid: true, message: "" },
      month: { value: "", isValid: true, message: "" },
      });
    fake_id = fake_id + 1;
  }
  if(!isOther){
    arr.push({
      id: { value: 'fake_' + fake_id, isValid: true, message: "" },
      candidateId: { value: id, isValid: true, message: "" },
      name: { value: 'その他', isValid: true, message: "" },
      level: { value: '', isValid: true, message: "" },
      year: { value: "", isValid: true, message: "" },
      month: { value: "", isValid: true, message: "" },
      });
    fake_id = fake_id + 1;
  }
  return arr;
};

export const transformCandidateArrWork = (list) => {
  if (!list) {
    return [];
  }
  return list.map((el) => {
    return {
      id: { value: el.id, isValid: true, message: "" },
      candidateId: { value: el.candidate_id, isValid: true, message: "" },
      fromMonth: { value: el.from_month === null? "": el.from_month, isValid: true, message: "" },
      fromYear: { value: el.from_year === null? "": el.from_year, isValid: true, message: "" },
      toMonth: { value: el.to_month === null? "": el.to_month, isValid: true, message: "" },
      toYear: { value: el.to_year === null? "": el.to_year, isValid: true, message: "" },
      company: { value: el.company === null? "": el.company, isValid: true, message: "" },
      contractType: { value: el.contract_type === null? "": el.contract_type, isValid: true, message: "" },
      workPlaceType: { value: el.work_place_type === null? "": el.work_place_type, isValid: true, message: "" },
      workContent: { value: el.work_content === null? "": el.work_content, isValid: true, message: "" },
      workSkill: { value: el.work_skill === null? "": el.work_skill, isValid: true, message: "" },
    };
  });
};

export const transformCandidateArrSchool = (list) => {
  if (!list) {
    return [];
  }
  return list.map((el) => {
    return {
      id: { value: el.id, isValid: true, message: "" },
      candidateId: { value: el.candidate_id, isValid: true, message: "" },
      fromMonth: { value: el.from_month === null? "": el.from_month, isValid: true, message: "" },
      fromYear: { value: el.from_year === null? "": el.from_year, isValid: true, message: "" },
      toMonth: { value: el.to_month === null? "": el.to_month, isValid: true, message: "" },
      toYear: { value: el.to_year === null? "": el.to_year, isValid: true, message: "" },
      schoolName: { value: el.school_name === null? "": el.school_name, isValid: true, message: "" },
      faculty: { value: el.faculty === null? "": el.faculty, isValid: true, message: "" },
      studySkills: { value: el.study_skills === null? "": el.study_skills, isValid: true, message: "" },
    };
  });
};

export const transformListCandidates = (listCandidates, t) => {
  if (!listCandidates) {
    return [];
  }

  //sort by id
  //listCandidates.sort(function (a, b) {
  //  return a.id - b.id;
  //});

  return listCandidates.map((item, index) => {
    let japan_language_level, status, referralIndustryType, referralCompany;
    let japaneseQualification = "",
      cadExperience = "",
      faculty = "";
    japaneseQualification = item.languages
      .filter((el) => el.name === "日本語能力")
      .map((el) => el.level);
    item.software.forEach((el) => {
      if (el.year > 0) {
        cadExperience += el.software + ",";
      }
    });
    item.schools.forEach((el) => {
      if (el.faculty != null && el.faculty.length > 0) {
        faculty += el.faculty + ",";
      }
    });
    if (cadExperience.length > 0) {
      cadExperience = cadExperience.substr(0, cadExperience.length - 1);
    } else {
      cadExperience = "-";
    }
    if (faculty.length > 0) {
      faculty = faculty.substr(0, faculty.length - 1);
    } else {
      faculty = "-";
    }
    if (japaneseQualification.length === 0) {
      japaneseQualification = "-";
    }
    switch (item.japan_language_level) {
      case 1:
        japan_language_level = `${t("label.nativeLevel")}`;
        break;
      case 2:
        japan_language_level = `${t("label.businessLevel")}`;
        break;
      case 3:
        japan_language_level = `${t("label.dailyConversationLevel")}`;
        break;
      default:
        japan_language_level = "-";
        break;
    }
    switch (item.status) {
      //case 1:
      //  status = `${t("label.waitingForRegistrationInterview")}`;
      //  break;
      //case 2:
      //  status = `${t("label.registeredInterviewCompleted")}`;
      //  break;
      //case 3:
      //  status = `${t("label.waitingForReferral")}`;
      //  break;
      case 4:
        status = `${t("label.underSelection")}`;
        break;
      case 5:
        status = `${t("label.unofficialOffer")}`;
        break;
      //case 6:
      //  status = `${t("label.applicationForStatusOfResidence")}`;
      //  break;
      //case 7:
      //  status = `${t("label.duringPreEmploymentEducation")}`;
      //  break;
      //case 8:
      //  status = `${t("label.joined")}`;
      //  break;
      case 9:
        status = `${t("label.registered")}`;
        break;
      case 10:
        status = `${t("label.interviewed")}`;
        break;
      case 11:
        status = `${t("label.atWorkAfterSales")}`;
        break;
      case 12:
        status = `${t("label.atWork")}`;
        break;
      case 13:
        status = `${t("label.cannotBeIntroduced")}`;
        break;
      default:
        status = "-";
        break;
    }
    if (null != item.introduce_company_type) {
      referralIndustryType = item.introduce_company_type.name;
    } else {
      referralIndustryType = "-";
    }
    if (null != item.company_referral) {
      referralCompany = item.company_referral;
    } else {
      referralCompany = "-";
    }

    let note = item.note || '-';
    if (note) {
      const size = 30;
      note = note.length > size ? note.slice(0, size - 1) + '...' : note;
    }

    return {
      id: item.id,
      status: status,
      name: item.name ? item.name : "-",
      createdAt: item.created_at
        ? moment(new Date(item.created_at)).format("YYYY/MM/DD")
        : "-",
      japanLanguageLevel: japan_language_level,
      japaneseQualification: japaneseQualification,
      cadExperience: cadExperience,
      major: faculty,
      referralIndustryType: referralIndustryType,
      referralCompany: referralCompany,
      note: note,
      combinedId: `${item.id}::${+ new Date()}`
    };
  });
};
