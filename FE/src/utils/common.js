import Cookies from 'js-cookie';
import axios from 'axios';
import JwtDecode from 'jwt-decode';
import moment from 'moment'


export const isAuthenticated = () => {
	return Cookies.get(process.env.REACT_APP_COOKIE_KEY);
};

export const API = axios.create({
	baseURL: `${process.env.REACT_APP_API}/api/`,
	headers: {
		'Access-Control-Allow-Origin': '*',
	}
});


export const initHeader = () => {
	let token = isAuthenticated();
	let lang = localStorage.getItem('lang');
	API.defaults.headers.common['Authorization'] = 'Bearer ' + token || '';
	API.defaults.headers.common['X-localization'] = lang ? lang.includes('-') ? lang.split('-')[0] : lang : 'en';
}

initHeader();
API.interceptors.response.use(
	response => response,
	error => {
		if (error.response.status === 401) {
			Cookies.remove(process.env.REACT_APP_COOKIE_KEY, { path: '/', domain: `${process.env.REACT_APP_URL}` });
			Cookies.remove('roles', { path: '/', domain: `${process.env.REACT_APP_API}` });
		}
		return Promise.reject(error);
	}
);

export const setCookie = (value = '', expires, key = process.env.REACT_APP_COOKIE_KEY) => {
	var now = new Date();
	now.setTime(now.getTime() + 1 * expires * 1000);
	Cookies.set(key, value, { expires: now });
}

export const logOut = () => {
	Cookies.remove(process.env.REACT_APP_COOKIE_KEY);
	Cookies.remove('roles');
}

export const getUserInfo = () => {
	let token = isAuthenticated();
	let userInfo = {};
	if (token) {
		userInfo = JwtDecode(token);
	}
	return userInfo;
}

export const formatTime = (data, format) => {
	let stringFormat = format || 'YYYY/MM/DD HH:mm:ss';
	return moment(data).format(stringFormat);
}

export const displayVND = money => {
	money = parseInt(money)
	return money.toLocaleString('vi', {style : 'currency', currency : 'VND'});
}

export const getCurrentPage = url => {
	let searchObj = new URLSearchParams(url);
	return searchObj.has('page') ? parseInt(searchObj.get('page')) : 1;
}

export const getDataSearch = url => {
	let searchArr = new URLSearchParams(url), dataSearch = {};
	if (searchArr.keys().length !== 0) {
		for(var item of searchArr.entries()) {
			dataSearch[item[0]] = item[1]
		}
	}
	return dataSearch
}

export const setParams = (localtion, callback) => {
	let searchParams = new URLSearchParams(localtion)
	for(var key of searchParams.entries()) {
		callback(key[0], key[1])
	}
}