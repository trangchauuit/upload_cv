export const transformQAResultInterview = (list, dataQuestions) => {
  if (!list) {
    return {listQuestionsShows: [], dataQA: []};
  }
  let score = 0;
  let dataQA = [];
  let listQuestionsShows = [];
  const suggests = {};
  dataQuestions.forEach((q) => {
    suggests[q.id] = q.suggest
  })
  list.forEach((item, index) => {
    score = item.answer == null? 0 : item.answer.score == null ? 0: item.answer.score;
    dataQA.push({
        id: item.id,
        question: { value: item.question_id, isValid: true, message: '' },
        answer: { value: item.answer_content == null? '' : item.answer_content, isValid: true, message: '' },
        answer_id: { value: item.answer_id, isValid: true, message: '' },
        question_kind: item.answer_id == null? '1':'2',
        answer_content: item.comment != null ? item.comment: item.answer_content == null? '': item.answer_content,
        question_content: item.question == null ? '': item.question.content,
        comment: { value: item.comment == null? '' : item.comment, isValid: true, message: '' },
        suggest: suggests[item.question_id] || '',
        score: score
    });
    listQuestionsShows[index] = dataQuestions;
  })
  return {listQuestionsShows: listQuestionsShows, dataQA: dataQA};
};

export const transformInfoCadenly = (list, dataBasicInfo, interviews) => {
  const rounds = {};
  interviews.forEach(i => {
    rounds[i.id] = i;
  });

  return {
    email: list.email || '',
    start_time: list.start_time || '',
    end_time: list.end_time || '',
    password: list.password || '',
    join_url: list.join_url || '',
    name: dataBasicInfo.name.value,
    interview: {rounds: rounds},
  };
};

export const transformQuestion = (list) => {
  if (!list) {
    return [];
  }
  let arr = [];
  list.forEach((item, index) => {
    arr.push({
      id: item.id,
      content: item.content,
      question_kind: item.question_kind,
      answers: item.answer,
      suggest: item.suggest,
    });
  });
  return arr;
};

export const transformDefaultInterviewInfo = (dataQuestions) => {
  let listQuestionsShows = [];
  listQuestionsShows[0] = dataQuestions;
  let dataInterviewInfo = {
    round: 1,
    interviewer: { value: "", isValid: true, message: "" },
    evaluation: { value: "", isValid: true, message: "" },
    youtube_url: { value: "", isValid: true, message: "" },
    dataQA: [
      {
        id: -1,
        question: { value: -1, isValid: true, message: "" },
        answer: { value: "", isValid: true, message: "" },
        question_kind: "1",
        answer_id: { value: -1, isValid: true, message: "" },
        answer_content: "",
        question_content: "",
        comment: { value: "", isValid: true, message: "" },
        suggest: "",
        score: 0,
      },
    ],
  };
  return {
    dataInterviewInfo: dataInterviewInfo,
    listQuestionsShow: listQuestionsShows,
  };
};
