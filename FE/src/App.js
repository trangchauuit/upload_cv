import React from 'react';
import { BrowserRouter as Router, Route, Switch, Redirect } from 'react-router-dom'
import Login from '../src/pages/login'
import {Error404} from '../src/pages/error'
import { isAuthenticated } from '../src/utils/common'
import LayoutView from '../src/components/LayoutView'
import MenuBar from '../src/components/menu/MenuBar'
import routers from '../src/routers'
import { Container } from 'reactstrap'
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.min.js';
import Cookies from 'js-cookie';

const AdminRouter = ({ Component, Roles, ...rest }) => (
  <Route
    {...rest}
    render={
      (props) => {
        if (isAuthenticated())
        {
          if (Roles.includes(Cookies.get('roles')))
          {
            return (<div>
              <div id="backdrop"></div>
              <div>
                <LayoutView>
                  <Container fluid>
                    <Component {...props} />
                  </Container>
                </LayoutView>
              </div>
              <MenuBar />
            </div>
            )
          }
          else
          {
            return <Redirect to={{ pathname: '/error400', state: { from: props.location } }} />
          }
        }
        else
        {
          return <Redirect to={{ pathname: '/login', state: { from: props.location } }} />
        }
      }
    }
  />
)

function App() {
  return (
    <Router>
    <Switch>
      {routers.map((route, key) => <AdminRouter exact key={route.path} path={route.path} Component={route.component} Roles={route.roles}/>)}
      <Route exact path='/login' component={Login} />
      <Route path="*" component={Error404} />
    </Switch>
  </Router>
  );
}

export default (App);
