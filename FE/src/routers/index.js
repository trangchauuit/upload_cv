import {Error400} from '../pages/error'
import ListCandidates from '../pages/resume/ListCandidates'
import Candidate from '../pages/resume/Candidate'
import ListEmployeesTimekeeping from '../pages/timekeeping/listEmployees'
import EmployeeTimekeeping from '../pages/timekeeping/employee'

// export
export default [
  //Error400
  {
    path: '/error400',
    component: Error400,
    roles: ['admin']
  },
  //Resume
  {
    path:  ["/", "/listCandidates"],
    component: ListCandidates,
    roles: ['admin']
  },
  {
    path:  '/candidate/:id',
    component: Candidate,
    roles: ['admin']
  },
  // Timekeeping
  {
    path: '/timekeeping',
    component: ListEmployeesTimekeeping,
    roles: ['admin']
  },
  {
    path: '/timekeeping/:employee_id',
    component: EmployeeTimekeeping,
    roles: ['admin']
  },
]
