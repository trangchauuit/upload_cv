import React from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button, Container, Row, Table } from 'reactstrap'
import { withTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import './listEmployees.css';

class ListEmployees extends React.PureComponent {
  constructor(props) {
    super(props);

    this.state = {
      tableHeaders: [],
    }
  }

  render() {
    return (
      <Container fluid className='timekeeping'>
        <Row className='mt-3'>
          <Table borderless>
            <thead>
              <tr>
                <th className='dept'>
                  Development Dept
                </th>
                <th>
                  行先１
                </th>
                <th>
                  行先２
                </th>
                <th>
                  行先３
                </th>
                <th>
                  帰社日
                </th>
                <th>
                  帰社時間
                </th>
                <th>
                </th>
              </tr>
            </thead>
            <tbody>
              <tr>
                <td scope="row" className='user-info-row'>
                  <div className='user-info'>
                    <img src='https://cdn-icons-png.flaticon.com/512/2202/2202112.png' />
                    <p>Vu Linh Chi</p>
                  </div>
                </td>
                <td>
                  フジキンつくば
                </td>
                <td>
                  --
                </td>
                <td>
                  --
                </td>
                <td>
                  20/02/2023
                </td>
                <td>
                  --
                </td>
                <td>
                  <Button className='btn-dark-blue btn-employee' size='md'>
                    休憩入り
                  </Button>
                </td>
              </tr>
              <tr className='separator' />
              <tr>
                <td scope="row" className='user-info-row'>
                  <div className='user-info'>
                    <img src='https://cdn-icons-png.flaticon.com/512/2202/2202112.png' />
                    <p>Nguyen Thi Phuoc</p>
                  </div>
                </td>
                <td>
                  フジキンつくば
                </td>
                <td>
                  --
                </td>
                <td>
                  --
                </td>
                <td>
                  20/02/2023
                </td>
                <td>
                  20/02/2023
                </td>
                <td>
                  <Button className='btn-light-pink btn-employee' size='md'>
                    直行
                  </Button>
                </td>
              </tr>
            </tbody>
          </Table>
        </Row>
      </Container>
    );
  }
}

export default withTranslation('common')(withRouter(ListEmployees));

