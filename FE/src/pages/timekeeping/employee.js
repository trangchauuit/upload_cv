import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css'
import { Button, Col, Container, Row, Table } from 'reactstrap'
import { withTranslation } from 'react-i18next';
import { withRouter } from 'react-router-dom';
import './employee.css';
import Circle from '../../components/common/Circle';

class Employee extends React.PureComponent {
  render() {
    return (
      <Container className='timekeeping'>
        <Row className='mt-5'>
          <span>2022年０３月２５日（金）</span>
        </Row>
        <Row className='mt-2'>
          <Col md={7} className='pl-0 timekeeping__employee_left'>
            <div className="timekeeping__datetime">
              <h2 className="timekeeping__datetime__hour">08:00</h2>
              <b className="timekeeping__datetime__date">04</b>
            </div>
            <div className='timekeeping__image'>
              <img src='https://i.imgur.com/OXDOQxZ.png' />
            </div>
            <div className='timekeeping__employee_info'>
              <p>Development Dept</p>
              <p>Nguyen Linh Chi</p>
            </div>
          </Col>
          <Col md={5} className="timekeeping__employee_right">
            <h3>あなたのステータスを選んでください</h3>
            <div className="timekeeping__status">
              <Circle borderColor="#2150B7" text="出勤" />
              <Circle borderColor="#1890FF" text="出勤" />
            </div>
            <div className="timekeeping__status">
              <Circle borderColor="#FF7474" text="出勤" />
              <Circle borderColor="#FF9C6E" text="出勤" />
            </div>
            <div className="timekeeping__status">
              <Circle borderColor="#135200" text="休憩入り" />
              <Circle borderColor="#237804" text="休憩戻り" />
            </div>
          </Col>
        </Row>
      </Container>
    )
  }
}

export default withTranslation('common')(withRouter(Employee));
