import React from 'react';
import  stylePDFArea from '../styles/stylePDFArea.js';
import { Text, View } from '@react-pdf/renderer';

class PDFNote extends React.PureComponent {

  render() {
    return(
        <>
            <View style={stylePDFArea.body} wrap={false}>
                <Text style={stylePDFArea.textP}>
                    ノート
                </Text>
                <View style={stylePDFArea.row}>
                    <Text style={stylePDFArea.textTBRL100}>		
                        {this.props.note || ""}
                    </Text>
                </View>
            </View>
        </>
    );
  }
}
export default PDFNote;
