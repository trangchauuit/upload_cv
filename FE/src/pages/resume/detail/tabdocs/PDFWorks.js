import React from 'react';
import  stylePDFWorks from '../styles/stylePDFWorks.js';
import { Text, View, Image } from '@react-pdf/renderer';
import imgCheckbox from './../../../../assets/images/imgCheckbox.png';
import imgChecked from './../../../../assets/images/imgChecked.png';

class PDFWorks extends React.PureComponent {

  truncate = (input) => input.length > 100 ? `${input.substring(0, 100)}...` : input;

  render() {
    let sizeWorks= this.props.dataWorks ? this.props.dataWorks.length: 0;
    return(
      <>
        <View style={stylePDFWorks.body} wrap={false}>
          <Text style={stylePDFWorks.textP}>
            職歴
          </Text>
          <View style={stylePDFWorks.row}>
            <Text style={stylePDFWorks.textTBRL15}>
              期間
            </Text>
            <Text style={stylePDFWorks.textTB32}>
              学校名
            </Text>
            <Text style={stylePDFWorks.textTBL35}>
              職種 (役職)
            </Text>
            <Text style={stylePDFWorks.textTBRL18}>
              業務内容{"\n"}（具体的に記載）
            </Text>
          </View>
          {this.props.dataWorks.map((work, index) => {
            let workFrom = "", workTo = "", row;
            if (work.fromMonth.value && work.fromYear.value){
              workFrom = work.fromYear.value + "年" + work.fromMonth.value + "月";
            }
            if (work.toMonth.value && work.toYear.value){
              workTo = work.toYear.value + "年" + work.toMonth.value + "月";
            }

            let fulltimeEmployeeDiv = (('') + work.contractType.value) === '4'? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
            let contractEmployeeDiv = (('') + work.contractType.value) === '3'? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
            let partimeJobDiv = (('') + work.contractType.value) === '1'? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
            let temporaryStaff = (('') + work.contractType.value) === '2'? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
            let internship = (('') + work.contractType.value) === '5'? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
            let volunteer = (('') + work.contractType.value) === '6'? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
            let enlistment = (('') + work.contractType.value) === '7'? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
            let nihon = (('') + work.workPlaceType.value) === '1'? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
            let vietnam = (('') + work.workPlaceType.value) === '2'? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
            let foreignCapital = (('') + work.workPlaceType.value) === '3'? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
            if (sizeWorks !== (index + 1)){
              row =   <View style={stylePDFWorks.row}>
                <View style={stylePDFWorks.colDotted15}>
                  <Text style={stylePDFWorks.textPCenter}>
                    {workFrom} ～ {workTo}
                  </Text>
                </View>
                <View style={stylePDFWorks.colBDotted32}>
                  <View style={stylePDFWorks.row}>
                    <Text style={stylePDFWorks.textBDotted100}>
                      {work.company.value}
                    </Text>
                  </View>
                  <View style={stylePDFWorks.row}>
                    <Text style={stylePDFWorks.textBDotted100}>
                      {fulltimeEmployeeDiv}正社員    {contractEmployeeDiv}契約社員{"\n"}
                      {temporaryStaff}派遣社員  {partimeJobDiv}アルバイト{"\n"}
                      {enlistment}入隊{'   '}   {internship}インターン{'\n'}
                      {volunteer}ボランティア
                    </Text>
                  </View>
                  <View style={stylePDFWorks.row}>
                    <Text style={stylePDFWorks.text100}>
                      {nihon}日本 {vietnam}ベトナム {foreignCapital}その他
                    </Text>
                  </View>
                </View>
                <View style={stylePDFWorks.colBLDotted35}>
                  <View style={stylePDFWorks.cardBody}>
                    <View style={stylePDFWorks.flexItem}>
                      <Text style={stylePDFWorks.textPLeft}>
                        {work.workContent.value}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={stylePDFWorks.colRBLDotted18}>
                  <Text style={stylePDFWorks.textPLeft}>
                    {this.truncate(work.workSkill.value || '')}
                  </Text>
                </View>
              </View>;
            }else{
              row =   <View style={stylePDFWorks.row}>
                <View style={stylePDFWorks.col15}>
                  <Text style={stylePDFWorks.textPCenter}>
                    {workFrom} ～ {workTo}
                  </Text>
                </View>
                <View style={stylePDFWorks.colB32}>
                  <View style={stylePDFWorks.row}>
                    <Text style={stylePDFWorks.textBDotted100}>
                      {work.company.value}
                    </Text>
                  </View>
                  <View style={stylePDFWorks.row}>
                    <Text style={stylePDFWorks.textBDotted100}>
                      {fulltimeEmployeeDiv}正社員    {contractEmployeeDiv}契約社員{"\n"}
                      {temporaryStaff}派遣社員  {partimeJobDiv}アルバイト{"\n"}
                      {enlistment}入隊{'   '}   {internship}インターン{'\n'}
                      {volunteer}ボランティア
                    </Text>
                  </View>
                  <View style={stylePDFWorks.row}>
                    <Text style={stylePDFWorks.text100}>
                      {nihon}日本 {vietnam}ベトナム {foreignCapital}その他
                    </Text>
                  </View>
                </View>
                <View style={stylePDFWorks.colBL35}>
                  <View style={stylePDFWorks.cardBody}>
                    <View style={stylePDFWorks.flexItem}>
                      <Text style={stylePDFWorks.textPLeft}>
                        {work.workContent.value}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={stylePDFWorks.colRBL18}>
                  <Text style={stylePDFWorks.textPLeft}>
                    {this.truncate(work.workSkill.value || '')}
                  </Text>
                </View>
              </View>;
            }

            return (
              <React.Fragment key={work.id.value}>
                {row}
              </React.Fragment>
            );
          })}

        </View>
        </>
    );
  }
}
export default PDFWorks;
