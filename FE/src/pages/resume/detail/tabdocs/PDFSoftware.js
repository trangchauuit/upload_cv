import React from 'react';
import  stylePDFSoftware from '../styles/stylePDFSoftware.js';
import { Text, View, Image } from '@react-pdf/renderer';
import imgCheckbox from './../../../../assets/images/imgCheckbox.png';
import imgChecked from './../../../../assets/images/imgChecked.png';

class PDFSoftware extends React.PureComponent {
  constructor() {
    super();
    Array.prototype.each_slice = function (size, callback){
      for (var i = 0, l = this.length; i < l; i += size){
        callback.call(this, this.slice(i, i + size));
      }
    };
  }
  otherTools = () => {
    const tools = ['AutoCAD', 'Tfas', 'JW CAD', 'SOLIDWORKS']
    const otherTools = this.props.dataSoftware.filter(sw => !tools.includes(sw.software.value));
    let numOfTools = otherTools.length;
    const ids = otherTools.map(e => +e.id.value)
    const greatestId = Math.max(...ids);
    if (numOfTools % 2 !== 0){
      otherTools.push({id: { value: greatestId + numOfTools }, software: { value: '' }, year: { value: '' } })
      numOfTools++;
    }
    return otherTools;
  }

  renderTools = () => {
    const components = []
    let page = 1;
    this.otherTools().each_slice(2, function(tools) {
      let leftStyle, rightStyle;
      leftStyle = {
        width: '30%',
        flexDirection: 'column',
        borderTop: page === 1 ? 1 : 0,
        borderBottom: 1,
        borderBottomStyle: page === 3 ? "solid" : "dotted",
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        textAlign: 'left',
      }
      rightStyle =  {
        width: '20%',
        flexDirection: 'column',
        borderTop: page === 1 ? 1 : 0,
        borderRight: 1,
        borderRightStyle: "solid",
        borderBottom: 1,
        borderBottomStyle: page === 3 ? "solid" : "dotted",
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        textAlign: 'right',
      }
      components.push(<View key={Math.random()} style={stylePDFSoftware.row}>
        {tools.map((t) => <React.Fragment key={t.id.value}>
          <Text style={leftStyle}>
            {t.software.value}
          </Text>
          <Text style={rightStyle}>
            ({t.year.value || '年'})
          </Text>
        </React.Fragment>)}
      </View>
      )
      page++;
    })
    return components;
  }
  render() {
    let autoCADDiv, tfasDiv, jwCADDiv, solidworksDiv, otherTool1Value, otherTool2Value;
    let yearAutoCAD =-1, yearTfas=-1, yearJwCAD=-1, yearSolidworks=-1, yearOtherTool1=-1, yearOtherTool2=-1;

    return(
      <>
        <View style={stylePDFSoftware.body} wrap={false}>
          <Text style={stylePDFSoftware.textP}>
            使用可能ツール・ソフトウェア等
          </Text>
          {this.props.dataSoftware.forEach((software) => {
            let nameSoftware = software.software.value || "";
            let year = software.year.value || '年';
            if (nameSoftware.indexOf(("AutoCAD")) !== -1){
              autoCADDiv = (('' + software.year.value) !== '0' && ('' + software.year.value) !== '-1' && ('' + software.year.value) !== '')? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              yearAutoCAD = year;
            }else if (nameSoftware.includes("Tfas")){
              tfasDiv = (('' + software.year.value) !== '0' && ('' + software.year.value) !== '-1' && ('' + software.year.value) !== '')? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              yearTfas = year;
            }else if (nameSoftware.includes("JW CAD")){
              jwCADDiv = (('' + software.year.value) !== '0' && ('' + software.year.value) !== '-1' && ('' + software.year.value) !== '')? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              yearJwCAD = year;
            }else if (nameSoftware.includes("SOLIDWORKS")){
              solidworksDiv = (('' + software.year.value) !== '0' && ('' + software.year.value) !== '-1' && ('' + software.year.value) !== '')? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              yearSolidworks = year;
            }
          })}
          <View style={stylePDFSoftware.row}>
            <View style={{...stylePDFSoftware.col20, borderBottom: 0}}>
              <Text style={stylePDFSoftware.textT}>
                CAD
              </Text>
            </View>
            <View style={stylePDFSoftware.col80}>
              <View style={stylePDFSoftware.row}>
                <Text style={stylePDFSoftware.textTBDotted20}>
                  {autoCADDiv} AutoCAD
                </Text>
                <Text style={stylePDFSoftware.textTBRDotted30}>
                  ({yearAutoCAD < 0? '年' : yearAutoCAD})
                </Text>
                <Text style={stylePDFSoftware.textTBDotted20}>
                  {tfasDiv} Tfas
                </Text>
                <Text style={stylePDFSoftware.textTBR30}>
                  ({yearTfas < 0? '年' : yearTfas})
                </Text>
              </View>
              <View style={stylePDFSoftware.row}>
                <Text style={{...stylePDFSoftware.textBDotted30, borderBottom: 0}}>
                  {jwCADDiv} JW CAD
                </Text>
                <Text style={{...stylePDFSoftware.textBRDotted20, borderBottom: 0}}>
                  ({yearJwCAD < 0? '年' : yearJwCAD})
                </Text>
                <Text style={{...stylePDFSoftware.textBDotted30, borderBottom: 0}}>
                  {solidworksDiv} SOLIDWORKS
                </Text>
                <Text style={{...stylePDFSoftware.textBR20, borderBottom: 0}}>
                  ({yearSolidworks < 0? '年' : yearSolidworks})
                </Text>
              </View>
            </View>
          </View>
          <View style={stylePDFSoftware.row}>
            <View style={{...stylePDFSoftware.col20, borderBottom: 1, borderBottomStyle: "solid"}}>
              <Text style={{ ...stylePDFSoftware.textT }}>
                その他
              </Text>
            </View>
            <View style={stylePDFSoftware.col80}>
              {this.renderTools()}
            </View>
          </View>
        </View>
      </>
    );
  }
}
export default PDFSoftware;
