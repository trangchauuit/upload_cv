import React from 'react';
import  styleInterviewInformation from '../styles/styleInterviewInformation.js';
import { Text, View } from '@react-pdf/renderer';
import moment from 'moment';

class PDFInterviewInformation extends React.PureComponent {
  render() {
    const {dataInforCalendly, dataInterviewInfo} = this.props;
    let dataSelectbox = dataInterviewInfo.dataQA.filter(obj => (obj.question_kind + "") === "2");
    let dataSelectboxSize = dataSelectbox.length;
    let dataInput = dataInterviewInfo.dataQA.filter(obj => (obj.question_kind + "") === "1");
    let dataInputSize = dataInput.length;
    let totalScore = 0;
    return(
        <>
            <View style={styleInterviewInformation.body}>
                <Text style={styleInterviewInformation.textP}>
                    面接内容（面接担当者記入）
                </Text>
                <View style={styleInterviewInformation.row}>
                    <View style={styleInterviewInformation.col15}>
                        <Text style={styleInterviewInformation.textP}>		
                            面接日
                        </Text>
                    </View>
                    <View style={styleInterviewInformation.col25}>
                        <Text style={styleInterviewInformation.textP}>	
                            {dataInforCalendly.start_time? moment.utc(dataInforCalendly.start_time).local().format('YYYY/MM/DD'):''}
                        </Text>
                    </View>
                    <View style={styleInterviewInformation.col15Left}>
                        <Text style={styleInterviewInformation.textP}>		
                            面接担当者
                        </Text>
                    </View>
                    <View style={styleInterviewInformation.col45}>
                        <Text style={styleInterviewInformation.textP}>	
                            {dataInterviewInfo.interviewer.value == null ? '' : dataInterviewInfo.interviewer.value}
                        </Text>
                    </View>
                </View>

                <View style={styleInterviewInformation.rowMargin}>
                    <Text style={styleInterviewInformation.textHeaderNo}>
                        No.
                    </Text>
                    <Text style={styleInterviewInformation.textHeaderQuestion}>
                        質問
                    </Text>
                    <Text style={styleInterviewInformation.textHeaderSuggest}>
                        評価
                    </Text>
                    <Text style={styleInterviewInformation.textHeaderAnswer}>
                        回答
                    </Text>
                </View>
                {dataSelectbox.map((item, index) => {
                    totalScore += parseInt(item.score);
                    return ( 
                    <React.Fragment key={index}>
                        <View style={styleInterviewInformation.row}>
                            <Text style={(dataSelectboxSize === index) ? styleInterviewInformation.textNoDotted: styleInterviewInformation.textNo}>
                                {index + 1}
                            </Text>
                            <Text style={(dataSelectboxSize === index) ? styleInterviewInformation.textQuestionDotted: styleInterviewInformation.textQuestion}>
                                {item.question_content || ''}
                            </Text>
                            <Text style={(dataSelectboxSize === index) ? styleInterviewInformation.textSuggestDotted: styleInterviewInformation.textSuggest}>
                                {item.suggest || ''}
                            </Text>
                            <Text style={(dataSelectboxSize === index) ? styleInterviewInformation.textScoreDotted: styleInterviewInformation.textScore}>
                                {item.score || ''}
                            </Text>
                            <Text style={(dataSelectboxSize === index) ? styleInterviewInformation.textAnswerDotted:  styleInterviewInformation.textAnswer}>
                                {item.answer_content}
                            </Text>
                        </View>
                    </React.Fragment>);
                })}

                <View style={styleInterviewInformation.row}>
                    <Text style={styleInterviewInformation.textNoTotal}>
                    </Text>
                    <Text style={styleInterviewInformation.textQuestionTotal}>
                    </Text>
                    <Text style={styleInterviewInformation.textSuggestTotal}>
                        合計
                    </Text>
                    <Text style={styleInterviewInformation.textScoreTotal}>
                        {totalScore}
                    </Text>
                    <Text style={styleInterviewInformation.textAnswerTotal}>
                        点
                    </Text>
                </View>

                <View style={styleInterviewInformation.row}>
                    <Text style={styleInterviewInformation.textHeaderNo}>
                        No.
                    </Text>
                    <Text style={styleInterviewInformation.textHeaderQuestion}>
                        質問
                    </Text>
                    <Text style={styleInterviewInformation.textHeaderSuggest}>
                        評価
                    </Text>
                    <Text style={styleInterviewInformation.textHeaderAnswer}>
                        回答
                    </Text>
                </View>
                {dataInput.map((item, index) => {
                    return ( 
                    <React.Fragment key={index}>
                        <View style={styleInterviewInformation.row}>
                            <Text style={(dataInputSize === index) ? styleInterviewInformation.textNoDotted: styleInterviewInformation.textNo}>
                                {index + 1}
                            </Text>
                            <Text style={(dataInputSize === index) ? styleInterviewInformation.textQuestionDotted: styleInterviewInformation.textQuestion}>
                                {item.question_content || ''}
                            </Text>
                            <Text style={(dataInputSize === index) ? styleInterviewInformation.textSuggestDotted: styleInterviewInformation.textSuggest}>
                                {item.suggest || ''}
                            </Text>
                            <Text style={(dataInputSize === index) ? styleInterviewInformation.textAnswerDottedInput: styleInterviewInformation.textAnswerInput}>
                                {item.answer_content || ''}
                            </Text>
                        </View>
                    </React.Fragment>);
                })}

                <View style={styleInterviewInformation.rowMargin}>
                    <Text style={styleInterviewInformation.textP}>
                        面接担当者からのコメント
                    </Text>
                </View>
                <View style={styleInterviewInformation.row}>
                    <Text style={styleInterviewInformation.textTBRL100}>		
                        {dataInterviewInfo.evaluation.value || ""}
                    </Text>
                </View>
            </View>          
        </>
    );
  }
}
export default PDFInterviewInformation;