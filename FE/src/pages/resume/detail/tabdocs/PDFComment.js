import React from 'react';
import  stylePDFArea from '../styles/stylePDFArea.js';
import { Text, View } from '@react-pdf/renderer';

class PDFComment extends React.PureComponent {

  render() {
    return(
        <>
            <View style={stylePDFArea.body} wrap={false}>
                <Text style={stylePDFArea.textP}>
                    本人希望記入欄{"\n"}
                    (給料・職種・勤務時間・勤務地その他について希望があれば記入）
                </Text>
                <View style={stylePDFArea.row}>
                    <Text style={stylePDFArea.textTBRL100}>		
                        {this.props.dataComment.applicantComment || ""}
                    </Text>
                </View>
            </View>
        </>
    );
  }
}
export default PDFComment;