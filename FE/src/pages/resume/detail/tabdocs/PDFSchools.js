import React from 'react';
import  stylePDFSchools from '../styles/stylePDFSchools';
import { Text, View } from '@react-pdf/renderer';

class PDFSchools extends React.PureComponent {

  render() {
    let sizeEducations = this.props.dataSchools ? this.props.dataSchools.length: 0;
    return(
      <>
        <View style={stylePDFSchools.body} wrap={false}>
          <Text style={stylePDFSchools.textP}>
            学歴
          </Text>
          <View style={stylePDFSchools.row}>
            <Text style={stylePDFSchools.textTBRL15}>
              期間
            </Text>
            <Text style={stylePDFSchools.textTB29}>
              学校名
            </Text>
            <Text style={stylePDFSchools.textTBL28}>
              学部・専攻 ・GPA
            </Text>
            <Text style={stylePDFSchools.textTBRL28}>
              習得技術など
            </Text>
          </View>
          {this.props.dataSchools.map((education, index) => {
            let admissionPeriod = "", graduationPeriod = "", row;
            if (education.fromMonth.value && education.fromYear.value){
              admissionPeriod = education.fromYear.value + "年" + education.fromMonth.value + "月";
            }
            if (education.toMonth.value && education.toYear.value){
              graduationPeriod = education.toYear.value + "年" + education.toMonth.value + "月";
            }
            if (sizeEducations !== (index + 1)){
              row = <View style={stylePDFSchools.row}>
                <Text style={stylePDFSchools.textBRLDotted15}>
                  {admissionPeriod} ～ {graduationPeriod}
                </Text>
                <Text style={stylePDFSchools.textBDotted29}>
                  {education.schoolName.value}
                </Text>
                <Text style={stylePDFSchools.textBLDotted28_1}>
                  {education.faculty.value}
                </Text>
                <Text style={stylePDFSchools.textBRLDotted28}>
                  {education.studySkills.value}
                </Text>
              </View>;
            }else{
              row = <View style={stylePDFSchools.row}>
                <Text style={stylePDFSchools.textBRL15}>
                  {admissionPeriod} ～ {graduationPeriod}
                </Text>
                <Text style={stylePDFSchools.textB29}>
                  {education.schoolName.value}
                </Text>
                <Text style={stylePDFSchools.textBLDotted28_2}>
                  {education.faculty.value}
                </Text>
                <Text style={stylePDFSchools.textBRL28}>
                  {education.studySkills.value}
                </Text>
              </View>;
            }

            return (
              <React.Fragment key={education.id.value}>
                {row}
              </React.Fragment>
            );
          })}
        </View>
        </>
    );
  }
}
export default PDFSchools;
