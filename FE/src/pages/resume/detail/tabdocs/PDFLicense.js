import React from 'react';
import  stylePDFSoftware from '../styles/stylePDFSoftware';
import  stylePDFLicense from '../styles/stylePDFLicense';
import moment from 'moment';
import { Text, View, Image } from '@react-pdf/renderer';
import imgCheckbox from './../../../../assets/images/imgCheckbox.png';
import imgChecked from './../../../../assets/images/imgChecked.png';

class PDFLicense extends React.PureComponent {
  render() {
    const components = [];

    return(
      <>
        <View style={stylePDFLicense.body}>
          <Text style={stylePDFLicense.textP}>
            保有資格・免許等
          </Text>
          {this.props.dataLicense.map((license) => {
            let nameLicense = license.name.value;
            let getTime = "";
            if (license.year.value && license.month.value && (license.year.value + "" !== "0" && (license.month.value +"" !== "0"))){
              getTime = license.year.value + "/" + license.month.value;
            }

            if (nameLicense.indexOf(("日本語能力")) !== -1){
              let japanN1Div = license.level.value === "N1"? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              let japanN2Div = license.level.value === "N2"? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              let japanN3Div = license.level.value === "N3"? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              let japanN4Div = license.level.value === "N4"? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              let japanN5Div = license.level.value === "N5"? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              return (
                <React.Fragment key={license.id.value}>
                  <View style={stylePDFLicense.row}>
                    <Text style={stylePDFLicense.textTRBL20}>
                      日本語
                    </Text>
                    <Text style={stylePDFLicense.textTB13}>
                      {japanN1Div} N1
                    </Text>
                    <Text style={stylePDFLicense.textTB13}>
                      {japanN2Div} N2
                    </Text>
                    <Text style={stylePDFLicense.textTB13}>
                      {japanN3Div} N3
                    </Text>
                    <Text style={stylePDFLicense.textTB13}>
                      {japanN4Div} N4
                    </Text>
                    <Text style={stylePDFLicense.textTB13}>
                      {japanN5Div} N5
                    </Text>
                    <Text style={stylePDFLicense.textTRB000}>
                      ({getTime !== "" ? moment(new Date(getTime)).format('YYYY/MM') : "年月"})
                    </Text>
                  </View>
                </React.Fragment>
              )
            }else if (nameLicense.includes("TOEIC")){
              return (
                <React.Fragment key={license.id.value}>
                  <View style={stylePDFLicense.row}>
                    <Text style={stylePDFLicense.textRBDottedL20}>
                      英語（TOEIC）
                    </Text>
                    <Text style={stylePDFLicense.textBDotted54}>
                      {license.level.value} 点
                    </Text>
                    <Text style={stylePDFLicense.textRBDotted26}>
                      ({getTime  !== "" ? moment(new Date(getTime)).format('YYYY/MM') : "年月"})
                    </Text>
                  </View>
                </React.Fragment>
              )
            }else if (nameLicense.includes("日本の自動車免許")){
              let carLicenseNoDiv = ['無/Không', '無'].includes(license.level.value)? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              let carLicenseYesDiv =  ['有', '有/Có'].includes(license.level.value) ? <Image src={imgChecked}/>:<Image src={imgCheckbox}/>;
              return (
                <React.Fragment key={license.id.value}>
                  <View style={stylePDFLicense.row}>
                    <Text style={stylePDFLicense.textRBDottedL20}>
                      日本の自動車免許
                    </Text>
                    <Text style={stylePDFLicense.textB27}>
                      {carLicenseYesDiv} 有/Có
                    </Text>
                    <Text style={stylePDFLicense.textB27}>
                      {carLicenseNoDiv} 無/Không
                    </Text>
                    <Text style={stylePDFLicense.textRBDotted26}>
                      ({getTime  !== "" ? moment(new Date(getTime)).format('YYYY/MM') : "年月"})
                    </Text>
                  </View>
                </React.Fragment>
              )
            }else{
              components.push(license)
            }
          })}
          {
            components.length > 0 ?
              <View style={stylePDFSoftware.row}>
                <View style={{...stylePDFSoftware.col20, borderBottom: 1, borderBottomStyle: "solid"}}>
                  <Text style={{...stylePDFSoftware.textT}}>
                    その他
                  </Text>
                </View>
                <View style={stylePDFSoftware.col80}>
                  {components.map((license, i) =>
                  {
                      let getTime = "";
                      if (license.year.value && license.month.value && (license.year.value + "" !== "0" && (license.month.value +"" !== "0"))){
                        getTime = license.year.value + "/" + license.month.value;
                      }
                      let leftStyle, rightStyle;
                      leftStyle = {
                        width: '50%',
                        flexDirection: 'column',
                        borderTop: i === 0 ? 1 : 0,
                        borderBottom: 1,
                        borderBottomStyle: i !== components.length - 1 ? 'dotted' : 'solid',
                        padding: 5,
                        fontSize: 12,
                        fontFamily: 'Arial Unicode MS',
                        textAlign: 'center',
                      }
                      rightStyle =  {
                        width: '50%',
                        flexDirection: 'column',
                        borderTop: i === 0 ? 1 : 0,
                        borderRight: 1,
                        borderRightStyle: "solid",
                        borderBottom: 1,
                        padding: 5,
                        fontSize: 12,
                        fontFamily: 'Arial Unicode MS',
                        borderBottomStyle: i !== components.length - 1 ? 'dotted' : 'solid',
                        textAlign: 'right',
                      }
                      return <View key={license.id.value} style={stylePDFSoftware.row}>
                        <Text style={leftStyle}>
                          {license.level.value}
                        </Text>
                        <Text style={rightStyle}>
                          ({getTime  !== "" ? moment(new Date(getTime)).format('YYYY/MM') : "年月"})
                        </Text>
                      </View>
                    }
                  )}
                </View>
              </View>

              : null
          }
        </View>
      </>
    );
  }
}
export default PDFLicense;
