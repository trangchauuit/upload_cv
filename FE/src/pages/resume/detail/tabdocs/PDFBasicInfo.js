import React from 'react';
import { Image, Text, View } from '@react-pdf/renderer';
import  stylePDFBasicInfo from '../styles/stylePDFBasicInfo';
import moment from 'moment';
import imgCheckbox from './../../../../assets/images/imgCheckbox.png';
import imgChecked from './../../../../assets/images/imgChecked.png';
import userProfile from './../../../../assets/images/User-Profile-Transparent.png';

class PDFBasicInfo extends React.PureComponent {

  drawCheckBox = (value, valueCompare) => {
    var tagDiv;
    if (('' + value) === ('' + valueCompare)) {
      tagDiv = <Image src={imgChecked}/>;
    }else{
      tagDiv = <Image src={imgCheckbox}/>;
    }
    return tagDiv;
  }

  render() {
    const { dataBasicInfo } = this.props;

    let imgMaleDiv = this.drawCheckBox(dataBasicInfo.gender.value, 0);
    let imgFemaleDiv = this.drawCheckBox(dataBasicInfo.gender.value, 1);

    let imgMarriedNoDiv = this.drawCheckBox(dataBasicInfo.marriedStatus.value, 0);
    let imgMarriedYesDiv = this.drawCheckBox(dataBasicInfo.marriedStatus.value, 1);

    let imgJPWorkNoDiv = this.drawCheckBox(dataBasicInfo.japanVisa.value, 0);
    let imgJPWorkYesDiv = this.drawCheckBox(dataBasicInfo.japanVisa.value, 1);
    const month = +dataBasicInfo.dobMonth.value > 9 ? dataBasicInfo.dobMonth.value : `0${dataBasicInfo.dobMonth.value}`;
    const day = +dataBasicInfo.dobDay.value > 9 ? dataBasicInfo.dobDay.value : `0${dataBasicInfo.dobDay.value}`;
    /*
    const birthday =  this.props.dataBasicInfo.birthday;
    let  birthdayDiv;
    if (birthday){
      var dateObj = new Date(birthday);
      birthdayDiv = dateObj.getFullYear() + "年" + (dateObj.getMonth() + 1)+ "月" + dateObj.getDate() + "日生";
    }*/
    return(
        <>
          <View style={stylePDFBasicInfo.body}>
            <View style={stylePDFBasicInfo.row}>
              <View style={stylePDFBasicInfo.col80}>
                <View style={stylePDFBasicInfo.row}>
                  <Text style={stylePDFBasicInfo.textTRL15}>
                    ﾌﾘｶﾞﾅ
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR50}>
                    {dataBasicInfo.nameKana.value}
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR15}>
                    生年月日
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR20}>
                    {`${dataBasicInfo.dobYear.value}/${month}/${day}`}
                  </Text>
                </View>
                <View style={stylePDFBasicInfo.row}>
                  <Text style={stylePDFBasicInfo.textTRL15}>
                    氏名
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR50}>
                    {dataBasicInfo.name.value}
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR15}>
                    年齢
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR20}>
                    {dataBasicInfo.age.value} 歳
                  </Text>
                </View>
                <View style={stylePDFBasicInfo.row}>
                  <Text style={stylePDFBasicInfo.textTRL15}>
                    国籍
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR50}>
                    {dataBasicInfo.country.value}
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR15}>
                    性別
                  </Text>
                  <Text style={stylePDFBasicInfo.textT10}>
                    {imgMaleDiv} 男
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR10}>
                    {imgFemaleDiv} 女
                  </Text>
                </View>
                <View style={stylePDFBasicInfo.row}>
                  <Text style={stylePDFBasicInfo.textTRL15}>
                    携帯電話
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR50}>
                    {dataBasicInfo.phone.value}
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR15}>
                    配偶者
                  </Text>
                  <Text style={stylePDFBasicInfo.textT10}>
                    {imgMarriedYesDiv} 有
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR10}>
                    {imgMarriedNoDiv} 無
                  </Text>
                </View>
                <View style={stylePDFBasicInfo.row}>
                  <View style={stylePDFBasicInfo.col15}>
                    <Text style={stylePDFBasicInfo.textT}>
                      現住所
                    </Text>
                  </View>
                  <View style={stylePDFBasicInfo.col85}>
                    <View style={stylePDFBasicInfo.row}>
                      <Text style={stylePDFBasicInfo.textT25}>
                        〒 {dataBasicInfo.zipCode.value}
                      </Text>
                      <Text style={stylePDFBasicInfo.textTR75Right}>
                      </Text>
                    </View>
                    <View style={stylePDFBasicInfo.row}>
                      <Text style={stylePDFBasicInfo.textR100}>
                        {dataBasicInfo.currentAddress.value}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={stylePDFBasicInfo.row}>
                  <View style={stylePDFBasicInfo.col15}>
                    <Text style={stylePDFBasicInfo.textT}>
                      住所
                    </Text>
                  </View>
                  <View style={stylePDFBasicInfo.col85}>
                    <View style={stylePDFBasicInfo.row}>
                      <Text style={stylePDFBasicInfo.textT25}>
                        〒 {dataBasicInfo.zipCodeContact.value}
                      </Text>
                      <Text style={stylePDFBasicInfo.textTR75Right}>
                      </Text>
                    </View>
                    <View style={stylePDFBasicInfo.row}>
                      <Text style={stylePDFBasicInfo.textR100}>
                        {dataBasicInfo.contactAddress.value}
                      </Text>
                    </View>
                  </View>
                </View>
                <View style={stylePDFBasicInfo.row}>
                  <Text style={stylePDFBasicInfo.textTRL15}>
                    Email
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR50}>
                    {dataBasicInfo.email.value}
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR35}>
                    日本就労ビザ
                  </Text>
                  <Text style={stylePDFBasicInfo.textT12}>
                    {imgJPWorkYesDiv} 有
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR12}>
                    {imgJPWorkNoDiv} 無
                  </Text>
                </View>
                <View style={stylePDFBasicInfo.row}>
                  <Text style={stylePDFBasicInfo.textTRL15}>
                    ビザ期限
                  </Text>
                  <Text style={stylePDFBasicInfo.textTR110}>
                    {dataBasicInfo.japanVisaDateExpired.value ? moment(new Date(dataBasicInfo.japanVisaDateExpired.value)).format('YYYY/MM/DD') : ""}
                  </Text>
                </View>
                <View style={stylePDFBasicInfo.row}>
                  <Text style={stylePDFBasicInfo.textTRBL15}>
                    ビザ種類
                  </Text>
                  <Text style={stylePDFBasicInfo.textTRB110}>
                    {dataBasicInfo.japanVisaType.value}
                  </Text>
                </View>
              </View>
              <View style={stylePDFBasicInfo.col20}>
                <Image
                  src={dataBasicInfo.avatar || userProfile}
                  style={stylePDFBasicInfo.image}
                />
              </View>
            </View>
        </View>
        </>
    );
  }
}
export default PDFBasicInfo;
