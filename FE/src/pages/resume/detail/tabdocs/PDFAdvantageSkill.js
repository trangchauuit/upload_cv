import React from 'react';
import  stylePDFArea from '../styles/stylePDFArea.js';
import { Text, View } from '@react-pdf/renderer';

class PDFAdvantageSkill extends React.PureComponent {

  render() {
    return(
        <>
            <View style={stylePDFArea.body} wrap={false}>
                <Text style={stylePDFArea.textP}>
                    趣味・特技
                </Text>
                <View style={stylePDFArea.row}>
                    <Text style={stylePDFArea.textTBRL100}>		
                        {this.props.dataAdvantageSkill.applicantAdvantageSkillExplanation || ""}
                    </Text>
                </View>
            </View>
        </>
    );
  }
}
export default PDFAdvantageSkill;