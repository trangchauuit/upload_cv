import React from 'react';
import  stylePDFLanguage from '../styles/stylePDFLanguage';
import { Text, View, Image } from '@react-pdf/renderer';
import imgCheckbox from './../../../../assets/images/imgCheckbox.png';
import imgChecked from './../../../../assets/images/imgChecked.png';

class PDFLanguage extends React.PureComponent {

  levelLanguage = (value, valueCompare) => {
    var tagDiv;
    if (('' + value) === ('' + valueCompare)) {
      tagDiv = <Image src={imgChecked}/>;
    }else{
      tagDiv = <Image src={imgCheckbox}/>;
    }
    return tagDiv;
  }

  render() {
    const japanLanguageLevel = this.props.dataLanguage.japanLanguageLevel;
    let japanLanguageLevel1Div = this.levelLanguage(japanLanguageLevel, 1);
    let japanLanguageLevel2Div = this.levelLanguage(japanLanguageLevel, 2);
    let japanLanguageLevel3Div = this.levelLanguage(japanLanguageLevel, 3);

    const englishLanguageLevel = this.props.dataLanguage.englishLanguageLevel;
    let englishLanguageLevel1Div = this.levelLanguage(englishLanguageLevel, 1);
    let englishLanguageLevel2Div = this.levelLanguage(englishLanguageLevel, 2);
    let englishLanguageLevel3Div = this.levelLanguage(englishLanguageLevel, 3);

    const koreanLanguageLevel = this.props.dataLanguage.koreanLanguageLevel;
    let koreanLanguageLevel1Div = this.levelLanguage(koreanLanguageLevel, 1);
    let koreanLanguageLevel2Div = this.levelLanguage(koreanLanguageLevel, 2);
    let koreanLanguageLevel3Div = this.levelLanguage(koreanLanguageLevel, 3);

    return(
      <>
        <View style={stylePDFLanguage.body} wrap={false}>
          <Text style={stylePDFLanguage.textP}>
            言語
          </Text>
          <View style={stylePDFLanguage.row}>
            <Text style={stylePDFLanguage.textTRL14}>
            </Text>
            <Text style={stylePDFLanguage.textT28}>
              日本語
            </Text>
            <Text style={stylePDFLanguage.textTL28}>
              英語
            </Text>
            <Text style={stylePDFLanguage.textTRL30}>
              韓国語
            </Text>
          </View>
          <View style={stylePDFLanguage.row}>
            <View style={stylePDFLanguage.col14}>
              <Text style={stylePDFLanguage.textT}>
                会話レベル
              </Text>
            </View>
            <View style={stylePDFLanguage.col86}>
              <View style={stylePDFLanguage.row}>
                <Text style={stylePDFLanguage.textTB32}>
                  {japanLanguageLevel1Div}日常会話レベル
                </Text>
                <Text style={stylePDFLanguage.textTBL32}>
                  {englishLanguageLevel1Div}日常会話レベル
                </Text>
                <Text style={stylePDFLanguage.textTRBL35}>
                  {koreanLanguageLevel1Div}日常会話レベル
                </Text>
              </View>
              <View style={stylePDFLanguage.row}>
                <Text style={stylePDFLanguage.textBDotted32}>
                  {japanLanguageLevel2Div}ビジネスレベル
                </Text>
                <Text style={stylePDFLanguage.textBLDotted32}>
                  {englishLanguageLevel2Div}ビジネスレベル
                </Text>
                <Text style={stylePDFLanguage.textRBLDotted35}>
                  {koreanLanguageLevel2Div}ビジネスレベル
                </Text>
              </View>
              <View style={stylePDFLanguage.row}>
                <Text style={stylePDFLanguage.textB32}>
                  {japanLanguageLevel3Div}ネイティブレベル
                </Text>
                <Text style={stylePDFLanguage.textBL32}>
                  {englishLanguageLevel3Div}ネイティブレベル
                </Text>
                <Text style={stylePDFLanguage.textRBL35}>
                  {koreanLanguageLevel3Div}ネイティブレベル
                </Text>
              </View>
            </View>
          </View>
        </View>
      </>
    );
  }
}
export default PDFLanguage;
