import React from 'react';
import  stylePDFArea from '../styles/stylePDFArea.js';
import { Text, View } from '@react-pdf/renderer';

class PDFWorkExperience extends React.PureComponent {

  render() {
    return(
        <>
            <View style={stylePDFArea.body} wrap={false}>
                <Text style={stylePDFArea.textP}>
                    職務経験での自己ＰＲ、応募動機など
                </Text>
                <View style={stylePDFArea.row}>
                    <Text style={stylePDFArea.textTBRL100}>		
                        {this.props.dataWorkExperience.selfPr || ""}
                    </Text>
                </View>
            </View>
        </>
    );
  }
}
export default PDFWorkExperience;