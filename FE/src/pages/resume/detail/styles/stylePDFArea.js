import {StyleSheet } from '@react-pdf/renderer';

export default StyleSheet.create({
    body: {
        flexDirection: 'column',
        paddingTop: 10,
        fontFamily: 'Arial Unicode MS',
    },
    row: {
        flexDirection: 'row',
        width: '100%',
    },
    textP: {
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        textAlign: 'justify',
        padding: 5,
        fontWeight: 500
    },
    textTBRL100: {
        width: '100%',
        flexDirection: 'column',
        border: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 200
    },
})