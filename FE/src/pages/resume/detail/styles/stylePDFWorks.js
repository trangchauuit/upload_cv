import { StyleSheet } from '@react-pdf/renderer';

export default StyleSheet.create({
  body: {
    flexDirection: 'column',
    paddingTop: 10,
    fontFamily: 'Arial Unicode MS',
  },
  row: {
    flexDirection: 'row',
    width: '100%',
  },
  colDotted15: {
    flexDirection: 'column',
    width: '15%',
    borderLeft: 1,
    borderRight: 1,
    borderBottom: 1,
    borderBottomStyle: "dotted"
  },
  col15: {
    flexDirection: 'column',
    width: '15%',
    borderLeft: 1,
    borderRight: 1,
    borderBottom: 1
  },
  colBDotted32: {
    width: '32%',
    flexDirection: 'column',
    borderBottom: 1,
    borderBottomStyle: "dotted"
  },
  colB32: {
    width: '32%',
    flexDirection: 'column',
    borderBottom: 1
  },
  colBLDotted35: {
    width: '35%',
    flexDirection: 'column',
    borderLeft: 1,
    borderLeftStyle: "dotted",
    borderBottom: 1,
    borderBottomStyle: "dotted"
  },
  colBL35: {
    width: '35%',
    flexDirection: 'column',
    borderLeft: 1,
    borderLeftStyle: "dotted",
    borderBottom: 1,
    flexWrap: 'wrap',
  },
  colRBLDotted18: {
    width: '18%',
    flexDirection: 'column',
    borderRight: 1,
    borderLeft: 1,
    borderLeftStyle: "dotted",
    borderBottom: 1,
    borderBottomStyle: "dotted"
  },
  colRBL18: {
    width: '18%',
    flexDirection: 'column',
    borderRight: 1,
    borderLeft: 1,
    borderLeftStyle: "dotted",
    borderBottom: 1,
  },
  textP: {
    fontSize: 12,
    fontWeight: "bold",
    fontFamily: 'Arial Unicode MS',
    textAlign: 'justify',
    padding: 5,
  },
  textPCenter: {
    fontSize: 12,
    fontWeight: "bold",
    fontFamily: 'Arial Unicode MS',
    textAlign: 'center',
    padding: 5,
  },
  textPLeft: {
    fontSize: 12,
    fontWeight: "bold",
    fontFamily: 'Arial Unicode MS',
    textAlign: 'left',
    padding: 5,
    wordWrap: 'breakWord'
  },
  a: {
    textOverflow: 'ellipsis',
    fontSize: 12,
    fontWeight: "bold",
    fontFamily: 'Arial Unicode MS',
    textAlign: 'left',
    padding: 5,
    wordWrap: 'breakWord'
  },
  textTBRL18: {
    width: '18%',
    flexDirection: 'column',
    border: 1,
    padding: 5,
    fontSize: 12,
    fontFamily: 'Arial Unicode MS',
    textAlign: 'center',
    wordWrap: 'breakWord'
  },
  textTBRL15: {
    width: '15%',
    flexDirection: 'column',
    border: 1,
    padding: 5,
    fontSize: 12,
    fontFamily: 'Arial Unicode MS',
    textAlign: 'center',
  },
  textTB32: {
    width: '32%',
    flexDirection: 'column',
    borderTopWidth: 1,
    borderBottom: 1,
    padding: 5,
    fontSize: 12,
    fontFamily: 'Arial Unicode MS',
    textAlign: 'center',
  },
  textBDotted100: {
    width: '100%',
    flexDirection: 'column',
    borderBottom: 1,
    borderBottomStyle: "dotted",
    padding: 5,
    fontSize: 12,
    fontFamily: 'Arial Unicode MS',
    minHeight: 25
  },
  text100: {
    width: '100%',
    flexDirection: 'column',
    padding: 5,
    fontSize: 12,
    fontFamily: 'Arial Unicode MS',
    minHeight: 25
  },
  textTBL35: {
    width: '35%',
    flexDirection: 'column',
    borderTopWidth: 1,
    borderBottom: 1,
    borderLeftWidth: 1,
    padding: 5,
    fontSize: 12,
    fontFamily: 'Arial Unicode MS',
    textAlign: 'center',
  },
  wrapper2: {
    display: 'flex',
    flexWrap: 'wrap',
    width: '20%'
  },
  cardBody: {
    padding: 5,
    wordWrap: 'breakWord'
  },
  flexItem: {
    width: '190px'
  }
})
