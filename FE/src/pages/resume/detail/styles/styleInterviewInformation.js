import {StyleSheet } from '@react-pdf/renderer';

export default StyleSheet.create({
    body: {
        flexDirection: 'column',
        paddingTop: 10,
        fontFamily: 'Arial Unicode MS',
    },
    rowMargin: {
        flexDirection: 'row',
        width: '100%',
        marginTop: '10px'
    },
    row: {
        flexDirection: 'row',
        width: '100%',
    },
    textP: {
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        textAlign: 'justify',
        padding: 5,
        fontWeight: 500
    },
    col15: {
        width: '15%',
        flexDirection: 'column',
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        marginBottom: '15px'
        //minHeight: 20
    },
    col15Left: {
        width: '15%',
        flexDirection: 'column',
        borderTopWidth: 1,
        //borderLeftWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        marginBottom: '15px'
        //minHeight: 20
    },
    col45: {
        width: '45%',
        flexDirection: 'column',
        borderTopWidth: 1,
        borderRightWidth: 1,
        borderLeftWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        marginBottom: '15px'
        //minHeight: 20
    },
    col25: {
        width: '25%',
        flexDirection: 'column',
        borderTopWidth: 1,
        borderRightWidth: 1,
        borderLeftWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        marginBottom: '15px'
        //minHeight: 20
    },
    textScore: {
        width: '5%',
        flexDirection: 'column',
        borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textAnswer: {
        width: '35%',
        flexDirection: 'column',
        borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textAnswerInput: {
        width: '40%',
        flexDirection: 'column',
        borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textNo: {
        width: '5%',
        flexDirection: 'column',
        borderLeftWidth: 1,
        //borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textQuestion: {
        width: '35%',
        flexDirection: 'column',
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textSuggest: {
        width: '20%',
        flexDirection: 'column',
        //borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textScoreDotted: {
        width: '5%',
        flexDirection: 'column',
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderBottomStyle: "dotted",
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textAnswerDotted: {
        width: '35%',
        flexDirection: 'column',
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderBottomStyle: "dotted",
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textAnswerDottedInput: {
        width: '40%',
        flexDirection: 'column',
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderBottomStyle: "dotted",
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textNoDotted: {
        width: '5%',
        flexDirection: 'column',
        borderLeftWidth: 1,
        //borderRightWidth: 1,
        borderBottomWidth: 1,
        borderBottomStyle: "dotted",
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textQuestionDotted: {
        width: '35%',
        flexDirection: 'column',
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderBottomStyle: "dotted",
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textSuggestDotted: {
        width: '20%',
        flexDirection: 'column',
        //borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        borderBottomStyle: "dotted",
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40
    },
    textHeaderNo: {
        width: '5%',
        flexDirection: 'column',
        borderTopWidth: 1,
        borderLeftWidth: 1,
        //borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        textAlign: 'center'
    },
    textHeaderQuestion: {
        width: '35%',
        flexDirection: 'column',
        borderTopWidth: 1,
        borderLeftWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        textAlign: 'center'
    },
    textHeaderAnswer: {
        width: '40%',
        flexDirection: 'column',
        borderTopWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        textAlign: 'center'
    },
    textHeaderSuggest: {
        width: '20%',
        flexDirection: 'column',
        borderTopWidth: 1,
        borderRightWidth: 1,
        borderBottomWidth: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        textAlign: 'center'
    },
    textScoreTotal: {
        width: '5%',
        flexDirection: 'column',
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40,
        //marginBottom: '15px',
        //marginTop: '15px'
    },
    textAnswerTotal: {
        width: '35%',
        flexDirection: 'column',
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40,
        textAlign: 'left',
        //marginBottom: '15px',
        //marginTop: '15px'
    },
    textNoTotal: {
        width: '5%',
        flexDirection: 'column',
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40,
        //marginBottom: '15px',
        //marginTop: '15px'
    },
    textQuestionTotal: {
        width: '35%',
        flexDirection: 'column',
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40,
        //marginBottom: '15px',
        //marginTop: '15px'
    },
    textSuggestTotal: {
        width: '20%',
        flexDirection: 'column',
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 40,
        textAlign: 'right',
        //marginBottom: '15px',
        //marginTop: '15px'
    },
    textTBRL100: {
        width: '100%',
        flexDirection: 'column',
        border: 1,
        padding: 5,
        fontSize: 12,
        fontFamily: 'Arial Unicode MS',
        minHeight: 100
    },
})