import React from "react";
import { FormGroup, Label, Input, Col, Alert } from "reactstrap";
import moment from 'moment';
import DatePicker, { registerLocale} from 'react-datepicker';
import { withTranslation } from 'react-i18next';
import "react-datepicker/dist/react-datepicker.css";
import ja from 'date-fns/locale/ja';
import vi from 'date-fns/locale/vi';
import en from 'date-fns/locale/en-US';

registerLocale('ja', ja);
registerLocale('vi', vi);
registerLocale('en', en);

class TabBasicInfo extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dataBasicInfo: this.props.dataBasicInfo,
      buttonEdit: {
        isDisable: true
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.dataBasicInfo !== this.state.dataBasicInfo) {
      this.setState({ dataBasicInfo: nextProps.dataBasicInfo });
    }

    // if(nextProps.buttonEdit !== this.state.buttonEdit){
    //   this.setState({ buttonEdit: nextProps.buttonEdit });
    // }
  }

  handleChange = (e) => {
    this.setState({
      dataBasicInfo: {
        ...this.state.dataBasicInfo,
        [e.target.name]: {
          ...this.state.dataBasicInfo[e.target.name],
          value: e.target.value
        }
      }
    });
  }

  handleChangeNumber = (e) => {
    const re = /^[0-9\b]+$/;
    if (e.target.value === '' || re.test(e.target.value)) {
      this.setState({
        dataBasicInfo: {
          ...this.state.dataBasicInfo,
          [e.target.name]: {
            ...this.state.dataBasicInfo[e.target.name],
            value: e.target.value
          }
        }
      });
    }
  }

  isValidDate = (year, month, day) => {
    if (!year || !month || !day) return true;

    try {
      var d = new Date(year, month, day);
      if (d.getFullYear() == year && d.getMonth() == month && d.getDate() == day) {
        return true;
      }
      return false;
    } catch (e) {
      return false;
    }
  }

  handleChangeSelect = (e) => {
    this.setState({
      dataBasicInfo: {
        ...this.state.dataBasicInfo,
        [e.target.name]: {
          ...this.state.dataBasicInfo[e.target.name],
          value: e.target.value,
          isValid: true,
          message: ''
        },
      }
    }, () => this.props.onDataBasicInfo(this.state.dataBasicInfo));
  }

  handleChangeDate = (date, field) => {
    this.setState({
      dataBasicInfo: {
        ...this.state.dataBasicInfo,
        [field]: {
          ...this.state.dataBasicInfo[field],
          value: date ? moment(new Date(date)).format('YYYY/MM/DD') : null
        }
      }
    }, () => this.props.onDataBasicInfo(this.state.dataBasicInfo));
  }

  handleKeyDown = (e) => {
    if (e.key === 'Tab') {
      this.props.onDataBasicInfo(this.state.dataBasicInfo);
    }
  }

  handleBlur = () => {
    this.props.onDataBasicInfo(this.state.dataBasicInfo);
  }

  genNumbers = (from, to) => {
    let currentNumber = from;
    const source = []
    while (currentNumber <= to) {
      source.unshift(currentNumber);
      currentNumber++;
    }

    return source;
  }

  render() {
    const { t } = this.props;
    const { dataBasicInfo, buttonEdit} = this.state;
    return(
      <>
        <FormGroup row >
          <Label className="col-form-label font-weight-bold">
            {t('label.applicantBasicInformation')}
          </Label>
        </FormGroup>
        <FormGroup row>
          <Label for="nameKana" md={3}>{t('label.nameKana')}</Label>
          <Col md={9}>
            <Input type="text" name="nameKana" id="nameKana"
              value={dataBasicInfo.nameKana.value || ""}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
            <Alert color="danger" isOpen={!(dataBasicInfo.nameKana.isValid)}>
              {dataBasicInfo.nameKana.message}
            </Alert>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="name" md={3}>{t('label.name')}</Label>
          <Col md={9}>
            <Input type="text" name="name" id="name"
              value={dataBasicInfo.name.value || ""}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
            <Alert color="danger" isOpen={!(dataBasicInfo.name.isValid)}>
              {dataBasicInfo.name.message}
            </Alert>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="country" md={3}>{t('label.nationality')}</Label>
          <Col md={9}>
            <Input type="text" name="country" id="country"
              value={dataBasicInfo.country.value || ""}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
            <Alert color="danger" isOpen={!(dataBasicInfo.country.isValid)}>
              {dataBasicInfo.country.message}
            </Alert>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="phone" md={3}>{t('label.phoneNumber')}</Label>
          <Col md={9}>
            <Input type="text" name="phone" id="phone"
              value={dataBasicInfo.phone.value || ""}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="year_birthday" md={3}>{t('label.birthday')}</Label>
          <Col md={9}>
            <div className="d-flex" style={{gap: '10px'}}>
              <div className="full-w">
                <Input type="select" name="dobYear" id="dobYear"
                  value={dataBasicInfo.dobYear.value}
                  onChange={this.handleChangeSelect}
                  onKeyDown={this.handleKeyDown}
                  onBlur={this.handleBlur}
                  disabled={buttonEdit.isDisable}
                >
                  <option value="">{t('label.choice')}</option>
                  {this.genNumbers(1900, new Date().getFullYear()).map(y => <option key={y} value={y}>{y}</option>)}
                </Input>

                <Alert color="danger" isOpen={!(dataBasicInfo.dobYear.isValid)}>
                  {dataBasicInfo.dobYear.message}
                </Alert>
              </div>
              <div className="full-w">
                <Input type="select" name="dobMonth" id="dobMonth"
                  value={dataBasicInfo.dobMonth.value}
                  onChange={this.handleChangeSelect}
                  onKeyDown={this.handleKeyDown}
                  onBlur={this.handleBlur}
                  disabled={buttonEdit.isDisable}
                >
                  <option value="">{t('label.choice')}</option>
                  {this.genNumbers(1, 12).map(y => <option key={y} value={y}>{y}</option>)}
                </Input>

                <Alert color="danger" isOpen={!(dataBasicInfo.dobMonth.isValid)}>
                  {dataBasicInfo.dobMonth.message}
                </Alert>
              </div>
              <div className="full-w">
                <Input type="select" name="dobDay" id="dobDay"
                  value={dataBasicInfo.dobDay.value}
                  onChange={this.handleChangeSelect}
                  onKeyDown={this.handleKeyDown}
                  onBlur={this.handleBlur}
                  disabled={buttonEdit.isDisable}
                >
                  <option value="">{t('label.choice')}</option>
                  {this.genNumbers(1, 31).map(y => <option key={y} value={y}>{y}</option>)}
                </Input>

                <Alert color="danger" isOpen={!(dataBasicInfo.dobDay.isValid)}>
                  {dataBasicInfo.dobDay.message}
                </Alert>
              </div>
            </div>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="age" md={3}>{t('label.age')}</Label>
          <Col md={9}>
            <Input type="text" name="age" id="age"
              value={dataBasicInfo.age.value || ""}
              onChange={this.handleChangeNumber}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
            <Alert color="danger" isOpen={!(dataBasicInfo.age.isValid)}>
              {dataBasicInfo.age.message}
            </Alert>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="gender" md={3}>{t('label.gender')}</Label>
          <Col md={9}>
            <Input type="select" name="gender" id="gender"
              value={dataBasicInfo.gender.value}
              onChange={this.handleChangeSelect}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
            >
              <option value={-1}>{t('label.choice')}</option>
              <option value={0}>{t('label.male')}</option>
              <option value={1}>{t('label.female')}</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="marriedStatus" md={3}>{t('label.married')}</Label>
          <Col md={9}>
            <Input type="select" name="marriedStatus" id="marriedStatus"
              value={dataBasicInfo.marriedStatus.value}
              onChange={this.handleChangeSelect}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
            >
              <option value={"-1"}>{t('label.choice')}</option>
              <option value="1">{t('label.yes')}</option>
              <option value="0">{t('label.no')}</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label md={3}>{t('label.currentAddress')}</Label>
          <Label md={1}>〒</Label>
          <Col md={2}>
            <Input type="text" name="zipCode" id="zipCode"
              value={dataBasicInfo.zipCode.value || ""}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
            <Alert color="danger" isOpen={!(dataBasicInfo.zipCode.isValid)}>
              {dataBasicInfo.zipCode.message}
            </Alert>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label md={3}></Label>
          <Col md={9}>
            <Input type="textarea" name="currentAddress" id="currentAddress"
              value={dataBasicInfo.currentAddress.value || ""}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
            <Alert color="danger" isOpen={!(dataBasicInfo.currentAddress.isValid)}>
              {dataBasicInfo.currentAddress.message}
            </Alert>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label md={3}>{t('label.contactAddress')}</Label>
          <Label md={1}>〒</Label>
          <Col md={2}>
            <Input type="text" name="zipCodeContact" id="zipCodeContact"
              value={dataBasicInfo.zipCodeContact.value || ""}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
            <Alert color="danger" isOpen={!(dataBasicInfo.zipCodeContact.isValid)}>
              {dataBasicInfo.zipCodeContact.message}
            </Alert>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label md={3}></Label>
          <Col md={9}>
            <Input type="textarea" name="contactAddress" id="contactAddress"
              value={dataBasicInfo.contactAddress.value || ""}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
            <Alert color="danger" isOpen={!(dataBasicInfo.contactAddress.isValid)}>
              {dataBasicInfo.contactAddress.message}
            </Alert>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label md={3}>Email</Label>
          <Col md={9}>
            <Input type="email" name="email" id="email"
              value={dataBasicInfo.email.value || ""}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
            <Alert color="danger" isOpen={!(dataBasicInfo.email.isValid)}>
              {dataBasicInfo.email.message}
            </Alert>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label md={3}>{t('label.japanWorkVisa')}</Label>
          <Col md={9}>
            <Input type="select" name="japanVisa" id="japanVisa"
              value={dataBasicInfo.japanVisa.value}
              onChange={this.handleChangeSelect}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
            >
              <option value="1">{t('label.yes')}</option>
              <option value="0">{t('label.no')}</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label md={3}>{t('label.visaExpirationDate')}</Label>
          <Col md={9}>
            <DatePicker name="japanVisaDateExpired"  id="japanVisaDateExpired"
              selected={dataBasicInfo.japanVisaDateExpired.value ? new Date(dataBasicInfo.japanVisaDateExpired.value) : null}
              onChange={(e) => this.handleChangeDate(e, 'japanVisaDateExpired')}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              dateFormat="yyyy/MM/dd"
              className="form-control"
              locale={t('opt.language')}
              disabled={buttonEdit.isDisable}
              />
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label md={3}>{t('label.visaType')}</Label>
          <Col md={9}>
            <Input type="text" name="japanVisaType" id="japanVisaType"
              value={dataBasicInfo.japanVisaType.value || ""}
              onChange={this.handleChange}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
              />
          </Col>
        </FormGroup>
        </>
    );
  }
}
export default withTranslation('common')(TabBasicInfo);
