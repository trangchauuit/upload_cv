import { API } from '../../../../utils/common';
import React from "react";
import { FormGroup, Label, Input, Col, Row, Button, Alert } from "reactstrap";
import { withTranslation } from 'react-i18next';
import swal from 'sweetalert';
import moment from 'moment';

class TabInterviewInformation extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dataInterviewInfo: this.props.dataInterviewInfo,
      dataQuestions: this.props.dataQuestions,
      dataInforCalendly: this.props.dataInforCalendly,
      listQuestionsShow: this.props.listQuestionsShow,
      buttonEditShow: this.props.buttonEdit,
      buttonEdit: {
        isDisable: true,
        text: 'button.edit',
      }
    };
  }
  componentWillReceiveProps(nextProps) {
    if (nextProps.dataInterviewInfo !== this.state.dataInterviewInfo) {
      this.setState({ dataInterviewInfo: nextProps.dataInterviewInfo });
    }
    if (nextProps.buttonEdit !== this.state.buttonEditShow) {
      this.setState({ buttonEditShow: nextProps.buttonEdit, buttonEdit: nextProps.buttonEdit });
    }
    if (nextProps.dataInforCalendly !== this.state.dataInforCalendly) {
      this.setState({ dataInforCalendly: nextProps.dataInforCalendly });
    }
  }
  loadAnswerByQuestions = (id) => {
    let dataQuestions = [...this.state.dataQuestions];
    let answers = dataQuestions.filter(obj => (obj.id + "") === (id + ""));
    return answers;
  }
  handleChangeQuestions = (e, indexSelected) => {
    let contentSelected = document.getElementById("question_" + indexSelected);
    // Thay đổi question_kind, load lại list câu trả lời để chọn khi thay doi cau hoi(nếu question_kind = 2)
    let temp = { ...this.state.dataInterviewInfo };
    let answers = this.loadAnswerByQuestions(e.target.value);
    //let elment = temp.dataQA.filter((el, index) => (index === indexSelected));
    let elment = temp.dataQA[indexSelected];
    elment.question.value = e.target.value;
    elment.question_kind = answers.length > 0? answers[0].question_kind: '1';
    elment.answer_id = { value: -1, isValid: true, message: '' };
    elment.answer_content = '';
    elment.comment = { value: '', isValid: true, message: '' };
    elment.question_content = contentSelected.options[contentSelected.selectedIndex].text;
    elment.suggest = answers.length > 0? answers[0].suggest==null? '': answers[0].suggest:'';
    if ((elment.question_kind + "") === '2'){
      elment.answers = answers.length > 0? answers[0].answers: [];
    }
    this.setState({ dataInterviewInfo: temp });
  }

  cancelInterviewInfo = () => {
    this.setState(prevState => ({
      buttonEdit: {
        text: ('button.edit'),
        isDisable: !(prevState.buttonEdit.isDisable)
      },
      buttonEditShow: {
        text: ('button.edit'),
        isDisable: !(prevState.buttonEdit.isDisable),
        type:'cancel'
      }
    }), () => { this.props.onDataChangeButton(this.state.buttonEditShow) });
  }

  editInterviewInfo = async (id) => {
    this.setState(prevState => ({
      buttonEdit: {
        text: (prevState.buttonEdit.isDisable === true ? 'button.save' : 'button.edit'),
        isDisable: !(prevState.buttonEdit.isDisable)
      },
      buttonEditShow: {
        text: (prevState.buttonEdit.isDisable === true ? 'button.save' : 'button.edit'),
        isDisable: !(prevState.buttonEdit.isDisable),
        type: 'save',
        interviewId: id,
      }
    }), () => { this.props.onDataChangeButton(this.state.buttonEditShow) });
  }

  handleZoomStart = (e, url) => {
    e.preventDefault();
    //url = 'https://us05web.zoom.us/j/83917940103?pwd=UEF1M0dRM2tXbkk1WW8raUZYa1U3UT09';
    if (url) {
      window.open(url, '_blank').focus();
    } else {
      swal({
        title: this.props.t('message.errorConnectingToZoom'),
        icon: "error",
      });
    }
  }
  handleDeleteQA = (e, index) => {
    e.preventDefault();
    var elements = { ...this.state.dataInterviewInfo };
    let selectedElements = elements.dataQA;
    selectedElements.splice(index, 1);
    this.setState({ dataInterviewInfo: elements }, () => this.props.onDataInterviewInfo(this.state.dataInterviewInfo));
  }
  addRow = (e) => {
    let index = document.getElementsByName('question').length - 1;
    if (index < 12) {
      var elements = [...this.state.dataInterviewInfo.dataQA];
      elements.push({
        id: -1,
        question: { value: -1, isValid: true, message: '' },
        answer: { value: '', isValid: true, message: '' },
        question_kind: '1',
        answer_id: { value: -1, isValid: true, message: '' },
        answer_content: '',
        question_content: '',
        comment: { value: '', isValid: true, message: '' },
        suggest: '',
        score: 0
      });
      this.setState({
        dataInterviewInfo: {
          ...this.state.dataInterviewInfo,
          dataQA: elements
        }
      });
    } else {
      swal({
        title: `${this.props.t('message.validMaxRow', { 0: 12 })}`,
        icon: "error",
      });
    }
  }
  handleChangeInput = (e, data) => {
    this.setState({
      [data]: { ...this.state[data], [e.target.name]: { ...this.state[data][e.target.name], value: e.target.value } }
    });
  }
  handleChangeSelect = (e, data) => {
    this.setState({
      [data]: { ...this.state[data], [e.target.name]: { ...this.state[data][e.target.name], value: e.target.value } }
    }, () => this.props.onDataInterviewInfo(this.state[data]));
  }
  handleKeyDown = (e) => {
    if (e.key === 'Tab') {
      this.props.onDataInterviewInfo(this.state.dataInterviewInfo);
    }
  }
  handleBlur = () => {
    this.props.onDataInterviewInfo(this.state.dataInterviewInfo);
  }
  handleChangeInputArr = (e, indexSelected) => {
    //let contentInput = document.getElementById("comment_" + indexSelected);
    let temp = { ...this.state.dataInterviewInfo };
    let elment = temp.dataQA[indexSelected];
    elment.answer.value = e.target.value;
    if (elment.question_kind + "" === "1"){
      elment.answer_id.value = null;
    }
    elment.answer_content = e.target.value
    elment.comment.value = e.target.value;
    this.setState({ dataInterviewInfo: temp });
  }
  handleChangeSelectArr = (e, indexSelected) => {
    let contentSelected = document.getElementById("answer_" + indexSelected);
    let contentInput = document.getElementById("comment_" + indexSelected);
    let temp = { ...this.state.dataInterviewInfo };
    let elment = temp.dataQA[indexSelected];
    elment.answer_id.value = e.target.value;
    elment.answer.value = null;
    elment.answer_content= contentInput.value;
    elment.score = contentSelected.options[contentSelected.selectedIndex].getAttribute('data-score');
    this.setState({ dataInterviewInfo: temp }, () => this.props.onDataInterviewInfo(this.state.dataInterviewInfo));
  }

  handleChangeDataInterviewInfo = (e) => {
    const { dataInterviewInfo } = this.state;
    dataInterviewInfo[e.target.name] = e.target.value;
    this.setState({
      dataInterviewInfo: { ...dataInterviewInfo }
    })

    this.props.onDataInterviewInfo(dataInterviewInfo);
  }

  confirmSendMail = (key = 'confirmSendMail') => {
    return window.confirm(this.props.t(`message.${key}`));
  }

  handleInviteInterview = async () => {
    const { dataInterviewInfo } = this.state;
    const params = {
      type_interview: +dataInterviewInfo.method,
      round: dataInterviewInfo.round,
    }
    if (!this.confirmSendMail()) return;

    await API.post(`/candidates/${this.props.candidateId}/sendMailInterview`, params)
    .then(() => {
      swal({
        title: this.props.t('message.update_success'),
        icon: "success",
      });
    })
    .catch(() => {
      swal({
        title:  this.props.t('message.update_fail'),
        icon: "error",
      });
    })
  }

  handleContinueInterviewNextRound = async () => {
    if (!this.confirmSendMail('confirmNextRound')) return;

    await API.post(`/result-interviews/continue-interview/${this.props.candidateId}`)
    .then(async () => {
      swal({
        title: this.props.t('message.update_success'),
        icon: "success",
      });
      await this.props.getData();
    })
    .catch((e) => {
      console.log(e)
      swal({
        title:  this.props.t('message.update_fail'),
        icon: "error",
      });
    })
  }

  handleSendInterviewResult = async () => {
    const { t } = this.props;
    const url = `/candidates/${this.props.candidateId}/sendMailResult`
    if (!this.confirmSendMail()) return;

    const params = { result: +this.state.dataInterviewInfo.result }
    await API.post(url, params)
    .then(async () => {
      swal({
        title: t('message.update_success'),
        icon: "success",
      });
    })
    .catch((e) => {
      console.log(e)
      swal({
        title: t('message.update_fail'),
        icon: "error",
      });
    })
  }

  disableSendNextRound = () => {
    let isDisable = true;
    const { orgInterviews } = this.props;
    const roundOneInterview = orgInterviews.filter(x => x.round === 1)[0];
    if (roundOneInterview && +roundOneInterview.result === 1 && roundOneInterview.is_continue !== 1) isDisable = false;

    return isDisable;
  }

  disableRound1() {
    return (this.state.dataInterviewInfo.round === 1 && this.state.dataInterviewInfo.is_continue === 1)
  }

  disableStartInterviewButton = () => {
    return this.disableRound1() || [0,2,3].includes(this.intInterviewMethod());
  }

  disableSendMailInterview = () => {
    const { end_time } = this.state.dataInforCalendly;
    let isEnd = false;
    if (end_time) {
      isEnd = moment(end_time) < moment();
    }
    return this.disableRound1() || [0, 3].includes(this.intInterviewMethod()) || isEnd;
  }

  intInterviewMethod = () => {
    return +(this.state.dataInterviewInfo.method || 0);
  }

  render() {
    const { t } = this.props;
    const { dataInterviewInfo, buttonEdit, dataInforCalendly } = this.state;

    return (
      <>
        <Row>
          <Col md={12}>
            <FormGroup row>
              <Label md={4} className="col-form-label font-weight-bold">
                {t('label.interviewSchedule')}
              </Label>

              <Col md={4}></Col>

              <Col md={4}>
                <div style={{ float: 'right' }}>
                  <Button outline color="primary" className="mr-1" onClick={() => this.editInterviewInfo(this.props.interviewId)}>{t(buttonEdit.text)}</Button>
                  <Button outline color="warning" className="ml-2 mr-1" hidden={buttonEdit.isDisable} onClick={this.cancelInterviewInfo}>{t('button.cancel')}</Button>
                </div>
              </Col>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <FormGroup row>
              <Label for="interview_type" md={4}>{t('label.interviewType')}</Label>
              <Col md={8}>
                <Input type="select" name="method" md={9}
                  disabled={buttonEdit.isDisable || this.disableRound1()}
                  onChange={this.handleChangeDataInterviewInfo}
                  value={this.state.dataInterviewInfo.method || 0}
                >
                  <option value={0}>{t('label.choice')}</option>
                  <option value={1}>{t('label.onlineInterview')}</option>
                  <option value={2}>{t('label.offlineInterview')}</option>
                  <option value={3}>{t('label.noInterview')}</option>
                </Input>
              </Col>
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup row>
              <Label for="date_interview" md={4}>{t('label.sendInterviewResult')}</Label>
              <Col md={8}>
                <Button
                  color="primary"
                  disabled={this.disableSendMailInterview()}
                  onClick={this.handleInviteInterview}>{t('button.sendInterviewResult')}</Button>
              </Col>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <FormGroup row>
              <Label for="applicant" md={4}>{t('label.applicant')}</Label>
              <Col md={8}>
                <Input type="text" name="applicant" id="applicant"
                  disabled={true} value={dataInforCalendly.name || ''}
                  />
              </Col>
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup row>
              <Label for="date_interview" md={4}>{t('label.data')}</Label>
              <Col md={8}>
                <Input type="text" name="date_interview" id="date_interview"
                  disabled={true}
                  value={dataInforCalendly.start_time? moment.utc(dataInforCalendly.start_time).local().format('YYYY/MM/DD'):''}
                  />
              </Col>
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup row>
              <Label for="date_interview" md={4}>{t('label.timezone')}</Label>
              <Col md={8}>
                <Input type="text" name="date_interview" id="date_interview"
                  disabled={true}
                  value={dataInforCalendly.start_time? new Date().toTimeString().slice(9):''}
                  />
              </Col>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <FormGroup row>
              <Label for="interview_email" md={4}>Email</Label>
              <Col md={8}>
                <Input type="text" name="interview_email" id="interview_email"
                  disabled={true} value={dataInforCalendly.email || ''}
                  />
              </Col>
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup row>
              <Label for="start" md={4}>{t('label.start')}</Label>
              <Col md={8}>
                <Input type="text" name="start" id="start"
                  disabled={true}
                  value={dataInforCalendly.start_time? moment.utc(dataInforCalendly.start_time).local().format('HH:mm'):''}
                  />
              </Col>
            </FormGroup>
          </Col>
          <Col md={4}>
            <Button color="primary" disabled={this.disableStartInterviewButton()} onClick={(e) => this.handleZoomStart(e, dataInforCalendly.join_url)}>{t('button.zoomStart')}</Button>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <FormGroup row>
              <Label for="password_zoom" md={4}>{t('label.passwordZoom')}</Label>
              <Col md={8}>
                <Input type="text" name="password_zoom" id="password_zoom"
                  disabled={true} value={dataInforCalendly.password || ''}
                  />
              </Col>
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup row>
              <Label for="end" md={4}>{t('label.end')}</Label>
              <Col md={8}>
                <Input type="text" name="end" id="end"
                  disabled={true} value={dataInforCalendly.end_time? moment.utc(dataInforCalendly.end_time).local().format('HH:mm'):''}
                  />
              </Col>
            </FormGroup>
          </Col>
        </Row>
        <div style={{ float: "left" }}>
          <Label className="col-form-label font-weight-bold">
            {t('label.interviewResult')}
          </Label>
        </div>
        <div style={{ float: "right" }}>
        </div>
        <div style={{ clear: "both" }}></div>
        <Row className="mt-3">
          <Col md={4}>
            <FormGroup row>
              <Label for="result" md={4}>{t('label.lblInterviewResult')}</Label>
              <Col md={8}>
                <Input type="select" name="result" md={9}
                  disabled={buttonEdit.isDisable || this.disableRound1()}
                  onChange={this.handleChangeDataInterviewInfo}
                  value={this.state.dataInterviewInfo.result || 0}
                >
                  <option value={0}>{t('label.choice')}</option>
                  { dataInterviewInfo.round !== 2 && <option value={1}>{t('label.interviewPass')}</option> }
                  <option value={2}>{t('label.interviewFail')}</option>
                  <option value={3}>{t('label.interviewPassFinish')}</option>
                </Input>
              </Col>
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup row>
              <Label for="result" md={4}></Label>
              <Col md={8}>
               { dataInterviewInfo.round !== 2 && <Button
                  disabled={this.disableSendNextRound()}
                  color="primary"
                  onClick={this.handleContinueInterviewNextRound}>
                    {t('button.secondInterview')}
                </Button> }
              </Col>
            </FormGroup>
          </Col>
          <Col md={4}>
            <FormGroup row>
              <Label for="result" md={4}>{t('label.lblSendInterviewResult')}</Label>
              <Col md={8}>
                <Button
                  disabled={[0,1].includes(+this.state.dataInterviewInfo.result)}
                  color="primary"
                  onClick={this.handleSendInterviewResult}>
                    {t('button.sendInterviewResult2')}
                </Button>
              </Col>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          <Col md={4}>
            <FormGroup row>
              <Label for="interviewer" md={4}>{t('label.interviewer')}</Label>
              <Col md={8}>
                <Input type="text" name="interviewer" id="interviewer"
                  disabled={buttonEdit.isDisable}
                  value={dataInterviewInfo.interviewer.value == null ? '' : dataInterviewInfo.interviewer.value}
                  onChange={(e) => this.handleChangeInput(e, 'dataInterviewInfo')}
                  onKeyDown={this.handleKeyDown}
                  onBlur={this.handleBlur}
                  />
              </Col>
            </FormGroup>
          </Col>
        </Row>
        <Row>
          {dataInterviewInfo.dataQA.map((o, i) => {
            let question_kind = null, div_comment = null;
            if ((o.question_kind + "") === "2"){
              let answers = this.loadAnswerByQuestions(o.question.value);
              o.answers = answers.length > 0? answers[0].answers: [];
              question_kind = <Input type="select" name="answer" id={`answer_${i}`}
                disabled={buttonEdit.isDisable}
                onChange={(e) => this.handleChangeSelectArr(e, i)}
                value={o.answer_id.value}
              >
                <option value={-1}>{t('label.choice')}</option>
                {o.answers && o.answers.map((item, index) => {
                  return (
                    <option key={index} value={item.id} data-score={item.score}>{`${item.score} - ${item.content}`}</option>
                  )
                })}
              </Input>
              div_comment = <Input type="textarea" name="comment" id={`comment_${i}`} rows='3'
                disabled={buttonEdit.isDisable}
                value={o.comment.value}
                onChange={(e) => this.handleChangeInputArr(e, i)}
                onKeyDown={this.handleKeyDown}
                onBlur={this.handleBlur}
                />;
            }else{
              div_comment = <Input type="textarea" name="comment" id={`comment_${i}`} rows='3'
                disabled={buttonEdit.isDisable}
                value={o.answer.value}
                onChange={(e) => this.handleChangeInputArr(e, i)}
                onKeyDown={this.handleKeyDown}
                onBlur={this.handleBlur}
                />;
            }
            return (
              <React.Fragment key={i}>
                <Col md={4}>
                  <FormGroup row>
                    <Label for="question" md={4}>{t('label.question')} {i + 1}</Label>
                    <Col md={8}>
                      <Input type="select" name="question" id={`question_${i}`} md={9}
                        disabled={buttonEdit.isDisable}
                        onChange={(e) => this.handleChangeQuestions(e, i)}
                        value={o.question.value}
                      >
                        <option value={-1}>{t('label.choice')}</option>
                        {this.props.dataQuestions.map((item, index) => {
                          return (
                            <option key={index} value={item.id}>{item.content}</option>
                          )
                        })}
                      </Input>
                      <Alert style={{ 'width': '100%' }} color="danger" isOpen={!(o.question.isValid)}>
                        {o.question.message}
                      </Alert>
                    </Col>
                  </FormGroup>
                  <FormGroup row>
                    <Label for="suggest" md={4}>{t('label.suggest')}</Label>
                    <Col md={8}>
                      <Input type="text" name="suggest" id={`suggest_${i}`}
                        disabled={true}
                        value={o.suggest || ''}
                        />
                    </Col>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <FormGroup row>
                    <Label for="comment" md={4}>{t('label.comment')}</Label>
                    <Col md={8}>
                      {div_comment}
                      <Alert style={{ 'width': '100%' }} color="danger" isOpen={!(o.comment.isValid)}>
                        {o.comment.message}
                      </Alert>
                    </Col>
                  </FormGroup>
                </Col>
                <Col md={4}>
                  <FormGroup row style={{"display":(o.question_kind + "") === "2"? "":"none"}}>
                    <Label for="answer" md={4}>{t('label.score')}</Label>
                    <Col md={8}>
                      {question_kind}
                      <Alert style={{ 'width': '100%' }} color="danger" isOpen={!(o.answer.isValid)}>
                        {o.answer.message}
                      </Alert>
                    </Col>
                  </FormGroup>
                  <FormGroup row style={{float:"right", marginRight:"2px"}}>
                    <Button outline color="danger" className="float-right"
                      onClick={(e) => this.handleDeleteQA(e, i)}
                      disabled={buttonEdit.isDisable}
                    >{t('button.delete')}</Button>
                  </FormGroup>
                </Col>
              </React.Fragment >
            )
          })}
        </Row>
        <FormGroup row className="ml-1">
          <Button color="primary" md={12}
            onClick={(e) => this.addRow(e)}
            disabled={buttonEdit.isDisable}
          >{t('button.addQuestion')}</Button>
        </FormGroup>
        <FormGroup row className="ml-1 mr-1">
          <Label for="evaluation">{t('label.commentsFromTheInterviewer')}</Label>
          <Input type="textarea" name="evaluation" id="evaluation" rows='3'
            value={dataInterviewInfo.evaluation.value == null ? '' : dataInterviewInfo.evaluation.value}
            disabled={buttonEdit.isDisable}
            onChange={(e) => this.handleChangeInput(e, 'dataInterviewInfo')}
            onKeyDown={this.handleKeyDown}
            onBlur={this.handleBlur}
            />
          <Alert style={{ 'width': '100%' }} color="danger" isOpen={!(dataInterviewInfo.evaluation.isValid)}>
            {dataInterviewInfo.evaluation.message}
          </Alert>
        </FormGroup>
        </>
    );
  }
}
export default withTranslation('common')(TabInterviewInformation);
