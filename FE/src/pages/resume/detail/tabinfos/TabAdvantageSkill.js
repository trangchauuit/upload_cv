import React from "react";
import { FormGroup, Label, Input, Col } from "reactstrap";
import { withTranslation } from 'react-i18next';

class TabAdvantageSkill extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            dataAdvantageSkill: this.props.dataAdvantageSkill,
            buttonEdit: { isDisable: true }
        };
    }

    // componentWillReceiveProps(nextProps) {
    //     if(nextProps.buttonEdit !== this.state.buttonEdit){
    //         this.setState({ buttonEdit: nextProps.buttonEdit });
    //     }
    // }

    handleChange = (e) => {
        this.setState({
            dataAdvantageSkill: {
                ...this.state.dataAdvantageSkill,
                [e.target.name]: e.target.value
            }
        });
    }
    handleKeyDown = (e) => {
        if (e.key === 'Tab') {
            this.props.onDataAdvantageSkill(this.state.dataAdvantageSkill);
        }
    }
    handleBlur = () => {
        this.props.onDataAdvantageSkill(this.state.dataAdvantageSkill);
    }
    render() {
        const { t } = this.props;
        const { dataAdvantageSkill, buttonEdit} = this.state;
        return(
            <>
                <FormGroup row>
                    <Label className="col-form-label font-weight-bold">
                    {t('label.skills')}
                    </Label>
                </FormGroup>
                <FormGroup row>
                    <Col md={12}>
                        <Input type="textarea" name="applicantAdvantageSkillExplanation" id="applicantAdvantageSkillExplanation" rows='10'
                            value={dataAdvantageSkill.applicantAdvantageSkillExplanation || ""}
                            onChange={this.handleChange}
                            onKeyDown={this.handleKeyDown}
                            onBlur={this.handleBlur}
                            disabled={buttonEdit.isDisable}
                        />
                    </Col>
                </FormGroup>
            </>
        );
    }
}
export default withTranslation('common')(TabAdvantageSkill);
