import React from "react";
import { FormGroup, Label, Input, Col } from "reactstrap";
import { withTranslation } from 'react-i18next';

class TabComment extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            dataComment: this.props.dataComment,
            buttonEdit: { isDisable: true }
        };
    }

    // componentWillReceiveProps(nextProps) {
    //     if(nextProps.buttonEdit !== this.state.buttonEdit){
    //         this.setState({ buttonEdit: nextProps.buttonEdit });
    //     }
    // }

    handleChange = (e) => {
        this.setState({
            dataComment: {
                ...this.state.dataComment,
                [e.target.name]: e.target.value
            }
        });
    }
    handleKeyDown = (e) => {
        if (e.key === 'Tab') {
            this.props.onDataComment(this.state.dataComment);
        }
    }
    handleBlur = () => {
        this.props.onDataComment(this.state.dataComment);
    }
    render() {
        const { t } = this.props;
        const { dataComment, buttonEdit} = this.state;
        return(
            <>
                <FormGroup row>
                    <Label className="col-form-label font-weight-bold">
                        {t('label.hope')}
                    </Label>
                </FormGroup>
                <FormGroup row>
                    <Col md={12}>
                        <Input type="textarea" name="applicantComment" id="applicantComment" rows='10'
                            value={dataComment.applicantComment || ""}
                            onChange={this.handleChange}
                            onKeyDown={this.handleKeyDown}
                            onBlur={this.handleBlur}
                            disabled={buttonEdit.isDisable}
                        />
                    </Col>
                </FormGroup>
            </>
        );
    }
}
export default withTranslation('common')(TabComment);
