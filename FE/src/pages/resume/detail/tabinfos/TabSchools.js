import React from "react";
import { FormGroup, Label, Input, Col, Alert } from "reactstrap";
import moment from 'moment';
import DatePicker, { registerLocale} from 'react-datepicker';
import { withTranslation } from 'react-i18next';
import "react-datepicker/dist/react-datepicker.css";
import ja from 'date-fns/locale/ja';
import vi from 'date-fns/locale/vi';
import en from 'date-fns/locale/en-US';
registerLocale('ja', ja);
registerLocale('vi', vi);
registerLocale('en', en);

class TabSchools extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            dataSchools: this.props.dataSchools,
            buttonEdit: {
        isDisable: true
      }
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dataSchools !== this.state.dataSchools) {
          this.setState({ dataSchools: nextProps.dataSchools });
        }

        // if(nextProps.buttonEdit !== this.state.buttonEdit){
        //     this.setState({ buttonEdit: nextProps.buttonEdit });
        // }
    }

    handleChange = (e, id) => {
        this.setState({
            dataSchools: this.state.dataSchools.map(el => (el.id.value === id ? {
                ...el,
                [e.target.name]:{
                    ...el[e.target.name],
                    value: e.target.value
                }
                } : el))
        });
    }

    handleChangeDate = (date, field, id) => {
        let valueSelected = date ? moment(new Date(date)).format('YYYY/MM') : null;
        let answerArray = valueSelected ? valueSelected.split('/') : [];
        this.setState({
            dataSchools: this.state.dataSchools.map(el => (el.id.value === id ? {
                ...el,
                [field + 'Year']: {
                    ...el[field + 'Year'],
                    value: answerArray[0]
                },
                [field + 'Month']: {
                    ...el[field + 'Month'],
                    value: answerArray[1]
                }
            } : el))
        }, () => {this.props.onDataSchools(this.state.dataSchools)});
    }

    handleKeyDown = (e) => {
        if (e.key === 'Tab') {
            this.props.onDataSchools(this.state.dataSchools);
        }
    }

    handleBlur = () => {
        this.props.onDataSchools(this.state.dataSchools);
    }

    render() {
        const { t } = this.props;
        const { dataSchools, buttonEdit } = this.state;
        const Line = <hr className="hr-primary" />;
        return(
            <>
                <FormGroup row>
                    <Label className="col-form-label font-weight-bold">
                    {t('label.education')}
                    </Label>
                </FormGroup>
                {dataSchools.map((education, index) => {
                    let admissionPeriod = null, graduationPeriod = null;
                    if (education.fromYear.value && education.fromMonth.value){
                        let timestampAdmissionPeriod = Date.parse(education.fromYear.value + "/" + education.fromMonth.value + "/01");
                        if (isNaN(timestampAdmissionPeriod) === false) {
                            admissionPeriod = new Date(timestampAdmissionPeriod);
                        }
                    }
                    if (education.toYear.value && education.toMonth.value){
                        let timestampGraduationPeriod = Date.parse(education.toYear.value + "/" + education.toMonth.value + "/01");
                        if (isNaN(timestampGraduationPeriod) === false) {
                            graduationPeriod = new Date(timestampGraduationPeriod);
                        }
                    }
                    return (
                    <React.Fragment key={index}>
                        <FormGroup row>
                    <Label for="admissionPeriod" md={3}>{t('label.admissionPeriod')}</Label>
                            <Col md={9}>
                                <DatePicker name="admissionPeriod"  id="admissionPeriod"
                                    selected={admissionPeriod !=null ? new Date(admissionPeriod) : null}
                                    onChange={(e) => this.handleChangeDate(e, 'from', education.id.value)}
                                    onKeyDown={this.handleKeyDown}
                                    onBlur={this.handleBlur}
                                    dateFormat="yyyy/MM"
                                    className="form-control"
                                    locale={t('opt.language')}
                                    showMonthYearPicker
                                    showFullMonthYearPicker
                                    disabled={buttonEdit.isDisable}
                                />
                                <Alert color="danger" isOpen={!(education.fromMonth.isValid)}>
                                    {education.fromMonth.message}
                                </Alert>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="graduationPeriod" md={3}>{t('label.graduationPeriod')}</Label>
                            <Col md={9}>
                                <DatePicker name="graduationPeriod"  id="graduationPeriod"
                                    selected={graduationPeriod !=null ? new Date(graduationPeriod) : null}
                                    onChange={(e) => this.handleChangeDate(e, 'to', education.id.value)}
                                    onKeyDown={this.handleKeyDown}
                                    onBlur={this.handleBlur}
                                    dateFormat="yyyy/MM"
                                    className="form-control"
                                    locale={t('opt.language')}
                                    showMonthYearPicker
                                    showFullMonthYearPicker
                                    disabled={buttonEdit.isDisable}
                                />
                                <Alert color="danger" isOpen={!(education.toMonth.isValid)}>
                                    {education.toMonth.message}
                                </Alert>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="schoolName" md={3}>{t('label.schoolName')}</Label>
                            <Col md={9}>
                                <Input type="text" name="schoolName" id="schoolName"
                                    value={education.schoolName.value}
                                    onChange={(e) => this.handleChange(e, education.id.value)}
                                    onKeyDown={this.handleKeyDown}
                                    onBlur={this.handleBlur}
                                    disabled={buttonEdit.isDisable}
                                />
                                <Alert color="danger" isOpen={!(education.schoolName.isValid)}>
                                    {education.schoolName.message}
                                </Alert>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="faculty" md={3}>{t('label.facultyUniversity')}</Label>
                            <Col md={9}>
                                <Input type="text" name="faculty" id="faculty"
                                    value={education.faculty.value || ''}
                                    onChange={(e) => this.handleChange(e, education.id.value)}
                                    onKeyDown={this.handleKeyDown}
                                    onBlur={this.handleBlur}
                                    disabled={buttonEdit.isDisable}
                                />
                                <Alert color="danger" isOpen={!(education.faculty.isValid)}>
                                    {education.faculty.message}
                                </Alert>
                            </Col>
                        </FormGroup>
                        <FormGroup row>
                            <Label for="studySkills" md={3}>{t('label.learningSkill')}</Label>
                            <Col md={9}>
                                <Input type="textarea" name="studySkills" id="studySkills" rows='3'
                                    value={education.studySkills.value || ''}
                                    onChange={(e) => this.handleChange(e, education.id.value)}
                                    onKeyDown={this.handleKeyDown}
                                    onBlur={this.handleBlur}
                                    disabled={buttonEdit.isDisable}
                                />
                                <Alert color="danger" isOpen={!(education.studySkills.isValid)}>
                                    {education.studySkills.message}
                                </Alert>
                            </Col>
                        </FormGroup>
                        {Line}
                       </React.Fragment>
                    );
                })}
            </>
        );
    }
}
export default withTranslation('common')(TabSchools);
