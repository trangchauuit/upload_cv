import React from "react";
import { FormGroup, Label, Input, Col, Alert } from "reactstrap";
import moment from 'moment';
import DatePicker, { registerLocale} from 'react-datepicker';
import { withTranslation } from 'react-i18next';
import "react-datepicker/dist/react-datepicker.css";
import ja from 'date-fns/locale/ja';
import vi from 'date-fns/locale/vi';
import en from 'date-fns/locale/en-US';
registerLocale('ja', ja);
registerLocale('vi', vi);
registerLocale('en', en);

class TabWorks extends React.PureComponent {

  constructor(props) {
    super(props);
    this.state = {
      dataWorks: this.props.dataWorks,
      buttonEdit: { isDisable: true }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.dataWorks !== this.state.dataWorks) {
      this.setState({ dataWorks: nextProps.dataWorks });
    }
    // if(nextProps.buttonEdit !== this.state.buttonEdit){
    //   this.setState({ buttonEdit: nextProps.buttonEdit });
    // }
  }

  handleChange = (e, id) => {
    this.setState({
      dataWorks: this.state.dataWorks.map(el => (el.id.value === id ? {
        ...el,
        [e.target.name]:{
          ...el[e.target.name],
          value: e.target.value
        }
      } : el))
    });
  }

  handleChangeSelect = (e, id) => {
    this.setState({
      dataWorks: this.state.dataWorks.map(el => (el.id.value === id ? {
        ...el,
        [e.target.name]:{
          ...el[e.target.name],
          value: e.target.value
        }} : el))
    }, () => {this.props.onDataWorks(this.state.dataWorks)});
  }

  handleChangeDate = (date, field, id) => {
    let valueSelected = date ? moment(new Date(date)).format('YYYY/MM') : null;
    let answerArray = valueSelected ? valueSelected.split('/') : [];
    this.setState({
      dataWorks: this.state.dataWorks.map(el => (el.id.value === id ? {
        ...el,
        [field + 'Year']: {
          ...el[field + 'Year'],
          value: answerArray[0]
        },
        [field + 'Month']: {
          ...el[field + 'Month'],
          value: answerArray[1]
        }
      } : el))
    }, () => {this.props.onDataWorks(this.state.dataWorks)});
  }

  handleKeyDown = (e) => {
    if (e.key === 'Tab') {
      this.props.onDataWorks(this.state.dataWorks);
    }
  }

  handleBlur = () => {
    this.props.onDataWorks(this.state.dataWorks);
  }
  render() {
    const { t } = this.props;
    const { dataWorks, buttonEdit } = this.state;
    const Line = <hr className="hr-primary" />;
    return(
      <>
        <FormGroup row>
          <Label className="col-form-label font-weight-bold">
            {t('label.work')}
          </Label>
        </FormGroup>

        {dataWorks.map((work) => {
          let hiringPeriod = "", retirementPeriod = "";
          if (work.fromYear.value && work.fromMonth.value){
            hiringPeriod = work.fromYear.value + "/" + work.fromMonth.value + "/01";
          }
          if (work.toYear.value && work.toMonth.value){
            retirementPeriod = work.toYear.value + "/" + work.toMonth.value + "/01";
          }
          return (
            <React.Fragment key={work.id.value}>
              <FormGroup row>
                <Label for="company" md={3}>{t('label.companyName')}</Label>
                <Col md={9}>
                  <Input type="text" name="company" id="company"
                    value={work.company.value || ""}
                    onChange={(e) => this.handleChange(e, work.id.value)}
                    onKeyDown={this.handleKeyDown}
                    onBlur={this.handleBlur}
                    disabled={buttonEdit.isDisable}
                    />
                  <Alert color="danger" isOpen={!(work.company.isValid)}>
                    {work.company.message}
                  </Alert>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="hiringPeriod" md={3}>{t('label.hiringPeriod')}</Label>
                <Col md={9}>
                  <DatePicker name="hiringPeriod"  id="hiringPeriod"
                    selected={hiringPeriod ? new Date(hiringPeriod) : null}
                    onChange={(e) => this.handleChangeDate(e, 'from', work.id.value)}
                    onKeyDown={this.handleKeyDown}
                    onBlur={this.handleBlur}
                    dateFormat="yyyy/MM"
                    className="form-control"
                    locale={t('opt.language')}
                    showMonthYearPicker
                    showFullMonthYearPicker
                    disabled={buttonEdit.isDisable}
                    />
                  <Alert color="danger" isOpen={!(work.fromMonth.isValid)}>
                    {work.fromMonth.message}
                  </Alert>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="retirementPeriod" md={3}>{t('label.retirementPeriod')}</Label>
                <Col md={9}>
                  <DatePicker name="retirementPeriod"  id="retirementPeriod"
                    selected={retirementPeriod ? new Date(retirementPeriod) : null}
                    onChange={(e) => this.handleChangeDate(e, 'to', work.id.value)}
                    onKeyDown={this.handleKeyDown}
                    onBlur={this.handleBlur}
                    dateFormat="yyyy/MM"
                    className="form-control"
                    locale={t('opt.language')}
                    showMonthYearPicker
                    showFullMonthYearPicker
                    disabled={buttonEdit.isDisable}
                    />
                  <Alert color="danger" isOpen={!(work.toMonth.isValid)}>
                    {work.toMonth.message}
                  </Alert>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="contractType" md={3}>{t('label.employmentForm')}</Label>
                <Col md={9}>
                  <Input type="select" name="contractType" id="contractType"
                    value={work.contractType.value || 0}
                    onChange={ (e) => this.handleChangeSelect(e, work.id.value)}
                    onKeyDown={this.handleKeyDown}
                    onBlur={this.handleBlur}
                    disabled={buttonEdit.isDisable}
                  >
                    <option value={4}>{t('label.fullTimeEmployee')}</option>
                    <option value={3}>{t('label.contractEmployee')}</option>
                    <option value={2}>{t('label.temporaryStaff')}</option>
                    <option value={1}>{t('label.partTimeJob')}</option>
                    <option value={5}>{t('label.internship')}</option>
                    <option value={6}>{t('label.volunteer')}</option>
                    <option value={7}>{t('label.enlistment')}</option>
                  </Input>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="workPlaceType" md={3}>{t('label.corporateLocation')}</Label>
                <Col md={9}>
                  <Input type="select" name="workPlaceType" id="workPlaceType"
                    value={work.workPlaceType.value || 0}
                    onChange={(e) => this.handleChangeSelect(e, work.id.value)}
                    onKeyDown={this.handleKeyDown}
                    onBlur={this.handleBlur}
                    disabled={buttonEdit.isDisable}
                  >
                    <option value={1}>{t('label.japan')}</option>
                    <option value={2}>{t('label.vn')}</option>
                    <option value={3}>{t('label.other')}</option>
                  </Input>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="workContent" md={3}>{t('label.businessContent')}</Label>
                <Col md={9}>
                  <Input type="textarea" name="workContent" id="workContent" rows='3'
                    value={work.workContent.value || ""}
                    onChange={(e) => this.handleChange(e, work.id.value)}
                    onKeyDown={this.handleKeyDown}
                    onBlur={this.handleBlur}
                    disabled={buttonEdit.isDisable}
                    />
                  <Alert color="danger" isOpen={!(work.workContent.isValid)}>
                    {work.workContent.message}
                  </Alert>
                </Col>
              </FormGroup>
              <FormGroup row>
                <Label for="workSkill" md={3}>{t('label.learningSkill')}</Label>
                <Col md={9}>
                  <Input type="textarea" name="workSkill" id="workSkill" rows='3'
                    value={work.workSkill.value || ""}
                    onChange={(e) => this.handleChange(e, work.id.value)}
                    onKeyDown={this.handleKeyDown}
                    onBlur={this.handleBlur}
                    disabled={buttonEdit.isDisable}
                    maxLength={900}
                    />
                  <Alert color="danger" isOpen={!(work.workSkill.isValid)}>
                    {work.workSkill.message}
                  </Alert>
                </Col>
              </FormGroup>
              {Line}
            </React.Fragment>
          );
        })}
        </>
    );
  }
}
export default withTranslation('common')(TabWorks);
