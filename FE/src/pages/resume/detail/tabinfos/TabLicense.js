import React from "react";
import { FormGroup, Label, Input, Col, Alert } from "reactstrap";
import moment from 'moment';
import DatePicker, { registerLocale} from 'react-datepicker';
import { withTranslation } from 'react-i18next';
import "react-datepicker/dist/react-datepicker.css";
import ja from 'date-fns/locale/ja';
import en from 'date-fns/locale/en-US';
import vi from 'date-fns/locale/vi';
registerLocale('ja', ja);
registerLocale('en', en);
registerLocale('vi', vi);

class TabLicense extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dataLicense: this.props.dataLicense,
      buttonEdit: {
        isDisable: true,
      }
    };
  }

  componentWillReceiveProps(nextProps) {
    if (nextProps.dataLicense !== this.state.dataLicense) {
      this.setState({ dataLicense: nextProps.dataLicense });
    }

    // if(nextProps.buttonEdit !== this.state.buttonEdit){
    //   this.setState({ buttonEdit: nextProps.buttonEdit });
    // }
  }

  getOtherLienceIndex = () => {
    const currentLicenses = this.state.dataLicense;
    const ids = currentLicenses.filter(cl => typeof cl.id.value === 'number').map(cl => cl.id.value);
    const smallestId = Math.min(...ids);
    return smallestId <= 0 ? smallestId - 5 : -1;
  }

  handleAddOtherLicense = () => {
    if (this.isDisableButton()) return;

    const currentLicenses = this.state.dataLicense;
    const defaultData = {
      "id": {
        "value": this.getOtherLienceIndex(),
        "isValid": true,
        "message": ""
      },
      "candidateId": {
        "value": 177,
        "isValid": true,
        "message": ""
      },
      "name": {
        "value": "その他",
        "isValid": true,
        "message": ""
      },
      "level": {
        "value": "",
        "isValid": true,
        "message": ""
      },
      "year": {
        "value": "",
        "isValid": true,
        "message": ""
      },
      "month": {
        "value": "",
        "isValid": true,
        "message": ""
      }
    }

    currentLicenses.push(defaultData);
    this.setState({ dataLicense: [...currentLicenses] });
  }

  handleRemoveOtherLicense = (id) => {
    if (this.state.buttonEdit.isDisable) return;

    const licenses = this.state.dataLicense.filter(l => l.id.value !== id);
    this.setState({ dataLicense: [...licenses] });
  }

  isNewOtherLicense = (id) => {
    if(id > 0) return false;

    return this.state.dataLicense.filter(l => l.id.value === id && id < 0).length > 0;
  }

  getOtherLiences = () => {
    return this.state.dataLicense.filter(l => l.name.value === 'その他');
  }

  isDisableButton = () => {
    return this.state.buttonEdit.isDisable || this.getOtherLiences().length >= 5;
  }

  handleChange = (e, id) => {
    this.setState({
      dataLicense: this.state.dataLicense.map(el => (el.id.value === id ? {
        ...el,
        [e.target.name]:{
          ...el[e.target.name],
          value: e.target.value
        }
      } : el))
    });
  }
  handleChangeDate = (date, id) => {
    let valueSelected = date ? moment(new Date(date)).format('YYYY/MM') : null;
    let answerArray = valueSelected ? valueSelected.split('/') : [];
    this.setState({
      dataLicense: this.state.dataLicense.map(el => (el.id.value === id ? {
        ...el,
        year: {
          ...el.year,
          value: answerArray[0]
        },
        month: {
          ...el.month,
          value: answerArray[1]
        }
      } : el))
    }, () => {this.props.onDataLicense(this.state.dataLicense)});
  }

  handleChangeSelect = (e, id) => {
    this.setState({
      dataLicense: this.state.dataLicense.map(el => (el.id.value === id ? {
        ...el,
        [e.target.name]:{
          ...el[e.target.name],
          value: e.target.value
        }} : el))
    }, () => {this.props.onDataLicense(this.state.dataLicense)});

  }
  handleKeyDown = (e) => {
    if (e.key === 'Tab') {
      this.props.onDataLicense(this.state.dataLicense);
    }
  }
  handleBlur = () => {
    this.props.onDataLicense(this.state.dataLicense);
  }

  render() {
    const { t } = this.props;
    const { dataLicense, buttonEdit } = this.state;
    let lang = `${t('opt.language')}`;
    let otherLienceIndex = 0;
    return(
      <>
        <FormGroup row>
          <Label className="col-form-label font-weight-bold">
            {t('label.applicantQualificationsLicenses')}
          </Label>
        </FormGroup>
        {dataLicense.map((license) => {
          let nameLicense = license.name.value;
          let getTime = "";
          if (license.year.value && license.month.value && (license.year.value + "") !== "0"  && (license.month.value + "") !== "0"){
            getTime = license.year.value + "/" + license.month.value + "/01";
          }

          if (nameLicense.indexOf(("日本語能力")) !== -1){
            return (
              <React.Fragment key={license.id.value}>
                <FormGroup row>
                  <Label for="japanese" md={3}>{t('label.japanese')}</Label>
                  <Col md={9}>
                    <Input type="select" name="level" id="japanese"
                      value={license.level.value || ''}
                      onChange={(e) => this.handleChangeSelect(e, license.id.value)}
                      onKeyDown={this.handleKeyDown}
                      onBlur={this.handleBlur}
                      disabled={buttonEdit.isDisable}
                    >
                      <option value={""}>{t('label.choice')}</option>
                      <option value="N1">N1</option>
                      <option value="N2">N2</option>
                      <option value="N3">N3</option>
                      <option value="N4">N4</option>
                      <option value="N5">N5</option>
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="dateGetJapanese" md={3}>{t('label.getYearAndMonth')}</Label>
                  <Col md={9}>
                    <DatePicker name="dateGetJapanese"  id="dateGetJapanese"
                      selected={getTime ? new Date(getTime) : null}
                      onChange={(e) => this.handleChangeDate(e, license.id.value)}
                      onKeyDown={this.handleKeyDown}
                      onBlur={this.handleBlur}
                      dateFormat="yyyy/MM"
                      className="form-control"
                      locale={lang}
                      showMonthYearPicker
                      showFullMonthYearPicker
                      disabled={buttonEdit.isDisable}
                      />
                    <Alert color="danger" isOpen={!(license.year.isValid)}>
                      {license.year.message}
                    </Alert>
                  </Col>
                </FormGroup>
              </React.Fragment>
            )
          }else if (nameLicense.includes("英語")){
            return (
              <React.Fragment key={license.id}>
                <FormGroup row>
                  <Label for="pointToeic" md={3}>{t('label.english')}</Label>
                  <Col md={9}>
                    <Input type="text" name="level" id="pointToeic"
                      value={license.level.value || ''}
                      onChange={(e) => this.handleChange(e, license.id.value)}
                      onKeyDown={this.handleKeyDown}
                      onBlur={this.handleBlur}
                      disabled={buttonEdit.isDisable}
                      />
                    <Alert color="danger" isOpen={!(license.level.isValid)}>
                      {license.level.message}
                    </Alert>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="dateGetToeic" md={3}>{t('label.getYearAndMonth')}</Label>
                  <Col md={9}>
                    <DatePicker name="dateGetToeic"  id="dateGetToeic"
                      selected={getTime ? new Date(getTime) : null}
                      onChange={(e) => this.handleChangeDate(e, license.id.value)}
                      onKeyDown={this.handleKeyDown}
                      onBlur={this.handleBlur}
                      dateFormat="yyyy/MM"
                      className="form-control"
                      locale={lang}
                      showMonthYearPicker
                      showFullMonthYearPicker
                      disabled={buttonEdit.isDisable}
                      />
                    <Alert color="danger" isOpen={!(license.year.isValid)}>
                      {license.year.message}
                    </Alert>
                  </Col>
                </FormGroup>
              </React.Fragment>
            )
          }else if (nameLicense.includes("日本の自動車免許")){
            return (
              <React.Fragment key={license.id.value}>
                <FormGroup row>
                  <Label for="japaneseCarLicense" md={3}>{t('label.japaneseCarLicense')}</Label>
                  <Col md={9}>
                    <Input type="select" name="level" id="japaneseCarLicense"
                      value={license.level.value || ''}
                      onChange={(e) => this.handleChangeSelect(e, license.id.value)}
                      onKeyDown={this.handleKeyDown}
                      onBlur={this.handleBlur}
                      disabled={buttonEdit.isDisable}
                    >
                      <option value={""}>{t('label.choice')}</option>
                      <option value="有/Có">{t('label.yes')}</option>
                      <option value="無/Không">{t('label.no')}</option>
                    </Input>
                  </Col>
                </FormGroup>
                <FormGroup row>
                  <Label for="dateGetJapaneseCarLicense" md={3}>{t('label.getYearAndMonth')}</Label>
                  <Col md={9}>
                    <DatePicker name="dateGetJapaneseCarLicense"  id="dateGetJapaneseCarLicense"
                      selected={getTime ? new Date(getTime) : null}
                      onChange={(e) => this.handleChangeDate(e, license.id.value)}
                      onKeyDown={this.handleKeyDown}
                      onBlur={this.handleBlur}
                      dateFormat="yyyy/MM"
                      className="form-control"
                      locale={lang}
                      showMonthYearPicker
                      showFullMonthYearPicker
                      disabled={buttonEdit.isDisable}
                      />
                    <Alert color="danger" isOpen={!(license.year.isValid)}>
                      {license.year.message}
                    </Alert>
                  </Col>
                </FormGroup>
              </React.Fragment>
            )
          }else{
            let el = (
              <React.Fragment key={license.id.value}>
                <FormGroup row>
                  <Label for="otherLience" md={3}>{otherLienceIndex === 0 ? t('label.other') : ''}</Label>
                  <Col md={this.isNewOtherLicense(license.id.value) ? 8 : 9}>
                    <Input type="text" name="level" id="otherLience"
                      value={license.level.value || ""}
                      onChange={(e) => this.handleChange(e, license.id.value)}
                      onKeyDown={this.handleKeyDown}
                      onBlur={this.handleBlur}
                      disabled={buttonEdit.isDisable}
                      />
                    <Alert color="danger" isOpen={!(license.level.isValid)}>
                      {license.level.message}
                    </Alert>
                  </Col>
                  {this.isNewOtherLicense(license.id.value) && <Col md={1} className="p-0">
                    <button
                      className="btn btn-outline-danger btn-sm"
                      type="button"
                      disabled={this.state.buttonEdit.isDisable}
                      onClick={() => this.handleRemoveOtherLicense(license.id.value)}
                    >
                      Delete
                    </button>
                  </Col>
                }
                </FormGroup>
                <FormGroup row>
                  <Label for="dateGetOtherLience" md={3}>{otherLienceIndex === 0 ? t('label.getYearAndMonth') : ''}</Label>
                  <Col md={9}>
                    <DatePicker name="dateGetOtherLience"  id="dateGetOtherLience"
                      selected={getTime ? new Date(getTime) : null}
                      onChange={(e) => this.handleChangeDate(e, license.id.value)}
                      onKeyDown={this.handleKeyDown}
                      onBlur={this.handleBlur}
                      dateFormat="yyyy/MM"
                      className="form-control"
                      locale={lang}
                      showMonthYearPicker
                      showFullMonthYearPicker
                      disabled={buttonEdit.isDisable}
                      />
                    <Alert color="danger" isOpen={!(license.year.isValid)}>
                      {license.year.message}
                    </Alert>
                  </Col>
                </FormGroup>
              </React.Fragment>
            )
            otherLienceIndex++;
            return el;
          }
        })}
       {/*
        <React.Fragment key="button-row">
          <FormGroup row>
            <Label md={3}></Label>
            <Col md={9}>
              <button
                onClick={this.handleAddOtherLicense}
                type="button"
                disabled={this.isDisableButton()}
                className="btn btn-sm btn-outline-primary">
                Add more
              </button>
            </Col>
          </FormGroup>
        </React.Fragment>
        */}
        </>
    );
  }
}
export default withTranslation('common')(TabLicense);
