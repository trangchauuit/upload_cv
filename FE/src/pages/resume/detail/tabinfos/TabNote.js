import React from "react";
import { FormGroup, Label, Input, Col } from "reactstrap";
import { withTranslation } from 'react-i18next';

class TabNote extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      note: this.props.note,
      buttonEdit: { isDisable: true }
    };
  }
  componentWillReceiveProps(nextProps) {
    if(nextProps.buttonEdit !== this.state.buttonEdit){
      this.setState({ buttonEdit: nextProps.buttonEdit });
    }
    if(nextProps.note!== this.state.note){
      this.setState({ note: nextProps.note });
    }
  }

  handleChange = (e) => {
    this.setState({ note: e.target.value });
  }
  handleKeyDown = (e) => {
    if (e.key === 'Tab') {
      this.props.onDataNote(this.state.note);
    }
  }
  handleBlur = () => {
    this.props.onDataNote(this.state.note);
  }
  render() {
    const { t } = this.props;
    const { note, buttonEdit } = this.state;
    return(
      <>
      <FormGroup row>
        <Label className="col-form-label font-weight-bold">
          {t('label.note')}
        </Label>
      </FormGroup>
      <FormGroup row>
        <Col md={12}>
          <Input type="textarea" name="note" id="note" rows='10'
            value={note || ""}
            onChange={this.handleChange}
            onKeyDown={this.handleKeyDown}
            onBlur={this.handleBlur}
            disabled={buttonEdit.isDisable}
          />
        </Col>
      </FormGroup>
      </>
    );
  }
}
export default withTranslation('common')(TabNote);

