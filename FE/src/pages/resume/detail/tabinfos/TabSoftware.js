import React from "react";
import { FormGroup, Label, Input, Col, Alert } from "reactstrap";
import { withTranslation } from 'react-i18next';

class TabSoftware extends React.PureComponent {

    constructor(props) {
        super(props);
        this.state = {
            dataSoftware: this.props.dataSoftware,
            buttonEdit: {
        isDisable: true
      }
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.dataSoftware !== this.state.dataSoftware) {
          this.setState({ dataSoftware: nextProps.dataSoftware });
        }
        // if(nextProps.buttonEdit !== this.state.buttonEdit){
        //     this.setState({ buttonEdit: nextProps.buttonEdit });
        // }
    }

    handleChange = (e, id) => {
        this.setState({
            dataSoftware: this.state.dataSoftware.map(el => (el.id.value === id ? {
                ...el,
                software:{
                    ...el.software,
                    value: e.target.value
                }
                } : el))
        });
    }
    handleChangeNumber = (e, id) => {
        const re = /^[0-9\b]+$/;
        if (e.target.value === '' || re.test(e.target.value)) {
            this.setState({
                dataSoftware: this.state.dataSoftware.map(el => (el.id.value === id ? {
                    ...el,
                    [e.target.name]:{
                        ...el[e.target.name],
                        value: e.target.value
                    }
                    } : el))
            });
        }
    }
    handleChangeSelect = (e, id) => {
        this.setState({
            dataSoftware: this.state.dataSoftware.map(el => (el.id.value === id ? {
                ...el,
                year:{
                    ...el.year,
                    value: e.target.value + "" === "-1" ? "" : e.target.value
                }} : el))
        }, () => {this.props.onDataSoftware(this.state.dataSoftware)});

    }
    handleKeyDown = (e) => {
        if (e.key === 'Tab') {
            this.props.onDataSoftware(this.state.dataSoftware);
        }
    }
    handleBlur = () => {
        this.props.onDataSoftware(this.state.dataSoftware);
    }
    render() {
        const { t } = this.props;
        const { dataSoftware, buttonEdit } = this.state;
        return(
            <>
                <FormGroup row>
                    <Label className="col-form-label font-weight-bold">
                    {t('label.toolsSoftwareCanBeUsed')}
                    </Label>
                </FormGroup>
                {dataSoftware.map((software) => {
                    let nameSoftware = software.software.value || "";
                    let yearSelect = software.year.value + "" === ""? -1: software.year.value + "" === "0"? 0: 1;
                    if (nameSoftware.indexOf(("AutoCAD")) !== -1
                        || nameSoftware.includes("Tfas")
                        || nameSoftware.includes("JW CAD")
                        || nameSoftware.includes("SOLIDWORKS")){
                        return (
                            <React.Fragment key={software.id.value}>
                                <FormGroup row>
                                    <Label for="nameSoftware" md={3}>{nameSoftware}</Label>
                                    <Col md={3}>
                                        <Input type="select" name="nameSoftware" id={'nameSoftware' + software.id.value}
                                            value={yearSelect}
                                            onChange={(e) => this.handleChangeSelect(e, software.id.value)}
                                            onKeyDown={this.handleKeyDown}
                                            onBlur={this.handleBlur}
                                            disabled={buttonEdit.isDisable}
                                        >
                                            <option value={-1}>{t('label.choice')}</option>
                                            <option value={1}>{t('label.yes')}</option>
                                            <option value={0}>{t('label.no')}</option>
                                        </Input>
                                    </Col>
                                    <Label for="year" md={3}>{t('label.usePeriod')}</Label>
                                    <Col md={3}>
                                        <Input type="text" name="year" id={'year' + software.id.value}
                                            value={software.year.value}
                                            onChange={(e) => this.handleChangeNumber(e, software.id.value)}
                                            onKeyDown={this.handleKeyDown}
                                            onBlur={this.handleBlur}
                                            disabled={buttonEdit.isDisable}
                                        />
                                        <Alert color="danger" isOpen={!(software.year.isValid)}>
                                            {software.year.message}
                                        </Alert>
                                    </Col>
                                </FormGroup>
                            </React.Fragment>
                        )
                    }else {
                        return (
                            <React.Fragment key={software.id.value}>
                                <FormGroup row>
                                    <Label for=", 'nameSoftware'" md={3}>{t('label.other')}</Label>
                                    <Col md={9}>
                                        <Input type="text" name="nameSoftware" id={'nameSoftware' + software.id.value}
                                            value={software.nameSoftware ? software.nameSoftware.value: software.software.value}
                                            onChange={(e) => this.handleChange(e, software.id.value)}
                                            onKeyDown={this.handleKeyDown}
                                            onBlur={this.handleBlur}
                                            disabled={buttonEdit.isDisable}
                                        />
                                         <Alert color="danger" isOpen={!(software.software.isValid)}>
                                            {software.software.message}
                                        </Alert>
                                    </Col>
                                </FormGroup>
                                <FormGroup row>
                                    <Label for="year" md={3}>{t('label.usePeriod')}</Label>
                                    <Col md={9}>
                                        <Input type="text" name="year" id={'year' + software.id.value}
                                            value={software.year.value || ""}
                                            onChange={(e) => this.handleChangeNumber(e, software.id.value)}
                                            onKeyDown={this.handleKeyDown}
                                            onBlur={this.handleBlur}
                                            disabled={buttonEdit.isDisable}
                                        />
                                        <Alert color="danger" isOpen={!(software.year.isValid)}>
                                            {software.year.message}
                                        </Alert>
                                    </Col>
                                </FormGroup>
                            </React.Fragment>
                        )
                    }
                })}
            </>
        );
    }
}
export default withTranslation('common')(TabSoftware);
