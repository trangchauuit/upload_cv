import React from "react";
import { FormGroup, Label, Input, Col, Row } from "reactstrap";
import moment from 'moment';
import DatePicker, { registerLocale } from 'react-datepicker';
import CompanyType from './../../CompanyType';
import { withTranslation } from 'react-i18next';
import "react-datepicker/dist/react-datepicker.css";
import ja from 'date-fns/locale/ja';
import vi from 'date-fns/locale/vi';
import en from 'date-fns/locale/en-US';
registerLocale('ja', ja);
registerLocale('vi', vi);
registerLocale('en', en);

class TabCommonInfo extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            dataCommonInfo: this.props.dataCommonInfo,
            buttonEdit: this.props.buttonEdit
        };
    }

    componentWillReceiveProps(nextProps) {
        if (nextProps.buttonEdit !== this.state.buttonEdit) {
            this.setState({ buttonEdit: nextProps.buttonEdit });
        }
    }

    handleChangeSelect = (e) => {
        this.setState({
            dataCommonInfo: {
                ...this.state.dataCommonInfo,
                [e.target.name]: e.target.value
            }
        }, () => this.props.onDataCommonInfo(this.state.dataCommonInfo));
    }

    handleChangeDate = (date, field) => {
        this.setState({
            dataCommonInfo: {
                ...this.state.dataCommonInfo,
                [field]: date ? moment(new Date(date)).format('YYYY/MM/DD') : null
            }
        }, () => this.props.onDataCommonInfo(this.state.dataCommonInfo));
    }
    handleChange = (e) => {
        this.setState({
            dataCommonInfo: {
                ...this.state.dataCommonInfo,
                [e.target.name]: e.target.value
            }
        });
    }

    handleKeyDown = (e) => {
        if (e.key === 'Tab') {
            this.props.onDataCommonInfo(this.state.dataCommonInfo);
        }
    }

    handleBlur = () => {
        this.props.onDataCommonInfo(this.state.dataCommonInfo);
    }

    handleDataCompanyType = (introduceCompanyTypeId) => {
        this.setState({
            dataCommonInfo: {
                ...this.state.dataCommonInfo,
                introduceCompanyTypeId: introduceCompanyTypeId
            }
        }, () => this.props.onDataCommonInfo(this.state.dataCommonInfo));
    }

    render() {
        const { t } = this.props;
        const { dataCommonInfo, buttonEdit } = this.state;
        return (
            <>
                <>
                    <FormGroup row >
                        <Label className="col-form-label font-weight-bold">
                            {t('label.referralInformation')}
                        </Label>
                    </FormGroup>
                    <Row className="mt-3">
                        <Col md={8}>
                            <Row>
                                <Col md={6}>
                                    <FormGroup row>
                                        <Label for="status" md={4}>{t('label.progress')}</Label>
                                        <Col md={8}>
                                            <Input type="select" name="status" id="status"
                                                value={dataCommonInfo.status}
                                                onChange={this.handleChangeSelect}
                                                onKeyDown={this.handleKeyDown}
                                                onBlur={this.handleBlur}
                                                disabled={buttonEdit.isDisable}
                                            >
                                                <option value={0}>{t('label.choice')}</option>
                                                <option value={9}>{t('label.registered')}</option>
                                                <option value={10}>{t('label.interviewed')}</option>
                                                <option value={4}>{t('label.underSelection')}</option>
                                                {/*<option value={5}>{t('label.unofficialOffer')}</option>*/}
                                                <option value={11}>{t('label.atWorkAfterSales')}</option>
                                                <option value={12}>{t('label.atWork')}</option>
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                </Col>
                                <Col md={6}>
                                    <FormGroup row>
                                        <Label for="introduceCompanyType" md={4}>{t('label.introduceCompanyTypeId')}</Label>
                                        <Col md={8}>
                                            <CompanyType
                                                buttonEdit={buttonEdit}
                                                introduceCompanyTypeId={dataCommonInfo.introduceCompanyTypeId}
                                                onDataCompanyTypes={this.handleDataCompanyType}
                                            />
                                        </Col>
                                    </FormGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col md={6}>
                                    <FormGroup row>
                                        <Label for="applicationDate" md={4}>{t('label.applicationDate')}</Label>
                                        <Col md={8}>
                                            <DatePicker name="applicationDate" id="applicationDate"
                                                selected={dataCommonInfo.applicationDate ? new Date(dataCommonInfo.applicationDate) : null}
                                                onChange={(e) => this.handleChangeDate(e, 'applicationDate')}
                                                onKeyDown={this.handleKeyDown}
                                                onBlur={this.handleBlur}
                                                dateFormat="yyyy/MM/dd"
                                                className="form-control"
                                                locale={t('opt.language')}
                                                disabled={buttonEdit.isDisable}
                                            />
                                        </Col>
                                    </FormGroup>
                                </Col>
                                <Col md={6}>
                                    <FormGroup row>
                                        <Label for="introduceCompanyName" md={4}>{t('label.introduceCompanyName')}</Label>
                                        <Col md={8}>
                                            <Input type="text" name="introduceCompanyName" id="introduceCompanyName"
                                                value={dataCommonInfo.introduceCompanyName || ""}
                                                onChange={this.handleChange}
                                                onKeyDown={this.handleKeyDown}
                                                onBlur={this.handleBlur}
                                                disabled={buttonEdit.isDisable}
                                            >
                                            </Input>
                                        </Col>
                                    </FormGroup>
                                </Col>
                            </Row>
                        </Col>
                        <Col md={4}>
                            <FormGroup row>
                                <Label for="remark" md={4}>{t('label.remarks')}</Label>
                                <Col md={8}>
                                    <Input type="textarea" name="remark" id="remark"
                                        value={dataCommonInfo.remark || ""}
                                        onChange={this.handleChange}
                                        onKeyDown={this.handleKeyDown}
                                        onBlur={this.handleBlur}
                                        disabled={buttonEdit.isDisable}
                                    />
                                </Col>
                            </FormGroup>
                        </Col>
                    </Row>
                </>
            </>
        );
    }
}
export default withTranslation('common')(TabCommonInfo);
