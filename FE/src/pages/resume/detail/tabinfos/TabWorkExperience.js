import React from "react";
import { FormGroup, Label, Input, Col } from "reactstrap";
import { withTranslation } from 'react-i18next';

class TabWorkExperience extends React.PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            dataWorkExperience: this.props.dataWorkExperience,
            buttonEdit: { isDisable: true }
        };
    }

    // componentWillReceiveProps(nextProps) {
    //     if(nextProps.buttonEdit !== this.state.buttonEdit){
    //         this.setState({ buttonEdit: nextProps.buttonEdit });
    //     }
    // }

    handleChange = (e) => {
        this.setState({
            dataWorkExperience: {
                ...this.state.dataWorkExperience,
                [e.target.name]: e.target.value
            }
        });
    }
    handleKeyDown = (e) => {
        if (e.key === 'Tab') {
            this.props.onDataWorkExperience(this.state.dataWorkExperience);
        }
    }
    handleBlur = () => {
        this.props.onDataWorkExperience(this.state.dataWorkExperience);
    }
    render() {
        const { t } = this.props;
        const { dataWorkExperience, buttonEdit } = this.state;
        return(
            <>
                <FormGroup row>
                    <Label className="col-form-label font-weight-bold">
                        {t('label.motivationForSelfPromotionApplication')}
                    </Label>
                </FormGroup>
                <FormGroup row>
                    <Col md={12}>
                        <Input type="textarea" name="selfPr" id="selfPr" rows='10'
                            value={dataWorkExperience.selfPr || ""}
                            onChange={this.handleChange}
                            onKeyDown={this.handleKeyDown}
                            onBlur={this.handleBlur}
                            disabled={buttonEdit.isDisable}
                        />
                    </Col>
                </FormGroup>
            </>
        );
    }
}
export default withTranslation('common')(TabWorkExperience);
