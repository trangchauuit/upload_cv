import React from "react";
import { FormGroup, Label, Input, Col } from "reactstrap";
import { withTranslation } from 'react-i18next';

class TabLanguage extends React.PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      dataLanguage: this.props.dataLanguage,
      buttonEdit: {
        isDisable: true,
      }
    };
  }

  // componentWillReceiveProps(nextProps) {
  //   if(nextProps.buttonEdit !== this.state.buttonEdit){
  //     this.setState({ buttonEdit: nextProps.buttonEdit });
  //   }
  // }

  handleChange = (e) => {
    this.setState({
      dataLanguage: {
        ...this.state.dataLanguage,
        [e.target.name]: e.target.value
      }
    });
  }
  handleChangeSelect = (e) => {
    this.setState({
      dataLanguage: {
        ...this.state.dataLanguage,
        [e.target.name]: e.target.value
      }
    }, () => this.props.onDataLanguage(this.state.dataLanguage));
  }
  handleKeyDown = (e) => {
    if (e.key === 'Tab') {
      this.props.onDataLanguage(this.state.dataLanguage);
    }
  }
  handleBlur = () => {
    this.props.onDataLanguage(this.state.dataLanguage);
  }

  render() {
    const { t } = this.props;
    const { dataLanguage, buttonEdit } = this.state;
    return(
      <>
        <FormGroup row>
          <Label className="col-form-label font-weight-bold">
            {t('label.applicantLanguageAbility')}
          </Label>
        </FormGroup>
        <FormGroup row>
          <Label for="japanLanguageLevel" md={3}>{t('label.japanese')}</Label>
          <Col md={9}>
            <Input type="select" name="japanLanguageLevel" id="japanLanguageLevel"
              value={dataLanguage.japanLanguageLevel || ''}
              onChange={this.handleChangeSelect}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
            >
              <option value={-1}>{t('label.choice')}</option>
              <option value={1}>{t('label.dailyConversationLevel')}</option>
              <option value={2}>{t('label.businessLevel')}</option>
              <option value={3}>{t('label.nativeLevel')}</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="englishLanguageLevel" md={3}>{t('label.english')}</Label>
          <Col md={9}>
            <Input type="select" name="englishLanguageLevel" id="englishLanguageLevel"
              value={dataLanguage.englishLanguageLevel || ''}
              onChange={this.handleChangeSelect}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
            >
              <option value={-1}>{t('label.choice')}</option>
              <option value={1}>{t('label.dailyConversationLevel')}</option>
              <option value={2}>{t('label.businessLevel')}</option>
              <option value={3}>{t('label.nativeLevel')}</option>
            </Input>
          </Col>
        </FormGroup>
        <FormGroup row>
          <Label for="otherLanguage" md={3}>{t('label.korean')}</Label>
          <Col md={9}>
            <Input type="select" name="koreanLanguageLevel" id="koreanLanguageLevel"
              value={dataLanguage.koreanLanguageLevel || ''}
              onChange={this.handleChangeSelect}
              onKeyDown={this.handleKeyDown}
              onBlur={this.handleBlur}
              disabled={buttonEdit.isDisable}
            >
              <option value={-1}>{t('label.choice')}</option>
              <option value={1}>{t('label.dailyConversationLevel')}</option>
              <option value={2}>{t('label.businessLevel')}</option>
              <option value={3}>{t('label.nativeLevel')}</option>
            </Input>
          </Col>
        </FormGroup>
        </>
    );
  }
}
export default withTranslation('common')(TabLanguage);
