import React from 'react';
import { API } from '../../utils/common';
import { OptionBox, Pagination } from '../../components/common';
import { Button, Row, Col, FormGroup, Label, Input } from 'reactstrap';
import { withRouter } from 'react-router';
import BootstrapTable from 'react-bootstrap-table-next';
import ToolkitProvider from 'react-bootstrap-table2-toolkit';
import 'react-bootstrap-table2-toolkit/dist/react-bootstrap-table2-toolkit.min.css';
import { translateColumn } from '../../utils/mockdata';
import { transformListCandidates } from '../../utils/transformData';
import { withTranslation } from 'react-i18next';
import moment from 'moment';
import swal from 'sweetalert';
import DatePicker, { registerLocale} from 'react-datepicker';
import "react-datepicker/dist/react-datepicker.css";
import ja from 'date-fns/locale/ja';
import vi from 'date-fns/locale/vi';
import en from 'date-fns/locale/en-US';
import CompanyType from './CompanyType';
registerLocale('ja', ja);
registerLocale('vi', vi);
registerLocale('en', en);

class ListCandidates extends React.PureComponent {
  _isMounted = false;

  constructor(props) {
    super(props)
    this.columns = [
      {
        dataField: 'id',
        text: 'ID',
      },
      {
        dataField: 'status',
        text: `${this.props.t('common:label.progress')}`
      },
      {
        dataField: 'name',
        text: `${this.props.t('common:label.name')}`
      },
      {
        dataField: 'createdAt',
        text: `${this.props.t('common:label.applicationDateHeader')}`
      },
      {
        dataField: 'japanLanguageLevel',
        text: `${this.props.t('common:label.japaneseLevel')}`
      },
      {
        dataField: 'japaneseQualification',
        text: `${this.props.t('common:label.japaneseQualification')}`
      },
      {
        dataField: 'cadExperience',
        text: this.props.t('common:label.CADExperience')
      },
      {
        dataField: 'major',
        text: this.props.t('common:label.faculty')
      },
      {
        dataField: 'referralIndustryType',
        text: `${this.props.t('common:label.introduceCompanyTypeId')}`
      },
      {
        dataField: 'referralCompany',
        text: `${this.props.t('common:label.introduceCompanyName')}`
      },
      {
        dataField: 'note',
        text: `${this.props.t('common:label.note')}`
      },
      {
        dataField: "combinedId",
        text: this.props.t('message.actions'),
        formatter: (cellContent, row) => {
          return <button id="deleteButton" key={row.combinedId} style={{ minWidth: '70px' }} className="btn btn-danger btn-xs" onClick={() => this.handleDeleteCandidate(row)}>
            {this.props.t('button.delete')}
          </button>
        },
      },
    ]

    this.state = {
      dataSearch: {},
      datas: [],
      currentPage: 1,
      maxPage: 10,
      pageSize: 15,
      isLoading: true,
      processUpdate: {'queryString1' : '', 'queryString2': '', 'isFinish': true}
    }
  }

  componentDidMount() {
    this._isMounted = true;
    this.getData();
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleDeleteCandidate = async (row) => {
    if (!window.confirm(this.props.t('message.confirmDeleteCandidate'))) return;

    const id = +row.combinedId.split('::')[0]
    console.log(id)
    await API.delete(`/admin/candidates/${id}`)
      .then(response => {
        swal({
          title: this.props.t('message.delete_success'),
          icon: "success",
        });
        this.getData();
      }, error => {
        swal({
          title:  this.props.t('message.delete_fail'),
          icon: "error",
        });
      })
  }

  createQuery = () => {
    const { currentPage, pageSize, dataSearch } = this.state;
    // Query string
    let queryString = `page=${currentPage}&limit=${pageSize}`;
    if (dataSearch.status){
      queryString += `&status=${dataSearch.status}`;
    }
    if (dataSearch.applyDateFrom){
      queryString += `&apply_date_from=${moment(new Date(dataSearch.applyDateFrom)).format('YYYY-MM-DD')}`;
    }
    if (dataSearch.applyDateTo){
      queryString += `&apply_date_to=${moment(new Date(dataSearch.applyDateTo)).format('YYYY-MM-DD')}`;
    }
    if (dataSearch.japaneseLevel){
      queryString += `&japanese_level=${dataSearch.japaneseLevel}`;
    }
    if (dataSearch.japaneseQualification){
      queryString += `&japanese_degree=${dataSearch.japaneseQualification}`;
    }
    if (dataSearch.cadExperience){
      queryString += `&skill=${dataSearch.cadExperience}`;
    }
    if (dataSearch.faculty){
      queryString += `&faculty=${dataSearch.faculty}`;
    }
    if (dataSearch.introduceCompanyTypeId){
      queryString += `&introduce_company_type_id=${dataSearch.introduceCompanyTypeId}`;
    }
    if (dataSearch.introduceCompanyName){
      queryString += `&company_referral=${dataSearch.introduceCompanyName}`;
    }
    return queryString;
  }

  setStateAsync(state) {
    return new Promise((resolve) => {
      this.setState(state, resolve)
    });
  }

  callAPIGetData = async(query) => {
    const response = await API.get(`/admin/candidates?${query}`);
    const listCandidate = await response.data.data;
    if (this._isMounted && listCandidate) {
      this.setState({
        datas: transformListCandidates(listCandidate.candidates, this.props.t),
        maxPage: listCandidate.total_pages,
        totalChilds: listCandidate.total,
        isLoading: false
      })
    }
  }

  getData = async() => {
    if (this.state.processUpdate.isFinish === true){
      let query = this.createQuery();
      await this.setStateAsync({processUpdate : {...this.state.processUpdate, 'queryString1' : query, 'isFinish': false}});
      await this.callAPIGetData(`${query}`);
      await this.setStateAsync({processUpdate : {...this.state.processUpdate, 'queryString2' : this.createQuery(), 'isFinish': true}});
    }
    if (this.state.processUpdate.queryString1 !== this.state.processUpdate.queryString2
        && `${this.createQuery()}` === this.state.processUpdate.queryString2){
      await this.callAPIGetData(`${this.createQuery()}`);
    }
  }

  handleChange =  (e) => {
    this.setState({
      dataSearch: { ...this.state.dataSearch, [e.target.name]: e.target.value }
    }, () => {
      this.getData();
    });
  }

  handleChangeDate = (date, field) => {
    this.setState({
      dataSearch: {
            ...this.state.dataSearch,
            [field]: date ? moment(new Date(date)).format('YYYY/MM/DD') : null
        }
    }, () => {this.getData();});
  }

  handlePaginationPrev = () => {
    this.setState({
      currentPage: this.state.currentPage - 1,
      isLoading: true
    }, () => {
      this.getData();
    })
  }

  handlePaginationNext = () => {
    this.setState({
      currentPage: this.state.currentPage + 1,
      isLoading: true
    }, () => {
      this.getData();
    })
  }

  handleSizePerPage = (pageSize) => {
    this.setState({
      pageSize: pageSize,
      isLoading: true
    }, () => {
      this.getData();
    })
  }

  handleDataCompanyType = (introduceCompanyTypeId) => {
    this.setState(
      {dataSearch: {
        ...this.state.dataSearch,
        'introduceCompanyTypeId': introduceCompanyTypeId
      }}, () => {
      this.getData();
    });
  }
  reset = () => {
    this.setState({
      datas: [],
      dataSearch: {},
      currentPage: 1,
      maxPage: 10,
      pageSize: 15,
      isLoading: true
    }, () => {
      this.getData();
    })
  }

  render() {
    const rowEvents = {
      onClick: (e, row, rowIndex) => {
        if (e.target.id === 'deleteButton') return;

        this.props.history.push(`/candidate/${row.id}`);
      },
    };
    const { dataSearch, datas, currentPage, maxPage, pageSize, isLoading } = this.state;
    const { t } = this.props;
    return (
      <ToolkitProvider
        keyField="id"
        data={datas}
        columns={this.columns}
      >
        {
          props => (
            <div>
              <Row className="mt-4">
                <Col>
                  <h4 className="font-weight-bold">{t('label.resumeList')}</h4>
                </Col>
              </Row>
              <Row>
                <Col md={10}>
                  <Row>
                    <Col md={6}>
                      <Row>
                        <Col md={4}>
                          <FormGroup>
                            <Label for="status" className="font-weight-bold">{t('label.progress')}</Label>
                            <Input  type="select" name="status" id="status" value={dataSearch.status ? dataSearch.status : ''} onChange={this.handleChange}>
                                <OptionBox value={0}>{t('label.pleaseChoose')}</OptionBox>
                                <OptionBox value={9}>{t('label.registered')}</OptionBox>
                                <OptionBox value={10}>{t('label.interviewed')}</OptionBox>
                                <OptionBox value={4}>{t('label.underSelection')}</OptionBox>
                                {/*<OptionBox value={5}>{t('label.unofficialOffer')}</OptionBox>*/}
                                <OptionBox value={11}>{t('label.atWorkAfterSales')}</OptionBox>
                                <OptionBox value={12}>{t('label.atWork')}</OptionBox>
                            </Input>
                          </FormGroup>
                        </Col>
                        <Col md={4}>
                          <FormGroup>
                            <Label for="applyDateFrom" className="font-weight-bold">{t('label.applicationPeriod')}</Label>
                            <DatePicker name="applyDateFrom"  id="applyDateFrom"
                              selected={dataSearch.applyDateFrom ? new Date(dataSearch.applyDateFrom) : null}
                              onChange={(e) => this.handleChangeDate(e, 'applyDateFrom')}
                              dateFormat="yyyy/MM/dd"
                              className="form-control"
                              locale={t('opt.language')}
                            />
                          </FormGroup>
                        </Col>
                        <Col md={4}>
                          <FormGroup>
                            <Label for="applyDateTo" className="font-weight-bold">{'\u00A0'}</Label>
                            <DatePicker name="applyDateTo"  id="applyDateTo"
                              selected={dataSearch.applyDateTo ? new Date(dataSearch.applyDateTo) : null}
                              onChange={(e) => this.handleChangeDate(e, 'applyDateTo')}
                              dateFormat="yyyy/MM/dd"
                              className="form-control"
                              locale={t('opt.language')}
                            />
                          </FormGroup>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={6}>
                      <Row>
                        <Col md={4}>
                            <FormGroup>
                            <Label for="japaneseLevel" className="font-weight-bold">{t('label.japaneseLevel')}</Label>
                            <Input type="select" name="japaneseLevel" id="japaneseLevel" value={dataSearch.japaneseLevel ? dataSearch.japaneseLevel : ''} onChange={this.handleChange}>
                                <option value="">{t('label.pleaseChoose')}</option>
                                <option value={1}>{t('label.nativeLevel')}</option>
                                <option value={2}>{t('label.businessLevel')}</option>
                                <option value={3}>{t('label.dailyConversationLevel')}</option>
                            </Input>
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                            <Label for="japaneseQualification" className="font-weight-bold">{t('label.japaneseQualification')}</Label>
                            <Input type="select" name="japaneseQualification" id="japaneseQualification" value={dataSearch.japaneseQualification ? dataSearch.japaneseQualification : ''} onChange={this.handleChange}>
                                <OptionBox value="">{t('label.pleaseChoose')}</OptionBox>
                                <OptionBox key="1" value="N1">N1</OptionBox>
                                <OptionBox key="2" value="N2">N2</OptionBox>
                                <OptionBox key="3" value="N3">N3</OptionBox>
                                <OptionBox key="4" value="N4">N4</OptionBox>
                                <OptionBox key="5" value="N5">N5</OptionBox>
                            </Input>
                            </FormGroup>
                        </Col>
                        <Col md={4}>
                            <FormGroup>
                              <Label for="cadExperience" className="font-weight-bold">{t('label.CADExperience')}</Label>
                              <Input type="select" name="cadExperience" id="cadExperience" value={dataSearch.cadExperience ? dataSearch.cadExperience : ''} onChange={this.handleChange}>
                                  <OptionBox value="">{t('label.pleaseChoose')}</OptionBox>
                                  <OptionBox value="AutoCAD">AutoCAD</OptionBox>
                                  <OptionBox value="Tfas">Tfas</OptionBox>
                                  <OptionBox value="JW CAD">JW CAD</OptionBox>
                                  <OptionBox value="SOLIDWORKS">SOLIDWORKS</OptionBox>
                                  <OptionBox value="Others">{t('label.others')}</OptionBox>
                              </Input>
                            </FormGroup>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                  <Row>
                    <Col md={6}>
                      <Row>
                        <Col md={8}>
                          <FormGroup>
                            <Label for="faculty" className="font-weight-bold">{t('label.faculty')}</Label>
                            <Input type="text" name="faculty" id="faculty" value={dataSearch.faculty ? dataSearch.faculty : ''} onChange={this.handleChange}/>
                          </FormGroup>
                        </Col>
                        <Col md={4}>
                        <FormGroup>
                          <Label for="introduceCompanyTypeId" className="font-weight-bold">{t('label.introduceCompanyTypeId')}</Label>
                          <CompanyType
                            introduceCompanyTypeId={dataSearch.introduceCompanyTypeId || '0'}
                            onDataCompanyTypes={this.handleDataCompanyType}
                          />
                        </FormGroup>
                        </Col>
                      </Row>
                    </Col>
                    <Col md={6}>
                      <Row>
                        <Col md={8}>
                          <FormGroup>
                            <Label for="introduceCompanyName" className="font-weight-bold">{t('label.introduceCompanyName')}</Label>
                            <Input type="text" name="introduceCompanyName" id="introduceCompanyName" value={dataSearch.introduceCompanyName ? dataSearch.introduceCompanyName : ''} onChange={this.handleChange}>
                            </Input>
                          </FormGroup>
                        </Col>
                      </Row>
                    </Col>
                  </Row>
                </Col>
                <Col md={2}>
                  <Row className="mt-4">
                    <Col md={12}>
                      <Button outline color="primary" onClick={this.reset} id="reset" className="float-right">{t('button.conditionReset')}</Button>
                    </Col>
                  </Row>
                  <Row>
                  </Row>
                </Col>
              </Row>
              <div className="table-custom-hovered">
                <BootstrapTable
                  {...props.baseProps}
                  wrapperClasses="table-responsive"
                  rowEvents={ rowEvents }
                />
              </div>
              {
                !isLoading && <Pagination
                  handlePrev={this.handlePaginationPrev}
                  handleNext={this.handlePaginationNext}
                  handleSizePage={this.handleSizePerPage}
                  showPrevButton={currentPage > 1}
                  showNextButton={currentPage < maxPage}
                  currentPage={currentPage}
                  pageSize={pageSize}
                />
              }
            </div>
          )
        }
      </ToolkitProvider>
    )
  }
}

export default withTranslation('common')(withRouter(ListCandidates));
