import React from 'react';
import PDFCandidate from './PDFCandidate';
import { PDFViewer } from '@react-pdf/renderer';

class PDFColumn extends React.PureComponent {

  render() {
    return(
      <PDFViewer style={{ width: '100%', height: '100%'}}>
        <PDFCandidate
          dataBasicInfo={this.props.dataBasicInfo}
          dataLanguage={this.props.dataLanguage}
          dataLicense={this.props.dataLicense}
          dataSoftware={this.props.dataSoftware}
          dataSchools={this.props.dataSchools}
          dataWorks={this.props.dataWorks}
          dataWorkExperience={this.props.dataWorkExperience}
          dataComment={this.props.dataComment}
          dataAdvantageSkill={this.props.dataAdvantageSkill}
          note={this.props.note}
          />
      </PDFViewer>
    );
  }
}

export default PDFColumn;
