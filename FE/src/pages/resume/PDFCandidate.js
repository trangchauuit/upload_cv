import React from 'react';
import { Document, Page, Text } from '@react-pdf/renderer';
import styleDoc from './detail/styles/styleDoc';
import PDFBasicInfo from './detail/tabdocs/PDFBasicInfo';
import PDFLanguage from './detail/tabdocs/PDFLanguage';
import PDFLicense from './detail/tabdocs/PDFLicense';
import PDFSoftware from './detail/tabdocs/PDFSoftware';
import PDFSchools from './detail/tabdocs/PDFSchools';
import PDFWorks from './detail/tabdocs/PDFWorks';
import PDFWorkExperience from './detail/tabdocs/PDFWorkExperience';
import PDFComment from './detail/tabdocs/PDFComment';
import PDFAdvantageSkill from './detail/tabdocs/PDFAdvantageSkill';
import PDFNote from './detail/tabdocs/PDFNote';

class PDFCandidate extends React.PureComponent {

  render() {
    return(
      <Document>
        <Page style={styleDoc.body}>
          <PDFBasicInfo dataBasicInfo={this.props.dataBasicInfo}/>
          <PDFLanguage dataLanguage={this.props.dataLanguage}/>
          <PDFLicense dataLicense={this.props.dataLicense}/>
          <PDFSoftware dataSoftware={this.props.dataSoftware}/>
          <PDFSchools dataSchools={this.props.dataSchools}/>
          <PDFWorks dataWorks={this.props.dataWorks}/>
          <PDFWorkExperience dataWorkExperience={this.props.dataWorkExperience}/>
          <PDFComment dataComment={this.props.dataComment}/>
          <PDFAdvantageSkill dataAdvantageSkill={this.props.dataAdvantageSkill}/>
          <PDFNote note={this.props.note}/>
          <Text style={styleDoc.pageNumber} render={({ pageNumber, totalPages }) => (
          `${pageNumber} / ${totalPages}`
          )} fixed />
      </Page>
    </Document>
    );
  }
}
export default PDFCandidate;
