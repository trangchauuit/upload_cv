import React from "react";
import { Input } from "reactstrap";
import { withTranslation } from 'react-i18next';
import { API } from '../../utils/common';

class CompanyType extends React.PureComponent {
  _isMounted = false;
  constructor(props) {
    super(props);
    this.state = {
      introduceCompanyTypeId: this.props.introduceCompanyTypeId,
      arrcompanyTypes: [],
      buttonEdit: this.props.buttonEdit
    };
  }

  componentDidMount() {
    this._isMounted = true;
    this.getDataCompanyTypes();
  }

  componentWillReceiveProps(nextProps) {
    if(nextProps.buttonEdit !== this.state.buttonEdit){
      this.setState({ buttonEdit: nextProps.buttonEdit });
    }
  }

  componentWillUnmount() {
    this._isMounted = false;
  }

  handleChangeSelect = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    }, () => this.props.onDataCompanyTypes(this.state.introduceCompanyTypeId));
  }

  getDataCompanyTypes = async () => {
    let companyTypes = [];
    const lang = this.props.i18n.language;
    try {
      const response = await API.get(`/company_types?lang=${lang.split('-')[0]}`);
      companyTypes = response.data.data;
    } catch (error) {
      console.log("getDataCompanyTypes", error);
    }
    if (this._isMounted && companyTypes) {
      this.setState({arrcompanyTypes: companyTypes});
    }
  }

  render() {
    const { t } = this.props;
    const { introduceCompanyTypeId, arrcompanyTypes, buttonEdit } = this.state;
    return(
      <Input type="select" name="introduceCompanyTypeId" id="introduceCompanyTypeId"
        value={introduceCompanyTypeId || 0}
        onChange={this.handleChangeSelect}
        disabled={buttonEdit === undefined ? false: buttonEdit.isDisable}
      >
        <option value={0}>{t('label.pleaseChoose')}</option>
        {arrcompanyTypes.map(el => {
          return (<option key={el.id} value={el.id}>{el.name}</option>)
        })}
      </Input>
    )
  }
}
export default withTranslation('common')(CompanyType);
