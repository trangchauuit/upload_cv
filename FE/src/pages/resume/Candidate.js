import React, {Suspense} from "react";
import { API } from '../../utils/common';
import { withRouter } from 'react-router';
import TabBasicInfo from './detail/tabinfos/TabBasicInfo';
import TabLanguage from "./detail/tabinfos/TabLanguage";
import TabInterviewInformation from "./detail/tabinfos/TabInterviewInformation";
import TabLicense from "./detail/tabinfos/TabLicense";
import TabSoftware from "./detail/tabinfos/TabSoftware";
import TabSchools from "./detail/tabinfos/TabSchools";
import TabWorks from "./detail/tabinfos/TabWorks";
import TabWorkExperience from "./detail/tabinfos/TabWorkExperience";
import TabComment from "./detail/tabinfos/TabComment";
import TabAdvantageSkill from "./detail/tabinfos/TabAdvantageSkill";
import TabNote from "./detail/tabinfos/TabNote";
import { Button, Col, Form, Row, Nav, NavItem, NavLink, TabContent, TabPane, Label } from "reactstrap";
import classnames from 'classnames';
import { withTranslation } from 'react-i18next';
import TabCommonInfo from "./detail/tabinfos/TabCommonInfo";
import swal from 'sweetalert';
import { transformDefaultInterviewInfo, transformQuestion, transformInfoCadenly, transformQAResultInterview } from './../../utils/transformInterView';
import { transformCandidateArrSchool, transformCandidateArrWork, transformCandidateArrLicense, transformCandidateArrSoftware
  , transformCandidateBasicInfor }
from './../../utils/transformData';
//import PDFColumn from "./PDFColumn";

const PDFColumn = React.lazy(() => import('./PDFColumn')); // Lazy-loaded

class Candidate extends React.PureComponent {
  _isMounted = false;
  constructor(props) {
    super(props)
    this.state = {
      id: 0,
      urlFileCV: '',
      avatar: null,
      dataBasicInfo: {},
      dataInforCalendly: {},
      interviews: [],
      interviewResults: [],
      orgInterviews: [],
      dataQuestions: [],
      dataLanguage: {},
      dataLicense: [],
      dataSoftware: [],
      dataSchools: [],
      dataWorks: [],
      dataWorkExperience: {},
      dataComment: {},
      dataAdvantageSkill: {},
      dataNote: {},
      dataCommonInfo: {},
      isLoading: { loadInterview: true, loadOther: true, loadQuestion: true, loadInfoCalendly: true },
      activeTab: '1',
      buttonEdit: {
        isDisable: true,
        text: 'button.edit',
        isWaitRender: true
      },
      errCallEdit:'',
      dataTempBangCap: [],
      dataTempSoftware: [],
    }
    this.toggle = this.toggle.bind(this);
  }

  componentDidMount() {
    this._isMounted = true;
    this.getData();
    //this.prepareLoad();
  }
  componentWillUnmount() {
    this._isMounted = false;
  }

  getInforCalendly = async (round = 1) => {
    let { isLoading, dataBasicInfo, interviews } = this.state;
    if(!isLoading.loadOther) {
      await API.get(`/candidates/` + this.state.id + `/getTimeInterview/${round}`)
      .then(response => {
        this.setState({
          dataInforCalendly: {...transformInfoCadenly(response.data.data || {}, dataBasicInfo, interviews)}
        })
      }, error => {
          console.log('error', error)
        })
      this.setState((prevState, props) => ({
        isLoading: {
          ...prevState.isLoading,
          loadInfoCalendly: false
        },
      }));
    }
  }

  getQuestion = async () => {
    let { isLoading } = this.state;
    const lang = this.props.i18n.language;
    if(!isLoading.loadOther){
      let arr = [];
      await API.get(`/questions?lang=${lang.split('-')[0]}`)
      .then(response => {
        arr = transformQuestion(response.data.data);
      }, error => {
          console.log('error', error)
        })
      this.setState({
        dataQuestions: arr,
        isLoading: {...this.state.isLoading, loadQuestion: false}
      }, () => this.getDataInterview());
    }
  }
  getDataInterview = async () => {
    let dataQuestions = [...this.state.dataQuestions];
    await API.get(`/result-interviews/view/` + this.state.id)
    .then(response => {
      if (!response.data.data) {
        this.setState({
          interviewResults: [{
            dataInterviewInfo: transformDefaultInterviewInfo(dataQuestions).dataInterviewInfo,
            listQuestionsShow: transformDefaultInterviewInfo(dataQuestions).listQuestionsShow
          }]
        });
      } else {
        let results = []
        for (const interview of this.state.interviews) {
          let dataQA = [];
          let listQuestionsShows = [];
          const interviewResult = response.data.data.filter((item) => {
            return item.interview_id == interview.id
          })[0] || {answer_interview: []};

          if (interviewResult && interviewResult.answer_interview.length > 0){
            dataQA = transformQAResultInterview(interviewResult.answer_interview, dataQuestions).dataQA;
            listQuestionsShows = transformQAResultInterview(interviewResult.answer_interview, dataQuestions).listQuestionsShows;
          } else {
            dataQA = transformDefaultInterviewInfo(dataQuestions).dataInterviewInfo.dataQA;
            listQuestionsShows = transformDefaultInterviewInfo(dataQuestions).listQuestionsShow;
          }

          results.push({
            dataInterviewInfo: {
              ...interview,
              interviewer: { value: interviewResult.interviewer, isValid: true, message: '' },
              evaluation: { value: interviewResult.evaluate, isValid: true, message: '' },
              dataQA: dataQA
            },
            listQuestionsShow: listQuestionsShows
          })
        }
        this.setState({
          interviewResults: results
        });
      }
    }, error => {
        this.setState({
          interviewResults: [{
            dataInterviewInfo: transformDefaultInterviewInfo(dataQuestions).dataInterviewInfo,
            listQuestionsShow: transformDefaultInterviewInfo(dataQuestions).listQuestionsShow
          }]
        });
        console.log('error', error)
      })
    this.setState((prevState, props) => ({
      isLoading: {
        ...prevState.isLoading,
        loadInterview: false
      },
    }));
  }
  getData = async () => {
    try {
      const response = await API.get(`/admin/candidates/${this.props.match.params.id}`);
      const candidate = await response.data.data;
      if (this._isMounted && candidate) {
        let arrSchool = transformCandidateArrSchool(candidate.schools);
        let arrWork = transformCandidateArrWork(candidate.works);
        let arrLicense = transformCandidateArrLicense(candidate.languages, `${this.props.match.params.id}`);
        let arrSoftware = transformCandidateArrSoftware(candidate.software, `${this.props.match.params.id}`);
        const avatar = candidate.picture_path ? `${process.env.REACT_APP_IMAGE_HOST}${candidate.picture_path}` : null;
        this.setState({
          id: candidate.id,
          urlFileCV: candidate.cv_filename,
          avatar: candidate.avatar,
          picturePath: avatar,
          dataCommonInfo: {
            ...this.state.dataCommonInfo,
            'status': candidate.status,
            'introduceCompanyTypeId': candidate.introduce_company_type_id,
            'applicationDate': candidate.created_at,
            'introduceCompanyName': candidate.company_referral,
            'remark': candidate.remark
          },
          interviews: candidate.interviews,
          orgInterviews: candidate.interviews,
          dataBasicInfo: {
            ...this.state.dataBasicInfo,
            'nameKana': { value: candidate.name_kana === null? "": candidate.name_kana, isValid: true, message: '' },
            'name': { value: candidate.name === null? "": candidate.name, isValid: true, message: '' },
            'country': { value: candidate.country === null? "": candidate.country, isValid: true, message: '' },
            'phone': { value: candidate.phone === null? "": candidate.phone, isValid: true, message: '' },
            'birthday': { value: candidate.birthday === null? "": candidate.birthday, isValid: true, message: '' },
            'age': { value: candidate.age === null? "": candidate.age, isValid: true, message: '' },
            'gender': { value: candidate.gender === null? "": candidate.gender, isValid: true, message: '' },
            'marriedStatus': { value: candidate.married_status === null? "": candidate.married_status, isValid: true, message: '' },
            'zipCode': { value: candidate.zip_code === null? "": candidate.zip_code, isValid: true, message: '' },
            'nearTrainStation': { value: candidate.near_train_station === null? "": candidate.near_train_station, isValid: true, message: '' },
            'email': { value: candidate.email === null? "": candidate.email, isValid: true, message: '' },
            'japanVisa': { value: candidate.japan_visa === null? "": candidate.japan_visa, isValid: true, message: '' },
            'japanVisaDateExpired': { value: candidate.japan_visa_date_expired === null? "": candidate.japan_visa_date_expired, isValid: true, message: '' },
            'japanVisaType': { value: candidate.japan_visa_type === null? "": candidate.japan_visa_type, isValid: true, message: '' },
            'currentAddress': { value: candidate.address === null? "": candidate.address, isValid: true, message: '' },
            'contactAddress': { value: candidate.address_contact === null? "": candidate.address_contact, isValid: true, message: '' },
            'zipCodeContact': { value: candidate.zip_code_contact === null? "": candidate.zip_code_contact, isValid: true, message: '' },
            'dobYear': { value: candidate.year_birthday === null? "": candidate.year_birthday, isValid: true, message: '' },
            'dobMonth': { value: candidate.month_birthday === null? "": candidate.month_birthday, isValid: true, message: '' },
            'dobDay': { value: candidate.day_birthday === null? "": candidate.day_birthday, isValid: true, message: '' },

          },
          dataLanguage: {
            ...this.state.dataLanguage,
            'japanLanguageLevel': candidate.japan_language_level,
            'englishLanguageLevel': candidate.english_language_level,
            'koreanLanguageLevel': candidate.korean_language_level
          },
          dataLicense: arrLicense,
          dataSoftware: arrSoftware,
          dataSchools: arrSchool,
          dataWorks: arrWork,
          dataNote: candidate.note || '',
          dataWorkExperience: {
            ...this.state.dataWorkExperience,
            'selfPr': candidate.self_pr
          },
          dataComment: {
            ...this.state.dataComment,
            'applicantComment': candidate.applicant_comment
          },
          dataAdvantageSkill: {
            ...this.state.dataAdvantageSkill,
            'applicantAdvantageSkillExplanation': candidate.applicant_advantage_skill_explanation
          },
          isLoading: {...this.state.isLoading, loadOther: false}
        }, ()=> {
            this.getQuestion();
            this.getInforCalendly();
          });
      }
    } catch (error) {
      console.log("errorGetData", error);
    }
  }
  prepareLoad() {
    setTimeout(
      () => this.setState({buttonEdit: {...this.state.buttonEdit, isWaitRender: false}}),
      5000
    );
  }

  async toggle(tab) {
    this.setState({ activeTab: tab });
    
    if (this.state.activeTab !== tab) {
      switch (tab) {
        case '2':
          await this.getInforCalendly(1);
          break;

        case '13':
          await this.getInforCalendly(2);
          break;
        default:
        break;
      }
    }
  }
  handleTabBasicInfo = (dataBasicInfo) => {
    this.setState({ dataBasicInfo: dataBasicInfo });
  }
  handleTabInterviewInfo = (dataInterviewInfo) => {
    const interviewResults = this.state.interviewResults;
    const index = interviewResults.findIndex(i => i.dataInterviewInfo.id === dataInterviewInfo.id);
    interviewResults[index].dataInterviewInfo = dataInterviewInfo;
    this.setState({ interviewResults: [...interviewResults] });
  }
  handleTabLanguage = (dataLanguage) => {
    this.setState({ dataLanguage: dataLanguage });
  }
  handleTabLicense = (dataLicense) => {
    this.setState({ dataLicense: dataLicense });
  }
  handleTabSoftware = (dataSoftware) => {
    this.setState({ dataSoftware: dataSoftware });
  }
  handleTabSchools = (dataSchools) => {
    this.setState({ dataSchools: dataSchools });
  }
  handleTabWorks = (dataWorks) => {
    this.setState({ dataWorks: dataWorks });
  }
  handleTabWorkExperience = (dataWorkExperience) => {
    this.setState({ dataWorkExperience: dataWorkExperience });
  }
  handleTabComment = (dataComment) => {
    this.setState({ dataComment: dataComment });
  }
  handleTabAdvantageSkill = (dataAdvantageSkill) => {
    this.setState({ dataAdvantageSkill: dataAdvantageSkill });
  }
  handleTabNote = (value) => {
    this.setState({ dataNote: value });
  }
  handleTabCommonInfo = (dataCommonInfo) => {
    this.setState({ dataCommonInfo: dataCommonInfo });
  }
  setBasicInfoValid = (dataBasicInfo, field, isValid, message) => {
    this.setState((prevState, props) => ({
      [dataBasicInfo]: {
        ...prevState[dataBasicInfo],
        [field]: {
          ...prevState[dataBasicInfo][field],
          isValid: isValid,
          message: message
        }
      },
    }));
  }
  setArrayIsValid = (data, id, field, isValid, message) => {
    this.setState((prevState, props) => ({
      [data]: prevState[data].map(el => (el.id.value === id ? {
        ...el,
        [field]: {
          ...el[field],
          isValid: isValid,
          message: message
        }
      } : el))
    }));
  }
  formBasicInfoIsValid = () => {
    let isOK = true;

    var pattern = new RegExp(/^(("[\w-\s]+")|([\w-]+(?:\.[\w-]+)*)|("[\w-\s]+")([\w-]+(?:\.[\w-]+)*))(@((?:[\w-]+\.)*\w[\w-]{0,66})\.([a-z]{2,6}(?:\.[a-z]{2})?)$)|(@\[?((25[0-5]\.|2[0-4][0-9]\.|1[0-9]{2}\.|[0-9]{1,2}\.))((25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\.){2}(25[0-5]|2[0-4][0-9]|1[0-9]{2}|[0-9]{1,2})\]?$)/i);
    if (!this.state.dataBasicInfo.email.value || this.state.dataBasicInfo.email.value.trim() === "") {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'email', false, `${this.props.t('errorMessage.notEmpty')}`);
    } else if (!pattern.test(this.state.dataBasicInfo.email.value)) {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'email', false, `${this.props.t('errorMessage.emailNotValid')}`);
    } else {
      this.setBasicInfoValid('dataBasicInfo', 'email', true, '');
    }

    if (!this.state.dataBasicInfo.name.value || this.state.dataBasicInfo.name.value.trim() === "") {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'name', false, `${this.props.t('errorMessage.notEmpty')}`);
    } else if (this.state.dataBasicInfo.name.value.length > 100) {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'name', false, `${this.props.t('errorMessage.max100')}`);
    } else if (this.state.dataBasicInfo.name.value.length < 3) {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'name', false, `${this.props.t('errorMessage.min3')}`);
    } else {
      this.setBasicInfoValid('dataBasicInfo', 'name', true, '');
    }

    if (!this.state.dataBasicInfo.nameKana.value || this.state.dataBasicInfo.nameKana.value.trim() === "") {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'nameKana', false, `${this.props.t('errorMessage.notEmpty')}`);
    } else {
      this.setBasicInfoValid('dataBasicInfo', 'nameKana', true, '');
    }

    if (!this.state.dataBasicInfo.country.value || this.state.dataBasicInfo.country.value.trim() === "") {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'country', false, `${this.props.t('errorMessage.notEmpty')}`);
    } else {
      this.setBasicInfoValid('dataBasicInfo', 'country', true, '');
    }

    if (!this.state.dataBasicInfo.age.value) {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'age', false, `${this.props.t('errorMessage.notEmpty')}`);
    } else if (this.state.dataBasicInfo.age.value > 1264) {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'age', false, `${this.props.t('errorMessage.max1264')}`);
    } else {
      this.setBasicInfoValid('dataBasicInfo', 'age', true, '');
    }

    /*if (!this.state.dataBasicInfo.zipCode.value || this.state.dataBasicInfo.zipCode.value.trim() === "") {
isOK = false;
this.setBasicInfoValid('dataBasicInfo', 'zipCode', false, `${this.props.t('errorMessage.notEmpty')}`);
} else */

    if (this.state.dataBasicInfo.zipCode.value !== undefined && this.state.dataBasicInfo.zipCode.value !== null
      && this.state.dataBasicInfo.zipCode.value !== ""
      && this.state.dataBasicInfo.zipCode.value.length !== 7) {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'zipCode', false, `${this.props.t('errorMessage.length7')}`);
    } else {
      this.setBasicInfoValid('dataBasicInfo', 'zipCode', true, '');
    }

    if (!this.state.dataBasicInfo.currentAddress.value ||
      ((this.state.dataBasicInfo.currentAddress.value != null ) &&
        (this.state.dataBasicInfo.currentAddress.value.trim() === ""))) {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'currentAddress', false, `${this.props.t('errorMessage.notEmpty')}`);
    } else {
      this.setBasicInfoValid('dataBasicInfo', 'currentAddress', true, '');
    }

    if (!this.state.dataBasicInfo.dobYear.value ||
      ((this.state.dataBasicInfo.dobYear.value != null ) &&
        (this.state.dataBasicInfo.dobYear.value.toString().trim() === ""))) {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'dobYear', false, `${this.props.t('errorMessage.notEmpty')}`);
    } else {
      this.setBasicInfoValid('dataBasicInfo', 'dobYear', true, '');
    }

    if (!this.state.dataBasicInfo.dobMonth.value ||
      ((this.state.dataBasicInfo.dobMonth.value != null ) &&
        (this.state.dataBasicInfo.dobMonth.value.toString().trim() === ""))) {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'dobMonth', false, `${this.props.t('errorMessage.notEmpty')}`);
    } else {
      this.setBasicInfoValid('dataBasicInfo', 'dobMonth', true, '');
    }

    if (!this.state.dataBasicInfo.dobDay.value ||
      ((this.state.dataBasicInfo.dobDay.value != null ) &&
        (this.state.dataBasicInfo.dobDay.value.toString().trim() === ""))) {
      isOK = false;
      this.setBasicInfoValid('dataBasicInfo', 'dobDay', false, `${this.props.t('errorMessage.notEmpty')}`);
    } else {
      this.setBasicInfoValid('dataBasicInfo', 'dobDay', true, '');
    }

    if (isOK) {
      const { dobYear, dobMonth, dobDay } = this.state.dataBasicInfo;
      if (!this.isValidDate(+dobYear.value, +dobMonth.value, +dobDay.value)) {
        isOK = false;
        this.setBasicInfoValid('dataBasicInfo', 'dobDay', false, `${this.props.t('errorMessage.dayInvalid')}`);
      }
    }

    return isOK;
  }

  daysInMonth = function (m, y) {
    switch (m) {
      case 1 :
        return (y % 4 == 0 && y % 100) || y % 400 == 0 ? 29 : 28;
      case 8 : case 3 : case 5 : case 10 :
        return 30;
      default :
      return 31
    }
  };


  isValidDate = function (y,m,d) {
    m = parseInt(m, 10) - 1;
    return m >= 0 && m < 12 && d > 0 && d <= this.daysInMonth(m, y);
  };

  formWorksIsValid = () => {
    let isOK = true;
    this.state.dataWorks.forEach(el => {
      /*if (!el.fromMonth.value || !el.fromYear.value) {
isOK = false;
this.setArrayIsValid('dataWorks', el.id.value, 'fromMonth', false, `${this.props.t('errorMessage.notEmpty')}`);
} else {
this.setArrayIsValid('dataWorks', el.id.value, 'fromMonth', true, '');
}

if (!el.toMonth.value || !el.toYear.value) {
isOK = false;
this.setArrayIsValid('dataWorks', el.id.value, 'toMonth', false, `${this.props.t('errorMessage.notEmpty')}`);
} else {
this.setArrayIsValid('dataWorks', el.id.value, 'toMonth', true, '');
}

if (!el.company.value || el.company.value.trim() === "") {
isOK = false;
this.setArrayIsValid('dataWorks', el.id.value, 'company', false, `${this.props.t('errorMessage.notEmpty')}`);
} else {
this.setArrayIsValid('dataWorks', el.id.value, 'company', true, '');
}

if (!el.workContent.value || el.workContent.value.trim() === "") {
isOK = false;
this.setArrayIsValid('dataWorks', el.id.value, 'workContent', false, `${this.props.t('errorMessage.notEmpty')}`);
} else {
this.setArrayIsValid('dataWorks', el.id.value, 'workContent', true, '');
}

if (!el.workSkill.value || el.workSkill.value.trim() === "") {
isOK = false;
this.setArrayIsValid('dataWorks', el.id.value, 'workSkill', false, `${this.props.t('errorMessage.notEmpty')}`);
} else {
this.setArrayIsValid('dataWorks', el.id.value, 'workSkill', true, '');
}*/
    });
    return isOK;
  }

  formSchoolsIsValid = () => {
    let isOK = true;
    this.state.dataSchools.forEach(el => {
      if (!el.fromMonth.value || !el.fromYear.value) {
        isOK = false;
        this.setArrayIsValid('dataSchools', el.id.value, 'fromMonth', false, `${this.props.t('errorMessage.notEmpty')}`);
      } else {
        this.setArrayIsValid('dataSchools', el.id.value, 'fromMonth', true, '');
      }

      if (!el.toMonth.value || !el.toYear.value) {
        isOK = false;
        this.setArrayIsValid('dataSchools', el.id.value, 'toMonth', false, `${this.props.t('errorMessage.notEmpty')}`);
      } else {
        this.setArrayIsValid('dataSchools', el.id.value, 'toMonth', true, '');
      }

      if (!el.schoolName.value || el.schoolName.value.trim() === "") {
        isOK = false;
        this.setArrayIsValid('dataSchools', el.id.value, 'schoolName', false, `${this.props.t('errorMessage.notEmpty')}`);
      } else {
        this.setArrayIsValid('dataSchools', el.id.value, 'schoolName', true, '');
      }
    });
    return isOK;
  }

  formSoftwareIsValid = () => {
    let isOK = true;
    // this.state.dataSoftware.forEach(el => {
    //   // row fake -> nameSoftware rong -> year rong -> ok
    //   // row co id -> moi thu deu bat buoc
    //   if (typeof el.id.value === "string"){
    //     this.setArrayIsValid('dataSoftware', el.id.value, 'year', true, '');
    //     if (el.software.value != null && el.software.value.length > 0) {
    //
    //       if (!el.year.value) {
    //         isOK = false;
    //         this.setArrayIsValid('dataSoftware', el.id.value, 'year', false, `${this.props.t('errorMessage.notEmpty')}`);
    //       }
    //     }
    //     this.setArrayIsValid('dataSoftware', el.id.value, 'software', true, '');
    //     if (el.year.value) {
    //       if (el.software.value != null && el.software.value.length === 0) {
    //         isOK = false;
    //         this.setArrayIsValid('dataSoftware', el.id.value, 'software', false, `${this.props.t('errorMessage.notEmpty')}`);
    //       }
    //     }
    //   }else{
    //     if (el.year.value != null && el.year.value.length === 0) {
    //       // isOK = false;
    //       // this.setArrayIsValid('dataSoftware', el.id.value, 'year', false, `${this.props.t('errorMessage.notEmpty')}`);
    //     } else {
    //       // this.setArrayIsValid('dataSoftware', el.id.value, 'year', true, '');
    //     }
    //     if (!el.software.value) {
    //       isOK = false;
    //       this.setArrayIsValid('dataSoftware', el.id.value, 'software', false, `${this.props.t('errorMessage.notEmpty')}`);
    //     } else {
    //       this.setArrayIsValid('dataSoftware', el.id.value, 'software', true, '');
    //     }
    //   }
    // });
    return isOK;
  }

  setValidInterviewArr = (data, arr, field, isValid, message, indexSelected) => {
    const interviewResults = this.state.interviewResults;
    const index = interviewResults.findIndex(i => i.dataInterviewInfo.id === data.id);
    const dataInterviewInfo = interviewResults[index].dataInterviewInfo;
    dataInterviewInfo[arr] = dataInterviewInfo[arr].map((el, index) => (index === indexSelected ? {
      ...el, [field]: { ...el[field], isValid: isValid, message: message }
    } : el))
    interviewResults[index].dataInterviewInfo = dataInterviewInfo;
    this.setState({ interviewResults: interviewResults })
  }

  validInterviewInfo = (interviewId) => {
    const { t } = this.props;
    const dataInterviewInfo = this.state.interviewResults.filter(i => i.dataInterviewInfo.id === interviewId)[0].dataInterviewInfo;
    let isOK = true;
    dataInterviewInfo.dataQA.forEach((el, index) => {
      if ((el.question.value + "") === "-1") {
        if (((el.answer.value != null ) && (el.answer.value.trim() !== ""))
          ||((el.answer_id.value !== null) && ((el.answer_id.value + "" ) !== '-1'))){
          isOK = false;
          this.setValidInterviewArr(dataInterviewInfo, 'dataQA', 'question', false, `${t('errorMessage.notEmpty')}`, index);
        }else{
          this.setValidInterviewArr(dataInterviewInfo, 'dataQA', 'question', true, '', index);
        }
      } else {
        //console.log('el.question_kind', el.question_kind)
        //console.log('el.answer_id.value', el.answer_id.value)
        if (((el.question_kind + "") === "1") && ((el.answer.value == null) || el.answer.value.trim() === "")) {
          isOK = false;
          this.setValidInterviewArr(dataInterviewInfo, 'dataQA', 'comment', false, `${t('errorMessage.notEmpty')}`, index);
        }else{
          this.setValidInterviewArr(dataInterviewInfo, 'dataQA', 'comment', true, '', index);
        }
        if (((el.question_kind + "") === "2") && ((el.answer_id.value == null) || ((el.answer_id.value + "" ) === '-1'))) {
          isOK = false;
          this.setValidInterviewArr(dataInterviewInfo, 'dataQA', 'answer', false, `${t('errorMessage.notEmpty')}`, index);
        }else{
          this.setValidInterviewArr(dataInterviewInfo, 'dataQA', 'answer', true, '', index);
        }
      }
    })
    return isOK;
  }
  callEdit = async (url, infos) => {
    let isSucess = false;
    const { id } = this.state;
    await API.put(url, infos)
    .then((response) => {
      if (response.data.status === true) {
        isSucess = true;
        let activeTab = '' + this.state.activeTab;
        switch (activeTab) {
          case '5':
            let tempLicense = transformCandidateArrLicense(response.data.data.languages, id);
            //let elLicense = [...this.state.dataLicense];
            //elLicense.dataTemp = tempLicense;
            this.setState({dataTempBangCap: tempLicense});
            break;
          case '6':
            let tempSoftware = transformCandidateArrSoftware(response.data.data.software, id);
            //let elSoftware  = [...this.state.dataSoftware];
            //elSoftware.dataTemp = tempSoftware;
            this.setState({dataTempSoftware: tempSoftware});
            break;
          default:
          break;
        }
      }
    })
    .catch(error => {
      console.log('callEdit', error);
      console.log('callEdit', error.response ? error.response.data.message : error.message);
      this.setState({errCallEdit:  error.response ? error.response.data.message : error.message});
    });
    return isSucess;
  }

  editInterviewInfo = async(interviewId) => {
    const dataInterviewInfo = this.state.interviewResults.filter(i => i.dataInterviewInfo.id === interviewId)[0].dataInterviewInfo;
    let result = { isValid: false };
    const { id } = this.state;
    let answers = [];
    let objRe = {};
    dataInterviewInfo.dataQA.forEach((el) => {
      if ((el.question_kind + "") === '1'){
        objRe = {
          question_id: el.question.value,
          answer_content: el.comment.value
        }
      }else{
        objRe = {
          question_id: el.question.value,
          answer_id: el.answer_id.value,
          comment: el.comment.value
        }
      }
      answers.push(objRe);
    });

    let params = {
      method: +dataInterviewInfo.method,
      round_interview: +dataInterviewInfo.round,
      result: +dataInterviewInfo.result,
      evaluate: dataInterviewInfo.evaluation.value,
      interviewer: dataInterviewInfo.interviewer.value,
      answers: answers,
    }
    if (this.validInterviewInfo(interviewId)) {
      result.isValid = true;
      let isSucess = false;
      await API.post('/result-interviews/save/' + id, params)
      .then(async (response) => {
        if (response.data.status === true) {
          await this.getData();
          isSucess = true;
        }
      })
      .catch(error => {
        console.log('callEdit', error);
        console.log('callEdit', error.response ? error.response.data.message : error.message);
      });
      result.status = isSucess;
      return result;
    }
    return result;
  }

  editBasicInfo = async () => {
    let result = { isValid: false };
    const { id, dataBasicInfo, dataCommonInfo, dataComment, dataLanguage, dataAdvantageSkill, dataWorkExperience } = this.state;

    let infos = transformCandidateBasicInfor(dataCommonInfo, dataBasicInfo, dataLanguage, dataWorkExperience, dataComment, dataAdvantageSkill);
    if (this.formBasicInfoIsValid()) {
      result.isValid = true;
      infos.note = this.state.dataNote;
      result.status = await this.callEdit(`/admin/candidates/${id}`, infos);
      //console.log('result', result);
      return result;
    }
    return result;
  }

  editLicense = async () => {
    let result = { isValid: false };
    //if (this.formLicenseIsValid()) {
    const ownerPromises = this.state.dataLicense.map(async el => {
      //if( el.level.value || (el.year.value && el.month.value )){
      let infos = {
        level: el.level.value,
        year: el.year.value,
        month: el.month.value,
        name: el.name.value
      }
      let owner;
      if (typeof el.id.value === "string" || el.id.value < 0){
        owner = await this.callEdit(`/admin/candidates/${this.state.id}/language`, infos);
      }else{
        owner = await this.callEdit(`/admin/candidates/${this.state.id}/language/${el.id.value}`, infos);
      }
      return owner;
      //}
    });
    let owners = await Promise.all(ownerPromises);
    result.isValid = true;
    result.status = (!owners.includes(false));

    if (Array.isArray(this.state.dataTempBangCap)){
      let elementDataLicense = [ ...this.state.dataTempBangCap ];
      this.setState({
        dataLicense: elementDataLicense,
        dataTempBangCap: []
      });
    }
    //console.log('result', result);
    return result;
    //}
  }

  editSchools = async () => {
    let result = { isValid: false };
    if (this.formSchoolsIsValid()) {
      const ownerPromises = this.state.dataSchools.map(async el => {
        let infos = {
          from_month: el.fromMonth.value,
          from_year: el.fromYear.value,
          to_month: el.toMonth.value,
          to_year: el.toYear.value,
          school_name: el.schoolName.value,
          faculty: el.faculty.value,
          study_skills: el.studySkills.value
        }
        const owner = await this.callEdit(`/admin/candidates/${this.state.id}/school/${el.id.value}`, infos);
        return owner;
      });
      let owners = await Promise.all(ownerPromises);
      result.isValid = true;
      result.status = (!owners.includes(false));
      //console.log('result', result);
      return result;
    }
    return result;
  }

  editWork = async () => {
    let result = { isValid: false };
    if (this.formWorksIsValid()) {
      const ownerPromises = this.state.dataWorks.map(async el => {
        let infos = {
          from_month: el.fromMonth.value,
          from_year: el.fromYear.value,
          to_month: el.toMonth.value,
          to_year: el.toYear.value,
          company: el.company.value,
          contract_type: el.contractType.value,
          work_place_type: el.workPlaceType.value,
          work_content: el.workContent.value,
          work_skill: el.workSkill.value
        }
        const owner = await this.callEdit(`/admin/candidates/${this.state.id}/work/${el.id.value}`, infos);
        return owner;
      });
      let owners = await Promise.all(ownerPromises);
      result.isValid = true;
      result.status = (!owners.includes(false));
      //console.log('result', result);
      return result;
    }
    return result;
  }

  editSoftware = async () => {
    let result = { isValid: false };
    if (this.formSoftwareIsValid()) {
      const ownerPromises = this.state.dataSoftware.map(async el => {
        if( el.year.value || el.software.value){
          let infos = {
            year: el.year.value,
            software: el.software.value
          }
          let owner;
          if (typeof el.id.value === "string"){
            owner = await this.callEdit(`/admin/candidates/${this.state.id}/software`, infos);
          }else{
            owner = await this.callEdit(`/admin/candidates/${this.state.id}/software/${el.id.value}`, infos);
          }
          return owner;
        }
      });
      let owners = await Promise.all(ownerPromises);
      result.isValid = true;
      result.status = (!owners.includes(false));

      if (Array.isArray(this.state.dataTempSoftware)){
        let elements = [ ...this.state.dataTempSoftware];
        this.setState({
          dataSoftware: elements,
          dataTempSoftware: []
        });
      }
      return result;
    }
    return result;
  }

  downloadCV = async () => {
    window.location.href = `${process.env.REACT_APP_API}` + this.state.urlFileCV;
  }

  downloadAvatar = async () => {
    let imagePath = window.location.origin + '/User-Profile-Transparent.png';
    if (this.state.picturePath) {
      imagePath = this.state.picturePath;
    }
    var link = document.createElement("a");
    link.href = imagePath;
    link.download = imagePath.split('/').pop();
    document.body.appendChild(link);
    link.click();
    document.body.removeChild(link);
  }

  cancelCandidate = () => {
    this.setState(prevState => ({
      buttonEdit: {
        ...prevState.buttonEdit,
        text: ('button.edit'),
        isDisable: !(prevState.buttonEdit.isDisable)
      }
    }));
    this.getData();
  }

  deleteCandidate = async () => {
    const { t } = this.props;
    if (!window.confirm(t('message.confirmDeleteCandidate'))) return;

    await API.delete(`/admin/candidates/${this.state.id}`)
      .then(response => {
        swal({
          title: t('message.delete_success'),
          icon: "success",
        });
        this.props.history.push({
          pathname: '/',
          state: { activeTab: 2 }
        })
      }, error => {
        swal({
          title:  t('message.delete_fail'),
          icon: "error",
        });
      })
  }

  editCandidate = async (editData) => {
    let result;
    const { t } = this.props;
    this.setState(prevState => ({
      buttonEdit: {
        ...prevState.buttonEdit,
        text: (prevState.buttonEdit.isDisable === true ? 'button.save' : 'button.edit'),
        isDisable: !(prevState.buttonEdit.isDisable)
      }
    }));

    if (this.state.buttonEdit.isDisable === false) {
      let activeTab = '' + this.state.activeTab;
      console.log('activeTab', activeTab)
      switch (activeTab) {
        case '1':
        case '3':
        case '4':
        case '9':
        case '10':
        case '11':
        case '14':
          result = await this.editBasicInfo()
          break;
        case '5':
          result = await this.editLicense();
          break;
        case '6':
          result = await this.editSoftware();
          break;
        case '7':
          result = await this.editSchools();
          break;
        case '8':
          result = await this.editWork();
          break;
        case '2':
        case '13':
          result = await this.editInterviewInfo(editData.interviewId)
          break;
        default:
        result = await this.editBasicInfo();
        break;
      }
      if (result.isValid) {
        if (result.status) {
          swal({
            title: t('message.update_success'),
            icon: "success",
          });
        } else {
          //swal({
          //    title:  (this.state.errCallEdit.length > 0 ? this.state.errCallEdit:t('message.update_fail')),
          //    icon: "error",
          //});
          swal({
            title:  t('message.update_fail'),
            icon: "error",
          });
        }
      } else {
        this.setState(prevState => ({
          buttonEdit: {
            ...prevState.buttonEdit,
            text: ('button.save'),
            isDisable: !(prevState.buttonEdit.isDisable)
          }
        }));
      }
    }
  }
  handleChangeButton = (button) => {
    if (button.type === 'save') {
      this.editCandidate({ interviewId: button.interviewId });
    }
    this.setState({ buttonEdit: button });
  }
  handleBack = () => {
    this.props.history.push({
      pathname: '/',
      state: { activeTab: 2 }
    })
  }
  handleDownloadProfileDoc = (e) => {
    e.preventDefault();
    window.location.href = `${process.env.REACT_APP_API}/api/result-interviews/download/${this.state.id}`;
  }
  render() {
    const { t } = this.props;
    const { dataInforCalendly, dataQuestions, activeTab, isLoading, buttonEdit, dataCommonInfo, dataBasicInfo, dataLanguage, dataLicense, dataSoftware, dataSchools,
      dataWorks, dataWorkExperience, dataComment, dataAdvantageSkill } = this.state;
    let contentTab;
    if ((!isLoading.loadOther && !isLoading.loadQuestion && !isLoading.loadInterview && !isLoading.loadInfoCalendly)
      && this._isMounted) {
      contentTab =
        <Row>
          <Col md={['1','2','13'].includes(activeTab) ? 12 : 6} className="first-column pt-5">
            <Form>
              <TabContent activeTab={activeTab}>
                <Row className="float-right" style={{ display: ['2','13'].includes(activeTab) ? 'none' : 'block' }}>
                  {/*{buttonEdit.isWaitRender&&<Button outline color="primary" className="mr-3" onClick={this.editCandidate} disabled>{t(buttonEdit.text)}</Button>*/}
                  {/*!buttonEdit.isWaitRender&&<Button outline color="primary" className="mr-3" onClick={this.editCandidate}>{t(buttonEdit.text)}</Button>}*/}
                  { ['1', '14'].includes(activeTab) && <React.Fragment>
                    <Button outline color="primary" className="mr-3" onClick={this.editCandidate}>{t(buttonEdit.text)}</Button>
                    <Button outline color="warning" className="ml-2 mr-3" onClick={this.cancelCandidate} hidden={buttonEdit.isDisable}>{t('button.cancel')}</Button>
                  </React.Fragment> }
                  { !['1', '2', '13'].includes(activeTab) && <React.Fragment>
                    <Button outline color="warning" className="mr-3" onClick={this.deleteCandidate}>{t('button.delete')}</Button>
                  </React.Fragment> }
                </Row>
                <TabPane tabId="1">
                  <TabCommonInfo
                    buttonEdit={buttonEdit}
                    dataCommonInfo={dataCommonInfo}
                    onDataCommonInfo={this.handleTabCommonInfo} />
                </TabPane>
                {/* Xu ly handleChangeButton de giong vs nghiep vu cua cac tab con lai*/}
                {this.state.interviews.map((interview, index) => {
                  if (this.state.interviewResults.filter(i=>i.dataInterviewInfo.id === interview.id).length === 0) return null;
                  return <TabPane tabId={index == 0 ? '2' : '13'} key={interview.id}>
                    <TabInterviewInformation
                      buttonEdit={buttonEdit}
                      interviewId={interview.id}
                      interviews={this.state.interviews}
                      getData={async () => {
                          this._isMounted = true;
                          await this.getData();
                          await this.getDataInterview();
                        }
                      }
                      candidateId={this.state.id}
                      dataInterviewInfo={this.state.interviewResults[index].dataInterviewInfo}
                      orgInterviews={this.state.orgInterviews}
                      dataQuestions={dataQuestions}
                      dataInforCalendly={dataInforCalendly}
                      listQuestionsShow={this.state.interviewResults[index].listQuestionsShow}
                      onDataInterviewInfo={this.handleTabInterviewInfo}
                      onDataChangeButton={this.handleChangeButton}
                      />
                  </TabPane>
                })}
                <TabPane tabId="3">
                  <TabBasicInfo
                    buttonEdit={buttonEdit}
                    dataBasicInfo={dataBasicInfo}
                    onDataBasicInfo={this.handleTabBasicInfo} />
                </TabPane>
                <TabPane tabId="4">
                  <TabLanguage
                    buttonEdit={buttonEdit}
                    dataLanguage={dataLanguage}
                    onDataLanguage={this.handleTabLanguage} />
                </TabPane>
                <TabPane tabId="5">
                  <TabLicense
                    buttonEdit={buttonEdit}
                    dataLicense={dataLicense}
                    onDataLicense={this.handleTabLicense} />
                </TabPane>
                <TabPane tabId="6">
                  <TabSoftware
                    buttonEdit={buttonEdit}
                    dataSoftware={dataSoftware}
                    onDataSoftware={this.handleTabSoftware} />
                </TabPane>
                <TabPane tabId="7">
                  <TabSchools
                    buttonEdit={buttonEdit}
                    dataSchools={dataSchools}
                    onDataSchools={this.handleTabSchools} />
                </TabPane>
                <TabPane tabId="8" >
                  <TabWorks
                    buttonEdit={buttonEdit}
                    dataWorks={dataWorks}
                    onDataWorks={this.handleTabWorks} />
                </TabPane>
                <TabPane tabId="9">
                  <TabWorkExperience
                    buttonEdit={buttonEdit}
                    dataWorkExperience={dataWorkExperience}
                    onDataWorkExperience={this.handleTabWorkExperience} />
                </TabPane>
                <TabPane tabId="10">
                  <TabComment
                    buttonEdit={buttonEdit}
                    dataComment={dataComment}
                    onDataComment={this.handleTabComment} />
                </TabPane>
                <TabPane tabId="11">
                  <TabAdvantageSkill
                    buttonEdit={buttonEdit}
                    dataAdvantageSkill={dataAdvantageSkill}
                    onDataAdvantageSkill={this.handleTabAdvantageSkill} />
                </TabPane>
                <TabPane tabId="14">
                  <TabNote
                    buttonEdit={buttonEdit}
                    note={this.state.dataNote}
                    onDataNote={this.handleTabNote} />
                </TabPane>
              </TabContent>
            </Form>
          </Col>
          <Col md={['1','2','13'].includes(activeTab) ? 0 : 6} className="second-column pt-5"
            style={{ minHeight: 800, display: ['1','2','13'].includes(activeTab) ? "none" : "block" }}>
            <Row className="float-right">
              <Button outline color="primary" className="mr-3 mb-3" onClick={this.downloadAvatar}>{t('button.downloadAvatar')}</Button>
              <Button outline color="primary" className="mr-3 mb-3" onClick={this.downloadCV}>{t('button.downloadProfile')}</Button>
            </Row>
            <Suspense fallback={<h1>Loading...</h1>}>
              <PDFColumn
                dataBasicInfo={{ ...this.state.dataBasicInfo, avatar: this.state.picturePath }}
                dataLanguage={this.state.dataLanguage}
                dataLicense={this.state.dataLicense}
                dataSoftware={this.state.dataSoftware}
                dataSchools={this.state.dataSchools}
                dataWorks={this.state.dataWorks}
                dataWorkExperience={this.state.dataWorkExperience}
                dataComment={this.state.dataComment}
                dataAdvantageSkill={this.state.dataAdvantageSkill}
                note={this.state.dataNote}
                // dataInforCalendly={this.state.dataInforCalendly}
                />
            </Suspense>

          </Col>
        </Row>;
    } else {
      contentTab = <React.Fragment />;
    }
    if ((!isLoading.loadOther && !isLoading.loadQuestion && !isLoading.loadInterview && !isLoading.loadInfoCalendly) && this._isMounted ){
      return (
        <>
          <Row>
            <Button outline color="primary" className="ml-3 mb-3 mt-3" onClick={this.handleBack}>{t('button.return')}</Button>
          </Row>
          <Row className="mb-3 mt-3">
            <Col className="md-6">
              <Label className="font-weight-bold">
                {t('label.manageCandidateDetails')}
              </Label>
            </Col>
            <Col className="md-6" hidden>
              <Button color="primary" style={{ float: "right" }} onClick={(e)=> this.handleDownloadProfileDoc(e)}>{t('button.profileDownload')}</Button>
            </Col>
          </Row>
          <Nav tabs id="nav-tab">
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '1' })}
                onClick={async () => { this.toggle('1'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.referralInformation')}
              </NavLink>
            </NavItem>
            {this.state.interviews.map((item, i) => {
              // if (item.round === 1 && item.is_continue !== 1) return null;

              return <NavItem key={item.id}>
              <NavLink
                className={classnames({ active: this.state.activeTab === (i === 0 ? '2' : '13') })}
                onClick={async () => { this.toggle(i === 0 ? '2' : '13'); }}
              >
                {t(`label.interviewInformation${i+1}`)}
              </NavLink>
            </NavItem>
            })}
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '3' })}
                onClick={async () => { this.toggle('3'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.basicInformation')}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '4' })}
                onClick={async () => { this.toggle('4'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.language')}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '5' })}
                onClick={async () => { this.toggle('5'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.license')}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '6' })}
                onClick={async () => { this.toggle('6'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.software')}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '7' })}
                onClick={async () => { this.toggle('7'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.education')}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '8' })}
                onClick={async () => { this.toggle('8'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.work')}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '9' })}
                onClick={async () => { this.toggle('9'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.selfPromotion')}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '10' })}
                onClick={async () => { this.toggle('10'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.hope')}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '11' })}
                onClick={async () => { this.toggle('11'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.skills')}
              </NavLink>
            </NavItem>
            <NavItem>
              <NavLink
                className={classnames({ active: this.state.activeTab === '14' })}
                onClick={async () => { this.toggle('14'); }}
                //disabled={buttonEdit.isWaitRender? true:false}
              >
                {t('label.note')}
              </NavLink>
            </NavItem>
          </Nav>
          {contentTab}
          </>
      );
    } else {
      return <>
        <Row>
          <Button outline color="primary" className="ml-3 mb-3 mt-3" onClick={this.handleBack}>{t('button.return')}</Button>
        </Row>
        <Row className="mb-3 mt-3">
          <Col className="md-6">
            <Label className="font-weight-bold">
              {t('label.manageCandidateDetails')}
            </Label>
          </Col>
          <Col className="md-6" hidden>
            <Button color="primary" style={{ float: "right" }} onClick={(e)=> this.handleDownloadProfileDoc(e)}>{t('button.profileDownload')}</Button>
          </Col>
        </Row>
        <Nav tabs id="nav-tab">
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '1' })}
            >
              {t('label.referralInformation')}
            </NavLink>
          </NavItem>
          {this.state.interviews.map((item, i) => {
            return <NavItem key={item.id}>
              <NavLink
                className={classnames({ active: this.state.activeTab === (i === 0 ? '2' : '13') })}
              >
                {t(`label.interviewInformation${i+1}`)}
              </NavLink>
            </NavItem>
          })}
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '3' })}
            >
              {t('label.basicInformation')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '4' })}
            >
              {t('label.language')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '5' })}
            >
              {t('label.license')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '6' })}
            >
              {t('label.software')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '7' })}
            >
              {t('label.education')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '8' })}
            >
              {t('label.work')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '9' })}
            >
              {t('label.selfPromotion')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '10' })}
            >
              {t('label.hope')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '11' })}
            >
              {t('label.skills')}
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: this.state.activeTab === '14' })}
            >
              {t('label.note')}
            </NavLink>
          </NavItem>
        </Nav>
          <div className="d-flex justify-content-center mt-5">
          <p>{this.props.t('message.loading')}</p>
        </div>
        </>
    }
  }
}

export default withTranslation('common')(withRouter(Candidate));
