import React from 'react'
import {Alert} from 'reactstrap'

/**
 * layout for page error
 */
export default class Error404 extends React.PureComponent {
  render() {
    const { location } = this.props
    return (
        <Alert color="danger">
          Page not found 404 URL: {location.pathname}
      </Alert>
      
    )
  }
}