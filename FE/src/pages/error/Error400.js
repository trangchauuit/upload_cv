import React from 'react'
import {Alert} from 'reactstrap'

/**
 * layout for page error
 */
export default class Error400 extends React.PureComponent {
  render() {
    return (
        <Alert color="danger">
          User does not have the right permissions.
      </Alert>
      
    )
  }
}