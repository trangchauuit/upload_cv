import Error400 from './Error400'
import Error404 from './Error404'
export {
  Error400,
  Error404
}