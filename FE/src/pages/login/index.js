import React, { PureComponent } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import { isAuthenticated, setCookie, API, initHeader } from '../../utils/common'
import { RESPONSE } from '../../utils/const'
import { Redirect } from 'react-router-dom';
import {
  Container, Col, Form,
  FormGroup, Input, Alert,
  Button, Row, Jumbotron, Card, CardBody, CardHeader
} from 'reactstrap'
import i18n from '../../utils/Language';
import { withTranslation } from 'react-i18next';
import './ionicons.min.css';
import './login.css'
import Logo from '../../assets/images/logo2.png'
import { LinkHref } from '../../components/common';

class Login extends PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      email: { value: '', isValid: true, message: '' },
      passWord: { value: '', isValid: true, message: '' },
      isShowNotification: false,
      messageError: ''
    }
  }

  handleChange = (e) => {
    const state = {
      ...this.state,
      [e.target.name]: {
        ...this.state[e.target.name],
        value: e.target.value,
      },
      isShowNotification: false
    }
    this.setState(state)
  }

  handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.handelSubmit()
    }
  }

  formIsValid = () => {
    const email = { ...this.state.email }
    const passWord = { ...this.state.passWord }

    let isOk = true

    if (!email.value) {
      email.isValid = false
      email.message = `${this.props.t('login.errorEmail')}`
      isOk = false
    } else {
      email.isValid = true
      email.message = ''
      isOk = true
    }
    if (!passWord.value) {
      passWord.isValid = false
      passWord.message = `${this.props.t('login.errorPass')}`
      isOk = false
    } else {
      passWord.isValid = true
      passWord.message = ''
      isOk = true
    }
    this.setState({
      email,
      passWord
    });

    return isOk
  }

  handelSubmit = async () => {
    if (this.formIsValid()) {
      this.setState({ isDisabled: true })
      const infos = {
        email: this.state.email.value,
        password: this.state.passWord.value
      }

      await API.post('auth/login', infos)
      .then(response => {
        if (response.status === RESPONSE.STATUS_SUCCESS) {
          const { expires, access_token} = response.data.data
          if (access_token) {
            setCookie(access_token, expires || process.env.REACT_APP_EXPIRES_COOKIE);
            setCookie('admin', expires, 'roles');
            initHeader()
            this.setState({ isLogin: true, role : 'admin'})
          }
        }
      })
      .catch(error => {
        let messageError = ''
        messageError = error.response ? error.response.data.message : error.message
        this.setState({
          isShowNotification: true,
          messageError: messageError,
          isDisabled: false
        })
      })
    }
  }

  setLang = (e) => {
    localStorage.setItem('lang', e.target.lang);
    i18n.changeLanguage(e.target.lang);
  }

  render() {
    const { email, passWord, isShowNotification, isDisabled } = this.state
    let pathname = "/"
    const { from } = this.props.location.state || { from: {  pathname: pathname } }
    const { t } = this.props;
    if (isAuthenticated()) {
      return <Redirect to={from} />
    } else {
      return (
        <Container className="App">
          <Row>
            <Col />
            <Col lg="8">
              <Jumbotron>

               <div className="d-flex justify-content-center">
                  <LinkHref
                  to={'/'}
                  customstyle={{
                    display: 'inline-block',
                    width: '280px',
                    height: '100px',
                    background: `url(${Logo}) top center no-repeat`,
                    backgroundSize: 'contain!important',
                    backgroundRepeat: 'no-repeat!important',
                    backgroundPosition: '50%!important',
                    cursor: 'pointer'
                  }}
                  />

                </div>
                <Card>
                  <CardHeader>
                    <button className="btn" lang="en-US"><span className="lang-sm lang-lbl" lang="en-US" onClick={this.setLang}></span></button>
                    <button className="btn" lang="vi-VN"><span className="lang-sm lang-lbl" lang="vi-VN" onClick={this.setLang}></span></button>
                    <button className="btn" lang="ja-JP"><span className="lang-sm lang-lbl" lang="ja-JP" onClick={this.setLang}></span></button>
                  </CardHeader>
                  <CardBody>
                    <Form className="form">
                      <Col>
                        <FormGroup>
                          <Input
                            type="email"
                            name="email"
                            id="email"
                            placeholder={t('login.username')}
                            value={email.value}
                            onKeyPress={this.handleKeyPress}
                            onChange={this.handleChange}
                            />
                          <Alert color="danger" isOpen={!(email.isValid || email.value)}>
                            {(email.isValid || email.value) ? '' : email.message}
                          </Alert>
                        </FormGroup>
                      </Col>
                      <Col>
                        <FormGroup>
                          <Input
                            type="password"
                            name="passWord"
                            id="passWord"
                            placeholder={t('login.password')}
                            value={passWord.value}
                            onKeyPress={this.handleKeyPress}
                            onChange={this.handleChange}
                            />
                          <Alert color="danger" isOpen={!(passWord.isValid || passWord.value)}>
                            {(passWord.isValid || passWord.value) ? '' : passWord.message}
                          </Alert>
                        </FormGroup>
                      </Col>
                      <Button
                        color="primary"
                        size="lg"
                        block
                        disabled={isDisabled}
                        onClick={this.handelSubmit}
                      >
                        {t('login.login')}
                      </Button>
                    </Form>
                  </CardBody>
                </Card>
                <Alert color="danger" isOpen={isShowNotification} className="testA">
                  {this.props.t('login.errorLogin')}
                </Alert>
              </Jumbotron>
            </Col>
            <Col />
          </Row>
        </Container>
      );
    }
  }
}

export default withTranslation('common')(Login);
