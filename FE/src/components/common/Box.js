import styled, { css } from 'styled-components'

const Box = styled.div`
  border: ${props => props.border};
  animation: ${props => props.animation ? props.animation : 'none'};
  ${props => css`${props.customstyle}`};
`

export default Box