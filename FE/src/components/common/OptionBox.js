import styled, { css } from 'styled-components'

const OptionBox = styled.option`
  ${props => css`${props.customStyle}`};
`

export default OptionBox