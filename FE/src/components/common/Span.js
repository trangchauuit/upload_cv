import styled, { css } from 'styled-components'

const Span = styled.span`
  display: flex;
  align-items: center;
  padding: .375rem .75rem;
  margin-bottom: 0;
  ${props => css`${props.customstyle}`};
`
export default Span
