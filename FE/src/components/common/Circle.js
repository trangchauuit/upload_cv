import React from 'react'
import styled, { css } from 'styled-components'

const Wrapper = styled.div`
  border: ${props => props.border || '3px'} solid ${props => props.borderColor || '#000000'};
  width: ${props => props.width || '100px'};
  height: ${props => props.width || '100px'};
  border-radius: 50%;
  display: flex;
  justify-content: center;
  align-items: center;
  ${props => css`${props.customstyle}`};
`

class Circle extends React.Component {
  render() {
    return <Wrapper {...this.props}>
      {this.props.text}
    </Wrapper>
  }
}

export default Circle;
