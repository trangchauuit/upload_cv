import styled, { css } from 'styled-components'
import DatePicker from 'react-datepicker'
import 'react-datepicker/dist/react-datepicker.css'

const TextBox = styled.input`
  height: 36px;
  border-radius: 4px;
  background-color: #ffffff;
  border: solid 1px #cccccc;
  width: 100%;
  outline: none;
  padding: 0 10px;
  box-sizing: border-box;
  ${props => css`${props.customStyle}`};
`
export const TextArea = styled.textarea`
  background: #fff;
  border: 1px solid #ccc;
  width: 100%;
  box-sizing: border-box;
  ${props => css`${props.customStyle}`};
`

export const CustomDatePicker = styled(DatePicker)`
  height: 36px;
  border-radius: 4px;
  background-color: #ffffff;
  border: solid 1px #cccccc;
  width: 100%;
  outline: none;
  padding: 0 10px;
  box-sizing: border-box;
  ${props => css`${props.customStyle}`};
`

export const Error = styled.p`
  color: red;
  margin-top: 10px;
  ${props => css`${props.customStyle}`};
`

export default TextBox
