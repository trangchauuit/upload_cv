import styled, { css } from 'styled-components'
import { Link as RouterLink } from 'react-router-dom'

export const LinkButton = styled(RouterLink)`
  width: 160px;
  height: 34px;
  border-radius: 4px;
  background-color: ${props => props.secondary ? '#f5a623' : props.success ? '#2ecc71' : props.danger ? '#d9534f' : props.transparent ? 'transparent' : '#0275d8'};
  border: solid 1px ${props => props.secondary ? '#f5a623' : props.success ? '#2ecc71' : props.danger ? '#d9534f' : props.transparent ? 'transparent' : '#0275d8'};
  color: ${props => props.transparent ? '#333' : '#fff'};
  ${props => props.transparent ? css`
    font-weight: bold;` : ''};
  outline: none;
  font-size: 14px;
  text-decoration: none;
  line-height: 34px;
  text-align: center;
  display: inline-block;
  &:hover {
    opacity: 0.8;
    cursor: pointer;
    ${props => props.hover ? props.hover : ''}
  }
  ${props => css`${props.customstyle}`};
`

export const BaseLink = styled(RouterLink)`
  color: #fff;
  text-decoration: none;
  ${props => css`${props.customstyle}`};
`
export const LinkHref = styled(RouterLink)`
  color: #000000;
  text-decoration: none;
  display: block;
  ${props => css`${props.customstyle}`};
`