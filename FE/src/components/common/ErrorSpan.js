import React, { Component } from 'react';
import Span from './Span'
import { Error } from './TextBox'
import PropTypes from 'prop-types'

class ErrorSpan extends Component {
    constructor(props) {
      super(props);
    }

    render() {
        const { errorMessage } = this.props
      return (
        <Span
            customStyle={{
                color: 'red',
                fontSize: '13px',
                display: `${errorMessage ? '' : 'none'}`
            }}
            >
              { errorMessage && <Error>{errorMessage}</Error>}
        </Span>
      );
    }
  }
  
  ErrorSpan.propTypes = {
    errorMessage: PropTypes.string,
  }
  
  ErrorSpan.defaultProps = {
    errorMessage: '',
  }
  export default ErrorSpan;