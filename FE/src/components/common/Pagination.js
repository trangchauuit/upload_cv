import React, { Component } from 'react'
import { Row, Col, FormGroup, Input, Button } from 'reactstrap';
import PropTypes from 'prop-types'
import { SIZE_PER_PAGE_LIST } from '../../utils/const'
import { OptionBox } from '../common'
class Pagination extends Component {
  handlePaginationPrev = (e) => {
    e.preventDefault()
    this.props.handlePrev(e)
  }

  handlePaginationNext = (e) => {
    e.preventDefault()
    this.props.handleNext(e)
  }

  handleSizePerPage = (e) => {
    e.preventDefault()
    this.props.handleSizePage(e.target.value)
  }

  render() {
    const { showPrevButton, showNextButton, currentPage, pageSize } = this.props
    return (
      <Row>
        <Col>
          <FormGroup>
            <Button color="link" onClick={(e) => this.handlePaginationPrev(e)} style={{ display: !showPrevButton ? 'none' : '' }}> {"<"} </Button>

            <Button outline color="primary"> {currentPage}</Button>
            <Button color="link" onClick={(e) => this.handlePaginationNext(e)} style={{ display: !showNextButton ? 'none' : '' }}> {">"} </Button>
          </FormGroup>
        </Col>
        <Col>
          <FormGroup className="float-right">
            <Input type="select" color="link" name="sizePerPage" id="sizePerPage" onChange={(e) => this.handleSizePerPage(e)} defaultValue={pageSize}>
              {SIZE_PER_PAGE_LIST.map((item, index) => {
                return (
                  <OptionBox  key={index} value={item}>{item}</OptionBox>
                )
              })}
            </Input>
          </FormGroup>
        </Col>
      </Row>
    )
  }
}

Pagination.propTypes = {
  showPrevButton: PropTypes.bool,
  showNextButton: PropTypes.bool,
  currentPage: PropTypes.number,
  totalPage: PropTypes.number
}

Pagination.defaultProps = {
  showPrevButton: false,
  showNextButton: false,
  currentPage: 1,
  totalPage: 1
}

export default Pagination