import React, { Component } from 'react';
import { Button, Modal, ModalHeader, ModalBody, ModalFooter } from 'reactstrap';

class ModalNotice extends Component {
  constructor(props) {
    super(props);
    this.state = {
      content: this.props.content,
      isShowModal: this.props.isShowModal
    };
  }
  toggle = ()=> {
    this.setState(prevstate => ({ isShowModal: !prevstate.isShowModal}));
  }
  render() {
    return (
      <Modal isOpen={this.state.isShowModal} toggle={this.toggle}>
        <ModalHeader toggle={this.toggle}>通知</ModalHeader>
        <ModalBody>
            {this.state.content}
        </ModalBody>
        <ModalFooter>
          <Button color="secondary" onClick={this.toggle}>OK</Button>
        </ModalFooter>
      </Modal>
    );
  }
}
export default ModalNotice;