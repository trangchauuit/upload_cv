import styled, { css } from 'styled-components'

const FlexBox = styled.div`
  display: flex;
  flex-direction: ${props => props.direction || 'row'};
  justify-content: ${props => props.justifyContent || 'flex-start'};
  align-items: ${props => props.alignItems || 'center'};
  ${props => css`${props.customstyle}`};

  &:hover {
    ${props => css`${props.customstyleHover}`};
  }
`

export const CustomFlexBox = styled(FlexBox)`
  &:nth-child(even) {
    background-color: #e6e6e6;
  }
  &:nth-child(odd) {
    background-color: #d6d6d6;
  }
  &:hover {
    background-color: #f5a623;
  }
  border-bottom: 1px solid #ccc;
`

export default FlexBox

