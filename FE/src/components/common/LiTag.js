import styled, { css } from 'styled-components'

const LiTag = styled.li`
  padding: 15px 10px;
  box-sizing: border-box;
  list-style: none;
  &:hover {
    background-color: rgb(232, 234, 246, 1);;
  }
  >span {
    &:after {
      content: "";
      border-width: 7px 5px 0;
      margin-left: 6px;
      border-style: solid;
      border-color: #313131 transparent;
      display: inline-block;
      width: 0;
      transform: rotate(-90deg)
    }
  }
  ${props => css`${props.customstyle}`};
`
export default LiTag