import React from 'react'
import { Box, UlTag, LiTag, LinkHref } from '../../components/common'
import { withRouter } from 'react-router'
import Logo from '../../assets/images/logo2.png'
import './menu.css'
import { logOut } from '../../utils/common'
import Cookies from 'js-cookie';
import { withTranslation } from 'react-i18next';

class MenuBar extends React.Component {

  constructor(props) {
    super(props)
    this.state = {
      isOpenedModalSwitchBranch: false,
      isDefault: true,
      role: Cookies.get('roles')
    }
  }

  componentDidMount() {
    // hanlde click sidebar
    document.getElementById('sidebarCollapse').onclick = function () {
      var menuSideBar = document.getElementById('sidebar')
      var contentMenu = document.getElementById('content-menu')
      var backdrop = document.getElementById('backdrop')

      if (!menuSideBar.classList.contains("activeMenuBar")) {
        menuSideBar.classList.add('activeMenuBar')
        contentMenu.classList.add('active-contentMenu')
        backdrop.classList.add('show')
      } else {
        menuSideBar.classList.remove('active')
        contentMenu.classList.remove('active-contentMenu')
        backdrop.classList.remove('show')
      }
    }

    // close menu when click element without sidebar
    document.getElementById('backdrop').onclick = function () {
      var menuSideBar = document.getElementById('sidebar')
      var contentMenu = document.getElementById('content-menu')
      var backdrop = document.getElementById('backdrop')

      if (menuSideBar.classList.contains("activeMenuBar")) {
        menuSideBar.classList.remove('activeMenuBar')
        contentMenu.classList.remove('active-contentMenu')
        backdrop.classList.remove('show')
      }
    }
  }

  confirmLogout = () => {
    if (window.confirm(`${this.props.t('login.confirmout')}`)) {
      logOut()
      this.props.history.push('/login')
    }
  }

  render() {
    const { t } = this.props;
    return (
      <Box
        id="sidebar"
        customstyle={{
          width: '0',
          height: '100vh',
          backgroundColor: '#ffff',
          position: 'fixed',
          top: '0',
          left: '0',
          transition: 'width 0.2s',
          transitionTimingFunction: 'ease',
          zIndex: '9',
          overflow: 'auto'
        }}
      >
        <Box
          customstyle={{
            padding: '0 30px'
          }}
        >
          <LinkHref
            to={['school_receptionist','school_attender'].includes(this.state.role) ? "/checkinout" : "/"}
            customstyle={{
              display: 'inline-block',
              width: '200px',
              height: '100px',
              background: `url(${Logo}) top center no-repeat`,
              backgroundSize: 'contain!important',
              backgroundRepeat: 'no-repeat!important',
              backgroundPosition: '50%!important',
              cursor: 'pointer'
            }}
          />
        </Box>
        <Box id="content-menu"
          customstyle={{
            color: '#000000',
            textTransform: 'uppercase',
            fontSize: '18px',
            paddingLeft: '15px'
          }}
        >
          <UlTag>
            {['admin'].includes(this.state.role) ? <LiTag><LinkHref to='/listCandidates'> {t('menubar.link')}</LinkHref></LiTag> : ''}
            <LiTag>
              <LinkHref to='/timekeeping'>
                {t('menubar.timekeeping')}
              </LinkHref>
            </LiTag>
            <LiTag><div style={{cursor: 'pointer'}} onClick={() => this.confirmLogout()}>{t('login.logout')}</div></LiTag>
          </UlTag>
        </Box>

      </Box>


    )
  }
}
export default withTranslation('common')(withRouter(MenuBar))
