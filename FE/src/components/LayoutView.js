import React from 'react'
import { Box } from '../components/common'
import { Navbar, NavbarToggler, NavbarBrand } from 'reactstrap'
import Cookies from 'js-cookie';
import { withTranslation } from 'react-i18next';

class LayoutView extends React.PureComponent {
  constructor(props) {
    super(props)
    this.state = {
      role: Cookies.get('roles')
    }
  }

  render() {
    return (
      <Box>
        <Navbar className="navbar navbar-dark" style={{ backgroundColor: '#02204b' }}>
          <NavbarToggler id="sidebarCollapse" className="ml-2" >
            
          </NavbarToggler>
          <NavbarBrand href={"/"} className="mr-auto">{this.props.t('menubar.link')}</NavbarBrand>
        </Navbar>
        {this.props.children}
      </Box>
    )
  }
}
export default withTranslation('common')(LayoutView)