import React from 'react';
import { TabContent, Nav, NavItem, NavLink } from 'reactstrap';
import PropTypes from 'prop-types'
import './tabs.css'
class Tabs extends React.Component {
    constructor(props) {
        super(props)
        this.state = {
            activeTab: 1,
            setActiveTab: 1
        }
    }

    render() {

        const { children, labels = [], activeTab, handleChangeTab } = this.props
        return (
            <div>
                <Nav tabs className={'tabs_Nav mx-auto'}>
                    {labels.map((item, index) => {
                        return (
                            <NavItem key={index}  className={activeTab === (index + 1).toString() ? 'tabs_NavItem' : ''}>
                                <NavLink
                                    className={activeTab === (index + 1).toString() ? 'active tabs_NavLinkActive' : 'tabs_NavLink'}
                                    onClick={() => { handleChangeTab((index + 1).toString()); }}
                                >
                                    {item}
                                </NavLink>
                            </NavItem>
                        )
                    })}
                </Nav>

                <TabContent activeTab={activeTab}>
                    {children}
                </TabContent>
            </div>
        );
    }
}

Tabs.propTypes = {
    children: PropTypes.node,
    activeTab: PropTypes.string,
    handleChangeTab: PropTypes.func
}
Tabs.defaultProps = {
    children: '',
    activeTab: 1,
    handleChangeTab: () => { },
}

export default Tabs 