<!DOCTYPE html>
<html>

<head>
	<title>Email UploadCV</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
	<p><?php print_r($interview['name']); ?>様</p>
	<p>お問い合わせありがとうございます。</p>
	<p>この度は、サオマイジャパン(以下SJ)にご興味をお持ちいただき、誠にありがとうございます。
		SJの人材紹介サービスに利用にあたり、オンライン面談 (Zoom Meetings)をいたしたく存じます。</p>
	<p>下記URLにてご都合のよろしいお日にち・お時間帯をお聞かせください。</p>
	<p>Bạn cảm phiền chọn thời gian trao đổi với Saomai Japan ở link dưới nhé. Xin cảm ơn.</p>
	<p> <i><u><?php print_r($interview['booking_url']); ?></u></i></p>
	<p>【今後の流れ】</p>
	<ol>
		<li>(SJ指定の)履歴書の作成・提出</li>
		<li>SJとの面談（必須）</li>
		<li>企業との面接</li>
		<li>内定&入社</li>
	</ol>
	<p>となります。</p>
	<p>その他、ご不明な点などございましたら、当メールアドレスまでお気軽にお問い合わせください。</p>
	<p>それでは、お返事お待ちしております。引き続きよろしくお願いいたします。</p>
</body>

</html>