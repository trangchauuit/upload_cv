<!DOCTYPE html>
<html>

<head>
	<title>Email UploadCV</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
	<p>Chào bạn <?php print_r($interview['name']); ?>,</p>
	<p>Chúc mừng bạn đã vượt qua hai vòng phỏng vấn tại Đà Nẵng Fujikin.</p>
	<p>Vui lòng xem tệp đính kèm Offer Letter, và chúng tôi mong sẽ nhận được sự xác nhận của bạn với đề nghị này vào <?php print_r($interview['date_expired']); ?> </p>
	<p>Ngoài ra, chúng tôi cũng mong muốn bạn cung cấp cho chứng tôi những tài liệu sau đây vào ngày làm việc đầu tiên:</p>
	<ol>
		<li>CMND /Công cước công dân</li>
		<li>Chứng chỉ đại học</li>
		<li>CV</li>
	</ol>
	<p>Chúng tôi rất mong chờ được chào đón bạn tham gia vào đội ngũ Đà Nẵng Fujikin. </p>
	<p>Nếu có bất cứ câu hỏi nào, xin đừng ngần ngại mà hãy liên lạc với chúng tôi nhé!</p>
	<p>Chúc bạn có một ngày tốt lành.</p>
	<p>Back Office – Da Nang Fujikin</p>
	<p>Add: Lot A14, Production area of Danang Hightech Park, Hoa Lien Commune, Hoa Vang District, Danang City, Vietnam</p>
	<p>Tel: (+84) 0236 3525 580 </p>
	<p>Website: https://fujikindnf.com</p>
</body>

</html>