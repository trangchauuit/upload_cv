<!DOCTYPE html>
<html>

<head>
	<title>Email UploadCV</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
	<p>Chào bạn <?php print_r($interview['name']); ?>,</p>
	<p>Cảm ơn bạn đã ứng tuyển vào vị trí <?php print_r($interview['job_title']); ?> tại Đà Nẵng Fujikin. Qua CV của bạn, chúng tôi nhận thấy bạn có những tiềm năng để trở thành một phần của công ty chúng tôi. Chúng tôi hy vọng có thể trao đổi thêm với bạn trong một buổi phỏng vấn offline. </p>
	<p>Bạn lựa chọn ngày, giờ phỏng vấn bằng đường link này nhé.</p>
	<p>Link : <i><u><?php print_r($interview['booking_url']); ?></u></i></p>
	<p>Nếu các khoảng thời gian ở đường link chưa thuận tiện cho bạn, vui lòng liên hệ với chúng tôi qua 02363525580 hoặc info@fujikindnf.com để sắp xếp một cuộc hẹn khác.</p>
	<p>Một lần nữa cảm ơn bạn đã quan tâm gia nhập đội ngũ Đà Nẵng Fujikin! Chúng tôi rất mong nhận được sự phản hồi từ bạn.</p>
	<p>Trân trọng,</p>
	<p>Back Office – Da Nang Fujikin</p>
	<p>Add: Lot A14, Production area of Danang Hightech Park, Hoa Lien Commune, Hoa Vang District, Danang City, Vietnam</p>
	<p>Tel: (+84) 0236 3525 580 </p>
	<p>Website: https://fujikindnf.com</p>
</body>

</html>