<!DOCTYPE html>
<html>

<head>
	<title>Email UploadCV</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
	<p>Chào bạn <?php print_r($interview['name']); ?>,</p>
	<p>Lời đầu tiên, cảm ơn bạn đã quan tâm dành thời gian gửi CV ứng tuyển vào vị trí <?php print_r($interview['job_title']); ?> tại Đà Nẵng Fujikin.</p>
	<p>Trong khoảng thời gian từ ngày <?php print_r($interview['date_now']); ?> đến <?php print_r($interview['date_expired']); ?> , Đà Nẵng Fujikin sẽ liên lạc với những ứng viên phù hợp để sắp xếp lịch phỏng vấn.</p>
	<p>Trong trường hợp bạn không nhận được mail hoặc sự liên lạc từ Back Office của Đà Nẵng Fujikin thì đồng nghĩa với việc CV của bạn chưa phù hợp với nhu cầu tuyển dụng hiện tại của Công ty. Chúng tôi rất hy vọng có cơ hội được chào đón bạn tại Đà Nẵng Fujikin.</p>
	<p>Chúng tôi rất hy vọng có cơ hội được chào đón bạn tại Đà Nẵng Fujikin.</p>
	<p>Để có thông tin tuyển dụng chính xác nhất, bạn theo dõi Fanpage:http://fujikindnf.com nhé.</p>
	<p>Nếu bạn có bất cứ câu hỏi gì liên quan đến vị trí tuyển dụng, xin hãy liên lạc qua số điện thoại 02363525580 hoặc Mail info@fujikindnf.com để được giải đáp thắc mắc.</p>
	<p>Chúc bạn có một ngày tốt lành!</p>
	<p>Thanks and Regards,</p>
	<p>Back Office – Da Nang Fujikin</p>
	<p>Add: Lot A14, Production area of Danang Hightech Park, Hoa Lien Commune, Hoa Vang District, Danang City, Vietnam</p>
	<p>Tel: (+84) 0236 3525 580 </p>
	<p>Website: https://fujikindnf.com</p>
</body>

</html>