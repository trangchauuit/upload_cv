<!DOCTYPE html>
<html>

<head>
	<title>Email UploadCV</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
	<p>Gửi <?php print_r($interview['name']); ?>,</p>
	<p>Cảm ơn bạn đã dành thời gian tham gia phỏng vấn cho vị trí <?php print_r($interview['job_title']); ?> . Chúng tôi đánh giá cao sự thể hiện và cố gắng của bạn trong buổi phỏng vấn. </p>
	<p>Tuy nhiên, chúng tôi rất tiếc phải thông báo với bạn rằng chúng tôi đã quyết định lựa chọn một ứng viên khác phù hợp hơn với vị trí <?php print_r($interview['job_title']); ?> và yêu cầu của công việc ở thời điểm hiện tại. Chúng tôi xin phép được lưu lại hồ sơ của bạn và sẽ liện hệ lại khi có bất kỳ một vị trí việc làm nào đó phù hợp với bạn trong tương lai.</p>
	<p>Chúc bạn luôn mạnh khỏe và mong rằng bạn sẽ thuận lợi trong hành trình tìm kiếm việc làm nha!</p>
	<p>Trân trọng, </p>
	<p>Back Office – Da Nang Fujikin</p>
	<p>Add: Lot A14, Production area of Danang Hightech Park, Hoa Lien Commune, Hoa Vang District, Danang City, Vietnam</p>
	<p>Tel: (+84) 0236 3525 580 </p>
	<p>Website: https://fujikindnf.com</p>
</body>

</html>