<!DOCTYPE html>
<html>

<head>
	<title>Email UploadCV</title>
	<meta charset="utf-8">
	<link rel="stylesheet" type="text/css" href="style.css">
</head>

<body>
	<p>Gửi <?php print_r($interview['name']); ?>,</p>
	<p>Một lần nữa cảm ơn bạn đã ứng tuyển vào vị trí <?php print_r($interview['job_title']); ?></p>
	<p>Chúng tôi đánh giá cao trình độ, khả năng, sự thể hiện của bạn trong cuộc phỏng vấn đầu tiên. Vì vậy, chúng tôi muốn mời bạn tham gia phỏng vấn lần thứ hai tại Đà Nẵng Fujikin.</p>
	<p>Cũng như lần trước, chúng tôi xin phép được gửi đường link để lựa chọn thời gian phỏng vấn. Vui lòng cho chúng tôi biết, thời điểm nào phù hợp để chúng ta tiến hành cuộc phỏng vấn nhé.</p>
	<p>Link: <i><u><?php print_r($interview['booking_url']); ?></u></i></p>
	<p>Nếu các khoảng thời gian ở đường link chưa thuận tiện cho bạn, vui lòng liên hệ với chúng tôi qua 02363525580 hoặc info@fujikindnf.com để sắp xếp một cuộc hẹn khác.</p>
	<p>Trân trọng,</p>
	<p>Back Office – Da Nang Fujikin</p>
	<p>Add: Lot A14, Production area of Danang Hightech Park, Hoa Lien Commune, Hoa Vang District, Danang City, Vietnam</p>
	<p>Tel: (+84) 0236 3525 580 </p>
	<p>Website: https://fujikindnf.com</p>
</body>

</html>