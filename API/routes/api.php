<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
Route::group(['middleware' => ['language']], function () {
    Route::post('cv/upload', 'App\Http\Controllers\API\CandidateController@uploadCV');

    Route::post('auth/login', 'App\Http\Controllers\API\AuthController@login');

    Route::get('result-interviews/view/{candiId}', 'App\Http\Controllers\API\ResultInterviewController@view');

    Route::get('questions', 'App\Http\Controllers\API\QuestionsController@getList');

    Route::get('candidates/{id}/getTimeInterview/{round}', 'App\Http\Controllers\API\CandidateController@getTimeInterview');

    Route::post('candidates/{id}/sendMailInterview', 'App\Http\Controllers\API\CandidateController@sendMailInterview');

    Route::post('candidates/{id}/sendMailResult', 'App\Http\Controllers\API\CandidateController@sendMailResult');

    Route::post('result-interviews/save/{candiId}', 'App\Http\Controllers\API\ResultInterviewController@saveResult');

    Route::post('result-interviews/continue-interview/{candiId}', 'App\Http\Controllers\API\ResultInterviewController@continueInterview');

    Route::get('result-interviews/download/{candiId}', 'App\Http\Controllers\API\ResultInterviewController@download');

    Route::group([
        'middleware' => [
            'api.admin',
            //'auth:api'
        ]
    ], function () {
        Route::get('auth/logout', 'App\Http\Controllers\API\AuthController@logout');
        Route::get('auth/user', 'App\Http\Controllers\API\AuthController@user');
        Route::group(['prefix' => 'admin'], function () {
            Route::resource('candidates', 'App\Http\Controllers\API\Admin\CandidateController');
            // work
            Route::post('candidates/{id}/work', 'App\Http\Controllers\API\Admin\CandidateController@addWork');
            Route::put('candidates/{id}/work/{workId}', 'App\Http\Controllers\API\Admin\CandidateController@updateWork');
            Route::delete('candidates/{id}/work/{workId}', 'App\Http\Controllers\API\Admin\CandidateController@deleteWork');

            Route::post('candidates/{id}/school', 'App\Http\Controllers\API\Admin\CandidateController@addSchool');
            Route::put('candidates/{id}/school/{schoolId}', 'App\Http\Controllers\API\Admin\CandidateController@updateSchool');
            Route::delete('candidates/{id}/school/{schoolId}', 'App\Http\Controllers\API\Admin\CandidateController@deleteSchool');

            //Route::post('candidates/{id}/software', 'App\Http\Controllers\API\Admin\CandidateController@addSoftwareSkill');
            Route::put('candidates/{id}/software/{softwareId}', 'App\Http\Controllers\API\Admin\CandidateController@updateSoftware');
            Route::put('candidates/{id}/software', 'App\Http\Controllers\API\Admin\CandidateController@insertSoftware');
            //Route::delete('candidates/{id}/software/{softwareId}', 'App\Http\Controllers\API\Admin\CandidateController@deleteSoftwareSkill');

            //Route::post('candidates/{id}/language', 'App\Http\Controllers\API\Admin\CandidateController@addLanguage');
            Route::put('candidates/{id}/language/{languageId}', 'App\Http\Controllers\API\Admin\CandidateController@updateLanguage');

            Route::put('candidates/{id}/language', 'App\Http\Controllers\API\Admin\CandidateController@insertLanguage');
            //Route::delete('candidates/{id}/school/{languageId}', 'App\Http\Controllers\API\Admin\CandidateController@deleteLanguage');


        });
    });

    Route::get('company_types', 'App\Http\Controllers\API\MasterDataController@companyTypes');
});
