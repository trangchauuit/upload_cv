<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class ResultFail extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * The reservation instance.
   *
   * @var Order
   */
  public $interview;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($interview)
  {
    $this->interview = $interview;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    $interview = $this->interview;
    return $this->subject('[Đà Nẵng Fujikin] Thư thông báo kết quả phỏng vấn vị trí ' . $interview['job_title'])
      ->from('noreply@saomai.co.jp', 'Đà Nẵng Fujikin')
      ->view('resultfail')
      ->with(['interview' => $this->interview]);
  }
}
