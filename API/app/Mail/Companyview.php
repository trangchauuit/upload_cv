<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class Companyview extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * The reservation instance.
   *
   * @var Order
   */
  public $candidate;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($candidate)
  {
      $this->candidate = $candidate;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  /**
   * Build the message.
   *
   * @return $this
   */
   public function build()
  {
    return $this->subject('新しいお問い合わせ')
                ->from('noreply@saomai.co.jp', 'Interview Link')
                ->view('companymailview')
                ->with(['candidate' => $this->candidate]);
  }
}