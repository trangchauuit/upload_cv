<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class Interview extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * The reservation instance.
   *
   * @var Order
   */
  public $interview;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($interview)
  {
    $this->interview = $interview;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  /**
   * Build the message.
   *
   * @return $this
   */
  public function build()
  {
    return $this->subject('お問い合わせありがとうございます')
      ->from('noreply@saomai.co.jp', 'Interview Link')
      ->view('mailinterview')
      ->with(['interview' => $this->interview]);
  }
}
