<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;


class InterviewRoundTwoOffline extends Mailable
{
  use Queueable, SerializesModels;

  /**
   * The reservation instance.
   *
   * @var Order
   */
  public $interview;

  /**
   * Create a new message instance.
   *
   * @return void
   */
  public function __construct($interview)
  {
      $this->interview = $interview;
  }

  /**
   * Build the message.
   *
   * @return $this
   */
  /**
   * Build the message.
   *
   * @return $this
   */
   public function build()
  {
    $interview = $this->interview;
    return $this->subject('[Đà Nẵng Fujikin] Phỏng vấn lần 2 cho vị trí  '.$interview['job_title'])
                ->from('noreply@saomai.co.jp', 'Da Nang Fujikin')
                ->view('interviewroundtwooffline')
                ->with(['interview' => $this->interview]);
  }
}