<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CandidateWorkingHistory extends Model
{
    use HasFactory;

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'candidate_working_histories';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'candidate_id',
        'from_month',
        'from_year',
        'to_month',
        'to_year',
        'company',
        'contract_type',
        'work_place_type',
        'work_content',
        'work_skill'
    ];

    protected $hidden = array('created_at', 'updated_at');
}
