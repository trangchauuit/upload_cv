<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    use HasFactory;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'questions';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'content',
        'suggest'
    ];

    public function answer()
    {
        return $this->hasMany('App\Models\Answer', 'question_id');
    }

    public function question_lang()
    {
        return $this->hasMany('App\Models\QuestionLang', 'question_id');
    }
}
