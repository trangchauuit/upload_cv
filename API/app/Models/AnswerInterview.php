<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class AnswerInterview extends Model
{
    use HasFactory;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'answer_interviews';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'question_id',
        'result_interview_id',
        'answer_content',
        'answer_id',
        'comment',
        'result'
    ];

    public function answer()
    {
        return $this->belongsTo('App\Models\Answer', 'answer_id');
    }


    public function question()
    {
        return $this->belongsTo('App\Models\Question', 'question_id');
    }
}
