<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    use HasFactory;

    public $statuses = [
    ];
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'name_kana',
        'name',
        'birthday',
        'year_birthday',
        'month_birthday',
        'day_birthday',
        'age',
        'country',
        'gender',
        'married_status',
        'phone',
        'zip_code',
        'zip_code_contact',
        'address',
        'address_contact',
        'near_train_station',
        'email',
        'japan_visa',
        'japan_visa_expired',
        'japan_visa_type',
        'japan_language_level',
        'english_language_level',
        'korean_language_level',
        'japan_visa_date_expired',
        'other_language',
        'other_language_level',
        'self_pr',
        'applicant_comment',
        'applicant_advantage_skill_explanation',
        'cv_filename',
        'status',
        'introduce_company_type_id',
        'introduce_company_category_id',
        'remark',
        'job_title',
        'year_birthday',
        'month_birthday',
        'day_birthday',
        'company_referral',
        'created_at',
        'note'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function achievements(){
        return $this->hasMany('App\Models\CandidateAchievement', 'candidate_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function schools(){
        return $this->hasMany('App\Models\CandidateEducationHistory', 'candidate_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function works(){
        return $this->hasMany('App\Models\CandidateWorkingHistory', 'candidate_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function languages(){
        return $this->hasMany('App\Models\CandidateLanguage', 'candidate_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function software(){
        return $this->hasMany('App\Models\CandidateSoftwareSkill', 'candidate_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function introduce_company_type(){
        return $this->belongsTo('App\Models\CompanyType', 'introduce_company_type_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function introduce_company_category(){
        return $this->belongsTo('App\Models\CompanyCategory', 'introduce_company_category_id');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function interviews(){
        return $this->hasMany('App\Models\Interview', 'candidate_id');
    }

    public function getStatus() {
        $status = [
            1 => '登録面談待ち',

            2 => '登録面談済み',

            3 => '紹介待ち',

            4 => '選考中',

            5 => '内定',

            6 => '在留資格申請中',
            7 => '入社前教育中',
            8 => '入社済み'
        ];

    }
}
