<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    use HasFactory;
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'answers';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id',
        'question_id',
        'content',
        'score'
    ];

    public function question() {
        return $this->belongsTo('App\Models\Question', 'question_id');
    }

    public function answer_lang(){
        return $this->hasMany('App\Models\AnswerLang', 'answer_id');
    }
}