<?php


namespace App\Utils;


class DateTimeUtil
{
    public static function convertDate($sDate){
        $tempDate = explode('-', $sDate);
        if(count($tempDate) == 3) {
            if ( checkdate($tempDate[1], $tempDate[2], $tempDate[0])) {
                return $sDate;
            }
            if ( checkdate($tempDate[0], $tempDate[1], $tempDate[2])) {
                return $tempDate[2] .'-'. $tempDate[0] .'-'. $tempDate[1];
            }
            return false;
        }

        $tempDate = explode('/', $sDate);
        if(count($tempDate) == 3) {
            if ( checkdate($tempDate[1], $tempDate[0], $tempDate[2])) {
                return $tempDate[2] .'-'. $tempDate[1] .'-'. $tempDate[0];
            }
            if ( checkdate($tempDate[1], $tempDate[2], $tempDate[0])) {
                return $tempDate[0] .'-'. $tempDate[1] .'-'. $tempDate[2];
            }
            return false;
        }

        return false;


    }
}