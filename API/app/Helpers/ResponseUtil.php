<?php
namespace App\Utils;

class ResponseUtil
{
    /**
     * @param string $message
     * @param mixed  $data
     *
     * @return array
     */
    public static function makeResponse($message, $data)
    {
        return [
            'success' => true,
            'code' => \Illuminate\Http\Response::HTTP_OK,
            'data'    => $data,
            'message' => $message,
        ];
    }

    /**
     * @param string $message
     * @param array  $data
     *
     * @return array
     */
    public static function makeError($message, $errorCode = \Illuminate\Http\Response::HTTP_BAD_REQUEST, array $data = [])
    {
        $res = [
            'success' => false,
            'code' => $errorCode,
            'message' => $message,
        ];

        if (!empty($data)) {
            $res['data'] = $data;
        }

        return $res;
    }
}
