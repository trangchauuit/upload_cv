<?php

namespace App\Http\Controllers;

use App\Imports\DataImport;
use App\Imports\ValidateCVExcel;
use Illuminate\Http\Request;
use Maatwebsite\Excel\Facades\Excel;

class ExcelController extends Controller
{
    public function index(Request $request){
        //dd(storage_path('app\public\cv.xlsx'));
        //$data = null;
        $validator = new ValidateCVExcel();
       Excel::import($validator, storage_path('app\public\cv1.xlsx'), null, \Maatwebsite\Excel\Excel::XLSX);
        if ($validator->errors){
            return response()->json($validator->errors);
//            $errors = [];
//            foreach ($validator->errors as $key => $error) {
////                foreach ($error->messages()->toArray() as $err) {
////                    $errors[$key][] = $err[0];
////                }
//                $errors[$key] = $error->messages()->toArray();
//
//            }
            return response()->json($errors);
        }
        $data = new DataImport();
       $data->import(storage_path('app\public\cv.xlsx'), null, \Maatwebsite\Excel\Excel::XLSX);
//        $reader = new \PhpOffice\PhpSpreadsheet\Reader\Xlsx();
//        $data = $reader->load( storage_path('app\public\cv.xlsx'));
//        dd($data);

    }
}
