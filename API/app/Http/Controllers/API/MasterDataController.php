<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\APIController;
use App\Imports\DataImport;
use App\Imports\ValidateCVExcel;
use App\Models\Candidate;
use App\Models\CompanyType;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;

class MasterDataController extends APIController
{
    /**
     * Master Data  - Company Type has company category
     * @param Request $request
     */
    public function companyTypes(Request $request)
    {
        $query = CompanyType::with('categories', 'company_type_lang')
            ->select('company_types.id', 'company_type_langs.name', 'company_type_langs.lang_code');
        if ($lang = $request->get('lang')) {
            $query->join('company_type_langs', 'company_type_langs.company_type_id', '=', 'company_types.id');
            $query->where('company_type_langs.lang_code', '=', $lang);
        }
        $types = $query->get();
        return $this->successResponse($types->toArray());
    }
}
