<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\APIController;
use App\Imports\DataImport;
use Illuminate\Http\Request;
use App\Models\Question;
use App\Models\Answer;

class QuestionsController extends APIController
{
  /**
   * Master Data  - Company Type has company category
   * @param Request $request
   */
  public function getList(Request $request)
  {
    $query = Question::with(['question_lang'])
      ->select('questions.id', 'questions.question_kind', 'question_langs.content', 'question_langs.suggest', 'question_langs.lang_code');
    if ($lang = $request->get('lang')) {
      $query->join('question_langs', 'question_langs.question_id', '=', 'questions.id');
      $query->where('question_langs.lang_code', '=', $lang);
    }
    $questions = $query->get();
    
    foreach ($questions as $question) {
      $queryAnswer = Answer::with(['answer_lang'])
        ->select('answers.id', 'answer_langs.content', 'answer_langs.lang_code', 'answer_langs.score');
      if ($lang = $request->get('lang')) {
        $queryAnswer->join('answer_langs', 'answer_langs.answer_id', '=', 'answers.id');
        $queryAnswer->where('answer_langs.lang_code', '=', $lang);
        $queryAnswer->where('answers.question_id', '=', $question->id);
      }
      $answers = $queryAnswer->get();
      $question['answer'] = $answers;
    }
    return $this->successResponse($questions);
  }
}
