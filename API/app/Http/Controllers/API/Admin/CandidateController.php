<?php

namespace App\Http\Controllers\API\Admin;

use App\Http\Controllers\API\APIController;
use App\Http\Resources\CandidateCollection;
use App\Models\Candidate;
use App\Models\CandidateEducationHistory;
use App\Models\CandidateLanguage;
use App\Models\CandidateSoftwareSkill;
use App\Models\CandidateWorkingHistory;
use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Maatwebsite\Excel\Facades\Excel;

class CandidateController extends APIController
{
    /**
     * Get all Candidate
     * @param Request $request
     * @return JsonResponse
     */
    public function index(Request $request)
    {
        $candidates = Candidate::with(['languages', 'software', 'schools', 'works', 'introduce_company_type', 'introduce_company_category', 'interviews'])
            ->select(
                'candidates.id',
                'candidates.created_at',
                'candidates.name',
                'candidates.name_kana',
                'candidates.birthday',
                'candidates.age',
                'candidates.note',
                'candidates.country',
                'candidates.gender',
                'candidates.married_status',
                'candidates.phone',
                'candidates.zip_code',
                'candidates.email',
                'candidates.japan_visa',
                'candidates.japan_language_level',
                'candidates.status',
                'candidates.introduce_company_type_id',
                'candidates.introduce_company_category_id',
                'candidates.company_referral'
            )
            ->groupBy(
                'candidates.id',
                'candidates.created_at',
                'candidates.name',
                'candidates.name_kana',
                'candidates.birthday',
                'candidates.age',
                'candidates.country',
                'candidates.gender',
                'candidates.married_status',
                'candidates.phone',
                'candidates.zip_code',
                'candidates.email',
                'candidates.japan_visa',
                'candidates.japan_language_level',
                'candidates.status',
                'candidates.introduce_company_type_id',
                'candidates.introduce_company_category_id',
                'candidates.company_referral'
            );

        if ($status = $request->get('status')) {
            $candidates->where('candidates.status', '=', $status);
        }
        if ($apply_from = $request->get('apply_date_from')) {

            $candidates->where('candidates.created_at', '>=', substr($apply_from, 0, 10));
        }

        if ($apply_to = $request->get('apply_date_to')) {
            $candidates->where('candidates.created_at', '<=', substr($apply_to, 0, 10) . ' 23:59');
        }

        if ($status = $request->get('status')) {
            $candidates->where('candidates.status', '=', $status);
        }

        if ($company_referral = $request->get('company_referral')) {
            $candidates->where('candidates.company_referral', 'LIKE', "%$company_referral%");
        }

        if ($japaneseLevel = $request->get('japanese_level')) {
            $candidates->where('candidates.japan_language_level', '=', $japaneseLevel);
        }

        if ($japaneseDegree = $request->get('japanese_degree')) {
            $candidates->join('candidate_languages', 'candidate_languages.candidate_id', '=', 'candidates.id');
            $candidates->where('candidate_languages.name', 'LIKE', "%日本語能力%");
            $candidates->where('candidate_languages.level', '=', $japaneseDegree);
        }

        if ($faculty = $request->get('faculty')) {
            $candidates->join('candidate_education_histories', 'candidate_education_histories.candidate_id', '=', 'candidates.id');
            $candidates->where('candidate_education_histories.faculty', 'LIKE', "%$faculty%");
        }

        if ($introduce_company_type_id = $request->get('introduce_company_type_id')) {
            $candidates->join('company_types', 'company_types.id', '=', 'candidates.introduce_company_type_id');
            $candidates->where('candidates.introduce_company_type_id', '=', $introduce_company_type_id);
        }
        if ($companyCategory = $request->get('introduce_company_category')) {
            $candidates->join('company_categories', 'company_categories.id', '=', 'candidates.introduce_company_category_id');
            $candidates->where('company_categories.name', 'LIKE', "%$companyCategory%");
        }

        if ($skill = $request->get('skill')) {
            $candidates->leftJoin('candidate_software_skills', 'candidate_software_skills.candidate_id', '=', 'candidates.id');
            $candidates->where('candidate_software_skills.name', '=', $skill)
                ->where('candidate_software_skills.year', '>', 0);
        }

        $limit = $request->get('limit', 15);
        $candidates = $candidates->orderBy('id', 'desc')->paginate($limit);

        return $this->successResponse(new CandidateCollection($candidates));
    }

    /**
     * Show Candidate
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponses
     */
    public function show(Request $request, $id)
    {
        $candidate = Candidate::with(['languages', 'software', 'schools', 'works', 'introduce_company_type', 'introduce_company_category', 'interviews'])
            ->find($id);
        return $this->successResponse($candidate);
    }

    /**
     * Update Candidate by ID
     * @method POST
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request, $id)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return $this->failedResponse(['error' => 'Not found']);
        }
        $validate = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'email' => 'required|email',
            'gender' => 'required|min:0|max:2',
            'name' => 'required|min:3|max:100',
            'name_kana' => 'required',
            'birthday' => 'nullable|date',
            'address' => 'nullable|date',
            'age' => 'required|numeric',
            'country' => 'required',
            'address' =>  'required',
            'japan_language_level' => 'required',
            'status' => 'required|integer',
            'japan_visa_date_expired' => 'nullable|date',
            'introduce_company_type_id' => 'nullable|integer',
            'introduce_company_category_id' => 'nullable|integer'
        ]);
        if ($validate->fails()) {
            return $this->failedResponse($validate->errors());
        }
        Candidate::where('id', $id)->update($request->all());
        $candidate = Candidate::with(['languages', 'software', 'schools', 'works'])->find($id);
        return $this->successResponse($candidate);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function destroy(Request $request, $id)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return $this->failedResponse(['error' => 'Not found']);
        }
        CandidateLanguage::where('candidate_id', $id)->delete();
        CandidateWorkingHistory::where('candidate_id', $id)->delete();
        CandidateSoftwareSkill::where('candidate_id', $id)->delete();
        CandidateEducationHistory::where('candidate_id', $id)->delete();
        Candidate::where('id', $id)->delete();
        return $this->successResponse([], 'Delete Candidate successful');
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function addWork(Request $request, $id)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return $this->failedResponse(['error' => 'Not found']);
        }

        $validate = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'from_month' => 'required|numeric|min:0|max:12',
            'from_year' =>  'required|numeric',
            'to_month' =>  'required|numeric|min:0|max:12',
            'to_year' => 'required|numeric|gte:from_year',
            'company' => 'required',
            'work_content' => 'required',
            'work_skill' => 'required'
        ]);
        if ($validate->fails()) {
            return $this->failedResponse($validate->errors());
        }
        $data = $request->all();
        $data['candidate_id'] = $id;
        $work = CandidateWorkingHistory::create($data);
        return $this->successResponse($work);
    }

    /**
     * @param Request $request
     * @param $id
     * @param $workId
     * @return JsonResponse
     */
    public function updateWork(Request $request, $id,  $workId)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return $this->failedResponse(['error' => 'Not found candidate']);
        }

        $work = CandidateWorkingHistory::find($workId);
        if (!$work) {
            return $this->failedResponse(['error' => 'Not found work info']);
        }

        $validate = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'from_month' => 'required|numeric|min:0|max:12',
            'from_year' =>  'required|numeric',
            'to_month' =>  'required|numeric|min:0|max:12',
            'to_year' => 'required|numeric|gte:from_year',
            'company' => 'required',
            'work_content' => 'required',
        ]);
        if ($validate->fails()) {
            return $this->failedResponse($validate->errors());
        }
        $data = $request->all();

        $work = $work->update($data);
        $candidate = Candidate::with(['languages', 'software', 'schools', 'works'])->find($id);
        return $this->successResponse($candidate);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteWork(Request $request, $id)
    {
        CandidateWorkingHistory::delete($id);
        return $this->successResponse([], 'Delete Candidate Work History successful');
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function addSchool(Request $request, $id)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return $this->failedResponse(['error' => 'Not found']);
        }

        $validate = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'from_month' => 'required|numeric|min:0|max:12',
            'from_year' =>  'required|numeric',
            'to_month' =>  'required|numeric|min:0|max:12',
            'to_year' => 'required|numeric|gte:from_year',
            'school_name' => 'required',
            'faculty' => 'required',
            'study_skills' => 'required'
        ]);
        if ($validate->fails()) {
            return $this->failedResponse($validate->errors());
        }
        $data = $request->all();
        $data['candidate_id'] = $id;
        $school = CandidateEducationHistory::create($data);
        return $this->successResponse($school);
    }

    /**
     * @param Request $request
     * @param $id
     * @param $workId
     * @return JsonResponse
     */
    public function updateSchool(Request $request, $id,  $schoolId)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return $this->failedResponse(['error' => 'Not found candidate']);
        }

        $school = CandidateEducationHistory::find($schoolId);
        if (!$school) {
            return $this->failedResponse(['error' => 'Not found work info']);
        }

        $validate = \Illuminate\Support\Facades\Validator::make($request->all(), [
            'from_month' => 'required|numeric|min:0|max:12',
            'from_year' =>  'required|numeric',
            'to_month' =>  'required|numeric|min:0|max:12',
            'to_year' => 'required|numeric|gte:from_year',
            'school_name' => 'required',
        ]);
        if ($validate->fails()) {
            return $this->failedResponse($validate->errors());
        }
        $data = $request->all();

        $school->update($data);
        $candidate = Candidate::with(['languages', 'software', 'schools', 'works'])->find($id);
        return $this->successResponse($candidate);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     * @throws \Exception
     */
    public function deleteSchool(Request $request, $id)
    {
        CandidateEducationHistory::delete($id);
        return $this->successResponse([], 'Delete Candidate Work History successful');
    }

    /**
     * @param Request $request
     * @param $id
     * @param $softwareId
     * @return JsonResponse
     */
    public function updateSoftware(Request $request, $id,  $softwareId)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return $this->failedResponse(['error' => 'Not found candidate']);
        }

        $software = CandidateSoftwareSkill::find($softwareId);
        if (!$software) {
            return $this->failedResponse(['error' => 'Not found software info']);
        }
        $data = $request->all();

        $software->update($data);
        $candidate = Candidate::with(['languages', 'software', 'schools', 'works'])->find($id);
        return $this->successResponse($candidate);
    }

    /**
     * @param Request $request
     * @param $id
     * @param $softwareId
     * @return JsonResponse
     */
    public function insertSoftware(Request $request, $id)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return $this->failedResponse(['error' => 'Not found candidate']);
        }

        $data = $request->all();
        $data['candidate_id'] = $id;
        $software = CandidateSoftwareSkill::create($data);
        $candidate = Candidate::with(['languages', 'software', 'schools', 'works'])->find($id);
        return $this->successResponse($candidate);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function updateLanguage(Request $request, $id,  $languageId)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return $this->failedResponse(['error' => 'Not found candidate']);
        }

        $language = CandidateLanguage::find($languageId);
        if (!$language) {
            return $this->failedResponse(['error' => 'Not found software info']);
        }

        $data = $request->all();

        $language->update($data);
        $candidate = Candidate::with(['languages', 'software', 'schools', 'works'])->find($id);
        return $this->successResponse($candidate);
    }

    /**
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function insertLanguage(Request $request, $id)
    {
        $candidate = Candidate::find($id);
        if (!$candidate) {
            return $this->failedResponse(['error' => 'Not found candidate']);
        }

        $data = $request->all();
        $data['candidate_id'] = $id;
        $language = CandidateLanguage::create($data);
        $candidate = Candidate::with(['languages', 'software', 'schools', 'works'])->find($id);
        return $this->successResponse($candidate);
    }

    public function exportCv(Request $request)
    {
        Excel::load('template.xls', function ($doc) {

            $sheet = $doc->setActiveSheetIndex(0);
            $sheet->setCellValue('D5', 'Test');
        })->download('xlsx');
    }
}
