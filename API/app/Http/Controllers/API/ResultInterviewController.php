<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\APIController;
use App\Imports\DataImport;
use Illuminate\Http\Request;
use App\Models\AnswerInterview;
use App\Models\ResultInterview;
use App\Models\Interview;
use Carbon\Carbon;

class ResultInterviewController extends APIController
{
  /**
   * Master Data  - Company Type has company category
   * @param Request $request
   */
  public function saveResult(Request $request, $id)
  {
    $data = $request->all();

    $data['candidate_id'] = $id;

    $result_interview = ResultInterview::select('result_interviews.id')
      ->with(['answer_interview', 'answer_interview.question',  'answer_interview.answer', 'answer_interview.answer.question', 'interview'])
      ->join('interviews', 'interviews.id', '=', 'result_interviews.interview_id')
      ->where('result_interviews.candidate_id', '=', $id)
      ->where('interviews.round', '=', $data['round_interview'])
      ->first();
    //print_r($result_interview); die();
    if (!empty($result_interview)) {
      $result_data = [];
      $result_data['candidate_id'] = $id;
      if (!empty($data['evaluate'])) {
        $result_data['evaluate'] = $data['evaluate'];
      }
      if (!empty($data['interviewer'])) {
        $result_data['interviewer'] = $data['interviewer'];
      }

      ResultInterview::where('id', $result_interview['id'])->update($result_data);

      $interview = Interview::where('candidate_id', '=', $id)
        ->where('round', '=', $data['round_interview'])
        ->first();
      $interview_data = [];
      $interview_data['method'] = $data['method'];
      $interview_data['result'] = $data['result'];

      Interview::where('id', $interview['id'])->update($interview_data);

      // Xoa toan bo cau hoi, cau tra loi
      AnswerInterview::where('result_interview_id', $result_interview['id'])->delete();
      // Tao moi cau tra loi
      if (!empty($data['answers'])) {
        $answers = $data['answers'];
        foreach ($answers as $value) {
          $value['result_interview_id'] = $result_interview['id'];
          $result = AnswerInterview::create($value);
        }
      }
    } else {
      $interview = Interview::where('candidate_id', '=', $id)
        ->where('round', '=', $data['round_interview'])
        ->first();
      $interview_data = [];
      $interview_data['method'] = $data['method'];
      $interview_data['result'] = $data['result'];
      Interview::where('id', $interview['id'])->update($interview_data);

      $result_data = [];
      // Luu ket qua phong van
      $result_data['candidate_id'] = $id;
      if (!empty($data['evaluate'])) {
        $result_data['evaluate'] = $data['evaluate'];
      }
      if (!empty($data['interviewer'])) {
        $result_data['interviewer'] = $data['interviewer'];
      }
      $result_data['interview_id'] = $interview['id'];
      $result_interview = ResultInterview::create($result_data);
      if (!empty($data['answers'])) {
        $answers = $data['answers'];
        foreach ($answers as $value) {
          $value['result_interview_id'] = $result_interview['id'];
          $result = AnswerInterview::create($value);
        }
      }
    }

    $result1 = ResultInterview::with(['answer_interview', 'answer_interview.answer', 'answer_interview.question', 'answer_interview.answer.question',  'answer_interview.answer.question.question_lang'])->find($result_interview['id']);
    return $this->successResponse($result1);
  }

  /**
   * Get result interview of Candidate
   * @param id $id
   */
  public function view($id)
  {
    // hien thi qua phong van

    $result = ResultInterview::with(['answer_interview', 'interview', 'answer_interview.answer', 'answer_interview.question', 'answer_interview.answer.question', 'answer_interview.answer.question.question_lang'])->where('candidate_id', '=', $id)->get();
    return $this->successResponse($result);
  }

  /**
   * Get result interview of Candidate
   * @param id $id
   */
  public function download($id)
  {
    $phpWord = new \PhpOffice\PhpWord\PhpWord();
    $section = $phpWord->addSection();
    $text = $section->addText('emp_name');
    $text = $section->addText('emp_salary');
    $text = $section->addText('emp_age');

    $objWriter = \PhpOffice\PhpWord\IOFactory::createWriter($phpWord, 'Word2007');
    $objWriter->save(storage_path('abc.doc'));
    return response()->download(storage_path('abc.doc'));
  }

  public function continueInterview($id)
  {
    $interviewOne = Interview::where('candidate_id', '=', $id)
      ->where('round', '=', 1)
      ->first();
    $interview_data = [];
    $interview_data['is_continue'] = 1;
    if (!empty($interviewOne)) {
      Interview::where('id', $interviewOne['id'])->update($interview_data);
    }

    $interviewTwo = Interview::where('candidate_id', '=', $id)
      ->where('round', '=', 2)
      ->first();
    if (empty($interviewTwo)) {
      $interview_data_new = [];
      $interview_data_new['candidate_id'] = $id;
      $interview_data_new['round'] = 2;
      $interview_data_new = Interview::create($interview_data_new);
    }
    return $this->successResponse($interviewTwo);
  }
}
