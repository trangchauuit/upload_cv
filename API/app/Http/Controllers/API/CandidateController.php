<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\API\APIController;
use App\Imports\DataImport;
use App\Imports\ValidateCVExcel;
use App\Models\Candidate;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Validator;
use Maatwebsite\Excel\Facades\Excel;
use Mail;
use App\Mail\Receive;
use App\Mail\InterviewRoundOneOnline;
use App\Mail\InterviewRoundOneOffline;
use App\Mail\InterviewRoundTwoOnline;
use App\Mail\InterviewRoundTwoOffline;
use App\Mail\ResultSuccess;
use App\Mail\ResultFail;
use App\Mail\Companyview;
use App\Models\Interview;
use Carbon\Carbon;
use DateTime;
use DateTimeZone;

class CandidateController extends APIController
{

   CONST CALENDLY_TOKEN = 'eyJraWQiOiIxY2UxZTEzNjE3ZGNmNzY2YjNjZWJjY2Y4ZGM1YmFmYThhNjVlNjg0MDIzZjdjMzJiZTgzNDliMjM4MDEzNWI0IiwidHlwIjoiUEFUIiwiYWxnIjoiRVMyNTYifQ.eyJpc3MiOiJodHRwczovL2F1dGguY2FsZW5kbHkuY29tIiwiaWF0IjoxNjgyMDQwNTM4LCJqdGkiOiJmNTU2NjQ2MS0yYjBkLTRiNjktOTY2MS1mNTk2MjhiY2EzZmUiLCJ1c2VyX3V1aWQiOiI5OTY1Mjc4OC1jMzcyLTRmNTItYjZiYS04NWNjZmU0YzZmNjIifQ.zF0ZK1TAxHnWqE6cJsG12dhZRbfqsQqSdct8emS74QpyxMIfl0SLF5hgHrTXdMJt_BD6yt2AQZqWDtJkfSjliQ';
   CONST INTERVIEW_ONE_ON_LINK = 'https://api.calendly.com/event_types/6dae5d8f-0655-409d-a5a4-53c6f202eac2';
   CONST INTERVIEW_ONE_OFF_LINK = 'https://api.calendly.com/event_types/553d62cf-2098-4bdf-a728-984810c19a50';
   CONST INTERVIEW_TWO_ON_LINK = 'https://api.calendly.com/event_types/7241e216-9dee-4788-96fe-8e3e5d6fa2b0';
   CONST INTERVIEW_TWO_OFF_LINK = 'https://api.calendly.com/event_types/43667cb7-d944-4d38-827d-211e14bccb2a';
   CONST USER_LINK = 'https://api.calendly.com/users/99652788-c372-4f52-b6ba-85ccfe4c6f62';
   CONST ORGANIZATION_LINK = 'https://api.calendly.com/organizations/a2052c76-f2d6-4716-845b-2212961c54ee';
   public function uploadCV(Request $request)
   {
      try {
         Log::info('------------- Start Import  CV ---------------');
         Log::info('----------- Request all------------', $request->all());
         $validator = Validator::make($request->all(), [
            'cv' => 'required|regex:/^(https?:\/\/)?([\da-z\.-]+)\.([a-z\.]{2,6})([\/\w \.-]*)*\/?$/'
         ]);
         
         $validator = new ValidateCVExcel();
         Excel::import($validator, $request->cv, null, \Maatwebsite\Excel\Excel::XLSX);
         if ($validator->errors) {
            Log::error('@@@@@@@@@@@@ Errors Occur: Content CV  @@@@@@@@@@@@', $validator->errors->toArray());
            return  $this->failedResponse($validator->errors);
         }
         Log::info('------------- Upload CV Success - Start process data ---------------');
         $data = new DataImport();
         Excel::import($data, $request->cv, null, \Maatwebsite\Excel\Excel::XLSX);

         $destinationPath = 'candidate';
         $cvFile = time() . '_' . $request->cv->getClientOriginalName();
         $request->cv->move(public_path($destinationPath), $cvFile);
         $filePath = '/candidate/' . $cvFile;
         Candidate::where(['id' => $data->candidate->id])->update(['cv_filename' => $filePath]);


         $destinationPath = 'pictures';
         $pictureFile = time() . '_' . $request->picture->getClientOriginalName();
         $request->picture->move(public_path($destinationPath), $pictureFile);
         $picturePath = '/pictures/' . $pictureFile;
         Candidate::where(['id' => $data->candidate->id])->update(['picture_path' => $picturePath]);

         // Job title 
         if (!empty($request->job_title)) {
            Candidate::where(['id' => $data->candidate->id])->update(['job_title' => $request->job_title]);
         }
         $candidate = Candidate::with(['schools', 'software', 'works', 'languages'])->find($data->candidate->id);
         $candidate->cv_filename = Storage::url($candidate->cv_filename);

         // Create Interview One
         $interview_data = [];
         $interview_data['candidate_id'] = $data->candidate->id;
         $interview_data['round'] = 1;
         $interview_data = Interview::create($interview_data);

         $date_expired = strtotime("+7 day");
         $date_expired = date('d-m-Y', $date_expired);
         Log::info('------------- End Upload CV - Successful ---------------');

         $candidate['date_expired'] = $date_expired;
         $candidate['date_now'] = date('d-m-Y');
         $candidate['job_title'] = $request->job_title;
         Mail::to($candidate['email'])->send(new Receive($candidate));

         $email_company = "nhaphuong1704@gmail.com";
         Mail::to($email_company)->cc(['trangchauuit@gmail.com'])->send(new Companyview($candidate));

         return $this->successResponse($candidate, 'Import successful');
      }  catch (\Throwable $th) {
         throw $th;
      }
   }

   // Call thirty api
   public function callAPI($method, $url, $data)
   {
      $curl = curl_init();
      switch ($method) {
         case "POST":
            curl_setopt($curl, CURLOPT_POST, 1);
            if ($data)
               curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
         case "PUT":
            curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "PUT");
            if ($data)
               curl_setopt($curl, CURLOPT_POSTFIELDS, $data);
            break;
         default:
            if ($data)
               $url = sprintf("%s?%s", $url, http_build_query($data));
      }
      $authorization = self::CALENDLY_TOKEN;
      curl_setopt($curl, CURLOPT_URL, $url);
      curl_setopt($curl, CURLOPT_HTTPHEADER, array(
         'Authorization: Bearer ' . $authorization,
         'Content-Type: application/json',
      ));
      curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
      curl_setopt($curl, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
      $result = curl_exec($curl);
      if (!$result) {
         die("Connection Failure");
      }
      curl_close($curl);
      return $result;
   }
   public function getTimeInterview($id, $round = null)
   {
      // Get time interview
      $candidate = Candidate::find($id);
      $email = $candidate->email;
      $make_call = $this->callAPI('GET', 'https://api.calendly.com/scheduled_events?user='.self::USER_LINK.'&organization='.self::ORGANIZATION_LINK.'&invitee_email=' . $email . '&status=active', null);
      $response = json_decode($make_call, true);
      if (empty($response['collection'])) {
         return $this->successResponse(null);
      }
      if (!empty($round) && $round == 1 && !empty($response['collection'][0])) {
         $meeting = $response['collection'][0];
      } else if (!empty($round) && $round == 2 && !empty($response['collection'][1])) {
         $meeting = $response['collection'][1];
      } else {
         $meeting = null;
      }


      $array_meeting = [];
      $array_meeting['email'] = $email;
      if (!empty($meeting['start_time'])) {
         $array_meeting['start_time'] = $meeting['start_time'];
      }
      if (!empty($meeting['end_time'])) {
         $array_meeting['end_time'] = $meeting['end_time'];
      }
      if (!empty($meeting['location']['join_url'])) {
         $array_meeting['join_url'] = $meeting['location']['join_url'];
      }
      if (!empty($meeting['location']['data']['password'])) {
         $array_meeting['password'] = $meeting['location']['data']['password'];
      }
      return $this->successResponse($array_meeting);
   }

   public function sendMailInterview($id, Request $request)
   {
      // Send mail for candiate
      $candidate = Candidate::with(['schools', 'software', 'works', 'languages'])->find($id);

      if ($request->round == 1 && $request->type_interview == 1) {
         $interviewUrl = self::INTERVIEW_ONE_ON_LINK;
         // Get Link Interview                                                                                
         $data_array =  array(
            "max_event_count"     => 1,
            "owner"     => $interviewUrl,
            "owner_type"     => "EventType"
         );
         $make_call = $this->callAPI('POST', 'https://api.calendly.com/scheduling_links', json_encode($data_array));
         $response = json_decode($make_call, true);
         $candidate['booking_url'] = $response['resource']['booking_url'];
         Mail::to($candidate['email'])->send(new InterviewRoundOneOnline($candidate));
      }

      if ($request->round == 1 && $request->type_interview == 2) {
         $interviewUrl = self::INTERVIEW_ONE_OFF_LINK;
         // Get Link Interview                                                                                
         $data_array =  array(
            "max_event_count"     => 1,
            "owner"     => $interviewUrl,
            "owner_type"     => "EventType"
         );
         $make_call = $this->callAPI('POST', 'https://api.calendly.com/scheduling_links', json_encode($data_array));
         $response = json_decode($make_call, true);
         $candidate['booking_url'] = $response['resource']['booking_url'];
         Mail::to($candidate['email'])->send(new InterviewRoundOneOffline($candidate));
      }

      if ($request->round == 2 && $request->type_interview == 1) {
         $interviewUrl = self::INTERVIEW_TWO_ON_LINK;
         // Get Link Interview                                                                                
         $data_array =  array(
            "max_event_count"     => 1,
            "owner"     => $interviewUrl,
            "owner_type"     => "EventType"
         );
         $make_call = $this->callAPI('POST', 'https://api.calendly.com/scheduling_links', json_encode($data_array));
         $response = json_decode($make_call, true);
         $candidate['booking_url'] = $response['resource']['booking_url'];
         Mail::to($candidate['email'])->send(new InterviewRoundTwoOnline($candidate));
      }

      if ($request->round == 2 && $request->type_interview == 2) {
         $interviewUrl = self::INTERVIEW_TWO_OFF_LINK;
         // Get Link Interview                                                                                
         $data_array =  array(
            "max_event_count"     => 1,
            "owner"     => $interviewUrl,
            "owner_type"     => "EventType"
         );
         $make_call = $this->callAPI('POST', 'https://api.calendly.com/scheduling_links', json_encode($data_array));
         $response = json_decode($make_call, true);
         $candidate['booking_url'] = $response['resource']['booking_url'];
         Mail::to($candidate['email'])->send(new InterviewRoundTwoOffline($candidate));
      }
      // Get time interview
      return $this->successResponse([], 'Send Mail successful');
   }
   public function sendMailResult($id, Request $request)
   {
      // Send mail for candiate
      $candidate = Candidate::with(['schools', 'software', 'works', 'languages'])->find($id);
      $date_expired = date('d-m-Y', strtotime('4 weekdays'));
      $candidate['date_expired'] = $date_expired;
      if ($request->result == 1) {
         Mail::to($candidate['email'])->send(new ResultSuccess($candidate));
      }

      if ($request->result == 2) {
         Mail::to($candidate['email'])->send(new ResultFail($candidate));
      }

      if ($request->result == 3) {
         Mail::to($candidate['email'])->send(new ResultSuccess($candidate));
      }
      // Get time interview
      return $this->successResponse([], 'Send Mail successful');
   }
}
