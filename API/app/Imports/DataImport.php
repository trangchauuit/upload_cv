<?php

namespace App\Imports;

use App\Models\Candidate;
use App\Models\CandidateEducationHistory;
use App\Models\CandidateLanguage;
use App\Models\CandidateSoftwareSkill;
use App\Models\CandidateWorkingHistory;
use App\Utils\DateTimeUtil;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Maatwebsite\Excel\Concerns\WithChunkReading;
use Maatwebsite\Excel\Concerns\WithStartRow;
use Maatwebsite\Excel\Concerns\WithValidation;

class DataImport implements ToCollection, WithValidation //, ShouldQueue, WithChunkReading, WithStartRow
{
    /**
     * @var $candidate
     */
    public $candidate;

    public function collection(Collection $rows)
    {
        $candidate['name']  = $rows[4][3];
        $candidate['birthday'] = $this->convertDate($rows[2][13]);
        $candidate['year_birthday'] = $rows[3][12];
        $candidate['month_birthday'] = $rows[3][14];
        $candidate['day_birthday'] = $rows[3][16];
        $candidate['name_kana']  = $rows[3][3];
        $candidate['age'] = $rows[4][12];
        $candidate['country'] = $rows[5][3];
        $candidate['phone'] = $rows[6][3];
        $gender_levels = ['女/Nữ' => 1, '男/Nam' => 0];
        $candidate['gender'] = $gender_levels[$rows[5][12]];
        $candidate['phone'] = $rows[6][3];
        $married_levels = ['有/Đã kết hôn' => 1, '無/Chưa kết hôn' => 0];
        $candidate['married_status'] = $married_levels[$rows[6][12]];
        $candidate['zip_code'] = mb_convert_kana($rows[7][4], "n") ?? NULL;
        $candidate['zip_code_contact'] = mb_convert_kana($rows[9][4], "n") ?? NULL;
        $candidate['address'] = $rows[8][3];
        $candidate['address_contact'] = $rows[10][3];

        $candidate['email'] = $rows[11][3];
        if ( !empty($rows[13][3])) {
            $candidate['japan_visa'] = $rows[13][3] == '無/Không' ? 0 : 1;
        }

        if (!empty($rows[13][9]) && !empty($rows[13][12]) && !empty($rows[13][14])) {
            $candidate['japan_year'] = mb_convert_kana($rows[13][9], "n");
            $candidate['japan_month'] = mb_convert_kana($rows[13][12], "n");
            $candidate['japan_day'] = mb_convert_kana($rows[13][14], "n");
            $date = strtotime(mb_convert_kana($rows[13][9], "n") . '-' . mb_convert_kana($rows[13][12], "n") . '-' . mb_convert_kana($rows[13][14], "n"));
            $candidate['japan_visa_date_expired'] = $this->convertDate(date('Y-m-d', $date));
        }

        if ( !empty($rows[13][20])) {
            $candidate['japan_visa_type'] = $rows[13][20];
        }

        $lang_levels = ['日常会話レベル/Giao tiếp cơ bản' => 1, 'ビジネスレベル/Trình độ công việc' => 2, 'ネイティブレベル/Trình độ bản xứ' => 3];
        $candidate['japan_language_level'] = ($rows[17][3]) ? ($lang_levels[$rows[17][3]]) : NULL;
        $candidate['english_language_level'] = ($rows[17][10]) ? ($lang_levels[$rows[17][10]]) : NULL;

        $candidate['korean_language_level'] = ($rows[17][17]) ? ($lang_levels[$rows[17][17]]) : NULL;

        $candidate['self_pr'] = $rows[96][0];
        $candidate['applicant_comment'] = $rows[105][0];
        $candidate['applicant_advantage_skill_explanation'] = $rows[112][0];
        $cnd = Candidate::create($candidate);

        // language skills
        $langs = [];
        for ($i = 0; $i < 3; $i++) {
            if ($rows[20 + $i][5]) {
                if ($rows[20 + $i][5]) {
                    $lang['name'] = $rows[20 + $i][0];
                    $lang['level'] = $rows[20 + $i][5] ?? '';
                    $lang['year'] = mb_convert_kana($rows[20 + $i][14], "n") ?? NULL;
                    $lang['month'] = mb_convert_kana($rows[20 + $i][17], "n") ?? NULL;
                    $lang['candidate_id'] = $cnd->id;
                    CandidateLanguage::create($lang);
                }
            }
        }
        // language other
        for ($i = 0; $i < 8; $i++) {
            if ($rows[23 + $i][5]) {
                if ($rows[23 + $i][5]) {
                    $langOthers['name'] = $rows[23][0];
                    $langOthers['level'] = $rows[23 + $i][5] ?? '';
                    $langOthers['year'] = mb_convert_kana($rows[23 + $i][14], "n") ?? NULL;
                    $langOthers['month'] = mb_convert_kana($rows[23 + $i][17], "n") ?? NULL;
                    $langOthers['candidate_id'] = $cnd->id;
                    CandidateLanguage::create($langOthers);
                }
            }
        }

        // software skills
        for ($i = 0; $i < 2; $i++) {
            if ($rows[30 + $i][11]) {
                $soft1['software'] = $rows[30 + $i][6];
                $soft1['year'] = mb_convert_kana($rows[30 + $i][11], "n");
                $soft1['candidate_id'] = $cnd->id;
                $soft1['name'] = $rows[30 + $i][6];
                CandidateSoftwareSkill::create($soft1);
            }
            if ($rows[30 + $i][20]) {
                $soft2['software'] = $rows[30 + $i][15];
                $soft2['year'] = mb_convert_kana($rows[30 + $i][20], "n");
                $soft2['candidate_id'] = $cnd->id;
                $soft1['name'] = $rows[30 + $i][15];
                CandidateSoftwareSkill::create($soft2);
            }
        }

        // other software skills
        for ($i = 0; $i < 3; $i++) {
            if ($rows[32 + $i][11]) {
                $soft3['software'] = $rows[32 + $i][6];
                $soft3['year'] = mb_convert_kana($rows[32 + $i][11], "n");
                $soft3['candidate_id'] = $cnd->id;
                $soft3['name'] = 'Others';
                CandidateSoftwareSkill::create($soft3);
            }
            if ($rows[32 + $i][20]) {
                $soft4['software'] = $rows[32 + $i][15];
                $soft4['year'] = mb_convert_kana($rows[32 + $i][20], "n");
                $soft4['candidate_id'] = $cnd->id;
                $soft3['name'] = 'Others';
                CandidateSoftwareSkill::create($soft4);
            }
        }

        // Educations
        $schools = [];
        $start_row_school = 39;

        for ($i = 0; $i < 21; $i = $i + 3) {
            if ($rows[$start_row_school + $i][3]) {
                $school['from_year'] = mb_convert_kana($rows[$start_row_school + $i][0], "n");
                $school['from_month'] = mb_convert_kana($rows[$start_row_school + $i][2], "n");
                $school['to_year'] = mb_convert_kana($rows[$start_row_school + $i + 2][0], "n");
                $school['to_month'] = mb_convert_kana($rows[$start_row_school + $i + 2][2], "n");
                $school['school_name'] = $rows[$start_row_school + $i][3];
                $school['faculty'] = $rows[$start_row_school + $i][10];
                $school['study_skills'] = $rows[$start_row_school + $i][15];
                $school['candidate_id'] = $cnd->id;
                CandidateEducationHistory::create($school);
            }
        }
        // Work
        $works = [];
        $start_row_work = 64;
        for ($i = 0; $i < 30; $i = $i + 3) {
            if ( trim($rows[$start_row_work + $i][3]) ||
            trim($rows[$start_row_work + $i][0]) || 
            trim($rows[$start_row_work + $i + 2][0]) ||
            trim($rows[$start_row_work + $i + 2][2]) ||
            trim($rows[$start_row_work + $i][3]) ||
            trim($rows[$start_row_work + $i + 1][7]) ||
            trim($rows[$start_row_work + $i][12]) ||
            trim($rows[$start_row_work + $i][18]) ) {
                $work['from_year'] = mb_convert_kana($rows[$start_row_work + $i][0], "n");
                $work['from_month'] = mb_convert_kana($rows[$start_row_work + $i][2], "n");
                $work['to_year'] = mb_convert_kana($rows[$start_row_work + $i + 2][0], "n");
                $work['to_month'] = mb_convert_kana($rows[$start_row_work + $i + 2][2], "n");
                $work['company'] = $rows[$start_row_work + $i][3];
                if ($rows[$start_row_work + $i + 1][7] == 'アルバイト/Bán thời gian') {
                    $contract_type = 1;
                } else if ($rows[$start_row_work + $i + 1][7] == '契約社員/Hợp đồng') {
                    $contract_type = 3;
                } else if ($rows[$start_row_work + $i + 1][7] == '正社員/Toàn thời gian') {
                    $contract_type = 4;
                } else if ($rows[$start_row_work + $i + 1][7] == 'インターン/Thực tập') {
                    $contract_type = 5;
                } else if ($rows[$start_row_work + $i + 1][7] == 'ボランティア/Tình nguyện') {
                    $contract_type = 6;
                } else if ($rows[$start_row_work + $i + 1][7] == '入隊/ Nhập ngũ') {
                    $contract_type = 7;
                } else {
                    $contract_type = 2;
                }
                $work['contract_type'] = $contract_type;

                if ($rows[$start_row_work + $i + 2][7] == '日本/Nhật Bản') {
                    $work_place_type = 1;
                } else if ($rows[$start_row_work + $i + 2][7] == 'ベトナム/ Việt Nam') {
                    $work_place_type = 2;
                } else if ($rows[$start_row_work + $i + 2][7] == '外資/Doah nghiệp FDI') {
                    $work_place_type = 3;
                } else {
                    $work_place_type = 4;
                }
                $work['work_place_type'] = $work_place_type;
                $work['work_content'] = $rows[$start_row_work + $i][12];
                $work['work_skill'] = $rows[$start_row_work + $i][18];
                $work['candidate_id'] = $cnd->id;
                CandidateWorkingHistory::create($work);
            }
        }



        $this->candidate = $cnd;
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        // TODO: Implement rules() method.
        return [];
    }

    /**
     * @return int
     */
    public function chunkSize(): int
    {
        // TODO: Implement chunkSize() method.
        return 100;
    }

    /**
     * @return int
     */
    public function startRow(): int
    {
        // TODO: Implement startRow() method.
        return 1;
    }

    public  function convertDate($sDate)
    {
        $sDate = trim($sDate);
        $tempDate = explode('-', $sDate);
        if (count($tempDate) == 3) {
            if (checkdate($tempDate[1], $tempDate[2], $tempDate[0])) {
                return $sDate;
            }
            if (checkdate($tempDate[0], $tempDate[1], $tempDate[2])) {
                return $tempDate[2] . '-' . $tempDate[0] . '-' . $tempDate[1];
            }
            return false;
        }

        $tempDate = explode('/', $sDate);
        if (count($tempDate) == 3) {
            if (checkdate($tempDate[1], $tempDate[0], $tempDate[2])) {
                return $tempDate[2] . '-' . $tempDate[1] . '-' . $tempDate[0];
            }

            if (checkdate($tempDate[1], $tempDate[2], $tempDate[0])) {
                return $tempDate[0] . '-' . $tempDate[1] . '-' . $tempDate[2];
            }
            return false;
        }

        return '1970-01-01';
    }
}
