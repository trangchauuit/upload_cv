<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\WithStartRow;
use Illuminate\Support\Collection;
use Maatwebsite\Excel\Concerns\ToCollection;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class ValidateCVExcel implements ToCollection, WithStartRow
{
    /**
     * @var errors
     */
    public $errors = [];

    /**
     * @var isValidFile
     */
    public $isValidFile = false;

    /**
     * ValidateCsvFile constructor.
     * @param StoreEntity $store
     */
    public function __construct()
    {
        //
    }

    public function collection(Collection $rows)
    {
        $errors = [];
        $candidate['name']  = $rows[4][3];
        $candidate['year_birthday'] = $rows[3][12];
        $candidate['month_birthday'] = $rows[3][14];
        $candidate['day_birthday'] = $rows[3][16];
        $candidate['birthday'] = $rows[3][13];
        $candidate['name_kana']  = $rows[3][3];
        $candidate['age'] = (int)$rows[4][12];
        $candidate['country'] = $rows[5][3];
        $candidate['address'] = $rows[8][3];
        $candidate['age'] = $rows[4][12];
        $candidate['gender'] = $rows[5][12];
        $candidate['phone'] = $rows[6][3];
        $candidate['address_contact'] = $rows[10][3];
        $candidate['married_status'] = $rows[6][12];
        $candidate['email'] = $rows[11][3];

        $candidate['email'] = $rows[11][3];
        if ( $rows[12][3]=='無' ) {
            $rows[12][3] = 0;
        }
        if ( $rows[12][3]=='有' ) {
            $rows[12][3] = 1;
        }

        $candidate['japan_language_level'] = $rows[17][3];

        $candidate['other_language'] = $rows[15][18];
        $candidate['other_language_level'] = $rows[16][17];

        $start_row_school = 39;
        $message_schools = [
            'from_year_school.required' => 'In CV file, Education histories year is a required field. Trong CV, vui lòng điền thông tin Năm trong Lý lịch học vấn.',
            'from_month_school.required' => 'In CV file, Education histories month is a required field. Trong CV vui lòng điền thông tin Tháng trong Lý lịch học vấn.',
            'to_year_school.required' => 'In CV file, Education histories year is a required field. Trong CV, vui lòng điền thông tin Năm trong Lý lịch học vấn.',
            'to_month_school.required' => 'In CV file, Education histories month is a required field. Trong CV vui lòng điền thông tin Tháng trong Lý lịch học vấn.',
            'school_name_school.required' => 'In CV file, School name is a required field. Trong CV, vui lòng điền thông tin Trường trong Lý lịch học vấn.',
            'faculty_school.required' => 'In CV file, Faculty is a required field. Trong CV, vui lòng điền thông tin Khoa, chuyên ngành, GPA  trong Lý lịch học vấn.',
            'study_skills_school.required' => 'In CV file, Skill is a required field. Trong CV, vui lòng điền thông tin Kỹ Năng trong Lý lịch học vấn.',
        ];
        for ($i = 0; $i < 21; $i = $i + 3) {
            if ($rows[$start_row_school + $i][3] || 
                $rows[$start_row_school + $i][0] || 
                $rows[$start_row_school + $i][2] ||
                $rows[$start_row_school + $i + 2][0] || 
                $rows[$start_row_school + $i + 2][2] ||
                $rows[$start_row_school + $i][3] ||
                $rows[$start_row_school + $i][10] || 
                $rows[$start_row_school + $i][15]) {
                $school['from_year_school'] = mb_convert_kana($rows[$start_row_school + $i][0], "n");
                $school['from_month_school'] = mb_convert_kana($rows[$start_row_school + $i][2], "n");
                $school['to_year_school'] = mb_convert_kana($rows[$start_row_school + $i + 2][0], "n");
                $school['to_month_school'] = mb_convert_kana($rows[$start_row_school + $i + 2][2], "n");
                $school['school_name_school'] = $rows[$start_row_school + $i][3];
                $school['faculty_school'] = $rows[$start_row_school + $i][10];
                $school['study_skills_school'] = $rows[$start_row_school + $i][15];
                $validatorStudy = Validator::make($school, [
                    'from_year_school' => [
                        'required',
                        'int'
                    ],
                    'from_month_school' => [
                        'required',
                        'int'
                    ],
                    'to_year_school' => [
                        'required',
                        'int'
                    ],
                    'to_month_school' => [
                        'required',
                        'int'
                    ],
                    'school_name_school' => [
                        'required'
                    ],
                    'faculty_school' => [
                        'required'
                    ]
                ],
                $message_schools);
                if ($validatorStudy->fails()) {
                    $errors[] = $validatorStudy->errors();
                }
            }
        }

        $message_works = [
            'from_year_work.required' => 'In CV file, Working histories year is a required filed. Trong CV, vui lòng điền thông tin Năm trong Lý lịch làm việc.',
            'from_month_work.required' => 'In CV file, Working histories month is a required field. Trong CV, vui lòng điền thông tin Tháng trong Lý lịch làm việc.',
            'to_year_work.required' => 'In CV file, Working histories year is a required filed. Trong CV, vui lòng điền thông tin Năm trong Lý lịch làm việc.',
            'to_month_work.required' => 'In CV file, Working histories month is a required field. Trong CV, vui lòng điền thông tin Tháng trong Lý lịch làm việc.',
            'company_work.required' => 'In CV file, Company is a required field. Trong CV, vui lòng điền thông tin Công Ty trong Lý lịch làm việc.',
            'contract_type_work.required' => 'In CV file, Working form is a required field. Trong CV, vui lòng điền thông tin Hình thức làm việc trong Lý lịch làm việc.',
            'work_place_type_work.required' => 'In CV file, Place of work is a required field. Trong CV, vui lòng điền thông tin Nơi làm việc trong Lý lịch làm việc.',
            'work_content_work.required' => 'In CV file, Job position is a required field. Trong CV, vui lòng điền thông tin Chức vụ, vị trí trong Lý lịch làm việc.',
            'work_skill_work.required' => 'In CV file, Job description is a required field. Trong CV, vui lòng điền thông tin Nội dung công việc trong Lý lịch làm việc.',
        ];
        // Work
        $start_row_work = 64;
        for ($i = 0; $i < 30; $i = $i + 3) {
            if ($rows[$start_row_work + $i][3] ||
                $rows[$start_row_work + $i][0] || 
                $rows[$start_row_work + $i + 2][0] ||
                $rows[$start_row_work + $i + 2][2] ||
                $rows[$start_row_work + $i][3] ||
                $rows[$start_row_work + $i + 1][7] ||
                $rows[$start_row_work + $i][12] ||
                $rows[$start_row_work + $i][18] ) {
                $work['from_year_work'] = mb_convert_kana($rows[$start_row_work + $i][0], "n");
                $work['from_month_work'] = mb_convert_kana($rows[$start_row_work + $i][2], "n");
                $work['to_year_work'] = mb_convert_kana($rows[$start_row_work + $i + 2][0], "n");
                $work['to_month_work'] = mb_convert_kana($rows[$start_row_work + $i + 2][2], "n");
                $work['company_work'] = $rows[$start_row_work + $i][3];
                $work['contract_type_work'] = $rows[$start_row_work + $i + 1][7];
                $work['work_place_type_work'] = $rows[$start_row_work + $i + 2][7];
                $work['work_content_work'] = $rows[$start_row_work + $i][12];
                $work['work_skill_work'] = $rows[$start_row_work + $i][18];
                $validatorWork = Validator::make($work, [
                    'from_year_work' => [
                        'required',
                        'int'
                    ],
                    'from_month_work' => [
                        'required',
                        'int'
                    ],
                    'to_year_work' => [
                        'required',
                        'int'
                    ],
                    'to_month_work' => [
                        'required',
                        'int'
                    ],
                    'contract_type_work' => [
                        'required'
                    ],
                    'work_place_type_work' => [
                        'required'
                    ],
                    'work_content_work' => [
                        'required'
                    ],
                    'work_skill_work' => [
                        'required'
                    ]
                ], $message_works);
                if ($validatorWork->fails()) {
                    $errors[] = $validatorWork->errors();
                }
            }
        }
        $candidate['self_pr'] = $rows[96][0];
        $candidate['applicant_comment'] = $rows[105][0];
        $candidate['applicant_advantage_skill_explanation'] = $rows[112][0];
        $messages = [
            'name.required' => 'In CV file, Name is a required field. Trong CV vui lòng điền thông tin Tên.',
            'year_birthday.required' => 'In CV file, Year birthday is a required field.  Trong CV, vui lòng điền thông tin Năm sinh',
            'month_birthday.required' => 'In CV file, Month birthday is a required field. Trong CV, vui lòng điền thông thông tin Tháng sinh.',
            'day_birthday.required' => 'In CV file, Date birthday is a required field. Trong CV, vui lòng điền thông tin Ngày sinh',
            'year_birthday.int' => 'In CV File,  Year birthday must be an integer. Trong CV, năm sinh bắt buộc ở dạng số.',
            'month_birthday.int' => 'In CV File, Month birthday must be an integer. Trong CV, ngày sinh bắt buộc ở dạng số.',
            'day_birthday.int' => 'In CV File, Date birthday must be an integer. Trong CV, ngày sinh bắt buộc ở dạng số.',
            'country.required' => 'In CV file, Nationality is a required field. Trong CV, vui lòng điền thông tin Quốc tịch.',
            'age.required' => 'In CV file, Age is a required field. Trong CV, vui lòng điền thông tin Tuổi.',
            'age.int' => 'In CV File,  Age must be an integer. Trong CV, tuổi bắt buộc ở dạng số.',
            'gender.required' => 'In CV file, Gender is a required field. Trong CV, vui lòng điền thông tin Giới tính.',
            'phone.required' => 'In CV file, Telephone is a required field. Trong CV, vui lòng điền thông tin Điện thoại.',
            'married_status.required' => 'In CV file, Relationship status is a required field. Trong CV, vui lòng điền thông tin Trạng thái Kết hôn.',
            'address.required' => 'In CV file, Address is a required filed. Trong CV, vui lòng điền thông tin Địa chỉ.',
            'address_contact.required' => 'In CV file, Contact address is a required field. Trong CV, vui lòng điền thông tin Địa chỉ liên hệ.',
            'email.required' => 'In CV file, Email is a required field. Trong CV, vui lòng điền thông tin email.',
            'self_pr.required' => 'In CV file, Application motivation is a required field. Trong CV, vui lòng điền thông tin Động lực ứng tuyển.',
            'applicant_comment.required' => 'In CV file, personal Expectation is a required field. Trong CV, vui lòng điền thông tin Nguyện vọng cá nhân.',
            'applicant_advantage_skill_explanation.required' => 'In CV file, Hobby, talent is a required field. Trong CV, vui lòng điền thông tin Sở thích, năng khiếu.'
        ];
        $validator = Validator::make($candidate, [
            'name' => [
                'required',
                'string'
            ],
            'year_birthday' => [
                'required',
                'int'
            ],
            'month_birthday' => [
                'required',
                'int'
            ],
            'day_birthday' => [
                'required',
                'int'
            ],
            'age' => [
                'required',
                'int'
            ],
            'country' => [
                'required',
            ],
            'address' => [
                'required',
            ],
            'phone' => [
                'required',
            ],
            'address_contact' => [
                'required',
            ],
            'married_status' => [
                'required',
            ],
            'gender' => [
                'required',
            ],
            'email' => ['required', 'email'],
            'self_pr' => [
                'required',
            ],
            'applicant_comment' => [
                'required',
            ],
            'applicant_advantage_skill_explanation' => [
                'required',
            ]

        ], $messages);
        if ($validator->fails()) {
            $errors[] = $validator->errors();
        }
        foreach ( $errors as $error) {
            $this->errors = $error;
        }
    }

    public function startRow(): int
    {
        return 1;
    }
}
