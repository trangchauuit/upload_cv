<?php

namespace Database\Seeders;

use App\Models\CompanyCategory;
use App\Models\CompanyType;
use Illuminate\Database\Seeder;

class CompanyCategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            '電気設備' => [
            ],
            '空調・衛生' => [
            ],
            '電気・電子（開発／回路・実装設計／制御設計／生産・品質管理など）' => [
            ],
            ' 計画・監督（ゼネコン・住宅メーカー・不動産・設計）' => [
            ],
            '建設機械・仮設資材' => [
            ],
            '土木・内装' => [
            ],
            '機械（設計／研究・開発／オペレーターなど）' => [
            ],
            '半導体' => [
            ],
            'IT・インターネット' => [
            ],
            '商社' => [
            ],
            '流通・小売' => [
            ],
            '物流・倉庫' => [
            ],
            'サービス（ホテル・旅館／不動産／飲食／航空など）' => [
            ],
            '広告・出版' => [
            ],
            'その他' => [
            ]
        ];
       foreach ($data as $type_name => $categories) {
           $type = CompanyType::create(['name' => $type_name]);
           foreach ($categories as $category_name) {
               CompanyCategory::create([
                   'company_type_id' => $type->id,
                   'name' => $category_name
               ]);
           }
       }
    }
}
