<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidateEducationHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_education_histories', function (Blueprint $table) {
            $table->id();
            $table->integer('candidate_id');
            $table->integer('from_month');
            $table->integer('from_year');
            $table->integer('to_month');
            $table->integer('to_year');
            $table->string('school_name', 255);
            $table->string('faculty', 255);
            $table->string('study_skills', 1000)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_education_histories');
    }
}
