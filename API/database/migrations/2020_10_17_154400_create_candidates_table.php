<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidatesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidates', function (Blueprint $table) {
            $table->id();
            $table->string('name_kana', 100);
            $table->date('birthday');
            $table->string('name', 100);
            $table->integer('age');
            $table->string('country', 100);
            $table->smallInteger('gender')->default(0);
            $table->string('phone', 20)->nullable();
            $table->tinyInteger('married_status')->default(0);
            $table->string('zip_code', 7);
            $table->string('near_train_station');
            $table->string('email', 255);

            $table->smallInteger('japan_visa')->default(0);
            $table->date('japan_visa_date_expired')->nullable();
            $table->string('japan_visa_type')->nullable();


            $table->smallInteger('japan_language_level')->nullable();
            $table->smallInteger('english_language_level')->nullable();
            $table->string('other_language')->nullable();
            $table->smallInteger('other_language_level')->nullable();
//
//            $table->string('japanese_language_achievement')->nullable();
//            $table->string('toeic_achievement')->nullable();

            $table->text('self_pr')->nullable();
            $table->text('applicant_comment')->nullable();
            $table->text('applicant_advantage_skill_explanation')->nullable();
            $table->string('cv_filename', 1000)->nullable();
            $table->smallInteger('status')->default(1);
            $table->smallInteger('introduce_company_type_id')->nullable();
            $table->smallInteger('introduce_company_category_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidates');
    }
}
