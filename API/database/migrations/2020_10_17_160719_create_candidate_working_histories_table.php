<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCandidateWorkingHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('candidate_working_histories', function (Blueprint $table) {
            $table->id();
            $table->integer('candidate_id');
            $table->integer('from_month');
            $table->integer('from_year');
            $table->integer('to_month');
            $table->integer('to_year');
            $table->string('company', 255);
            $table->string('contract_type', 255)->nullable();
            $table->string('work_place_type', 255)->nullable();
            $table->string('work_content', 1000)->nullable();
            $table->string('work_skill', 255)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('candidate_working_histories');
    }
}
