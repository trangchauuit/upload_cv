### Tech stacks
- PHP: 7.3.33
- Nginx: 1.22.1
- MySQL: 5.7.41
- NodeJS: v14.21.3
- Laravel 8
### Cài đặt
Vui lòng cài đặt các stacks ở trên theo đúng phiên bản.
1. Thiết lập cấu hình nginx theo mẫu:
```
server {
  listen 80;
  server_name _; # fake IP address
  root /home/ec2-user/upload_cv/FE/build; #path to static directory

  add_header X-Frame-Options "SAMEORIGIN";
  add_header X-XSS-Protection "1; mode=block";
  add_header X-Content-Type-Options "nosniff";
  index index.html index.htm index.php;
  charset utf-8;

  location / {
    try_files $uri $uri/ /index.html;
  }

  location /api {
    alias /home/ec2-user/upload_cv/API/public;
    try_files $uri $uri/ @laravelapi;

    location ~ \.php$ {
      include snippets/fastcgi-php.conf;
      fastcgi_pass unix:/var/run/php-fpm/www.sock;
      fastcgi_param SCRIPT_FILENAME $request_filename;
    }
  }

  location @laravelapi {
    rewrite /api/(.*)?$ /api/index.php?$is_args$args last;
  }

  location = /favicon.ico {
    access_log off; log_not_found off;
  }

  location = /robots.txt {
    access_log off; log_not_found off;
  }

  error_page 404 /index.php;
  location ~ /\.(?!well-known).* {
    deny all;
  }
}
```
2. Restart nginx: `sudo systemctl restart nginx`
#### API
Di chuyển vào thư mục API
1. Sau khi cài MySQL thì tạo một database
2. Copy file `.env.example` thành `.env`, cập nhật biến môi trường theo  mẫu
3. Deploy API theo tài liệu của Laravel: https://laravel.com/docs/8.x/deployment
#### FE
Di chuyển vào thư mục FE
1. Cài package `npm install`
2. Copy file `.env.example` thành `.env`, cập nhật biến môi trường theo  mẫu
3. Chạy `npm run build` để build

=======
Enjoy your app!!!

